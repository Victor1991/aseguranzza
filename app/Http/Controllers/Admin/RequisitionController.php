<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Requisition;
use App\Policy;
use App\TypeEndorsement;
use App\TypeLoad;
use App\WayPay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class RequisitionController extends Controller
{
    //
    public function index()
    {
        $requisitions = Requisition::all();
        $policys = Policy::all();
        return view('admin.requisition.index', compact('requisitions', 'policys'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
         
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

         $data = $request->validate([
              'num' => 'required|string|max:255',
         ]);
         $data = request()->all();
         $data["creator_user"] = Auth::id(); 

         $requisition = Requisition::create($data);

         return redirect()->route('admin.requisition.index')->with('success', 'Endoso registrado');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Requisition $requisition)
    {
         $type_persons = TypePerson::all();
         $promoters = requisition::all();
         $sexs = Sex::all();
         $types_sellers = TypeSeller::all();
         $states = State::all();
         return view('admin.requisition.show', compact('requisition','type_persons', 'promoters', 'sexs', 'types_sellers', 'states'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Requisition $requisition)
    {
         $type_endorsements = TypeEndorsement::all();
         $way_pays = WayPay::all();
         $policys = Policy::all();
         $type_load = TypeLoad::all();
         return view('admin.requisition.edit', compact('requisition', 'type_endorsements', 'way_pays', 'policys', 'type_load'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Requisition $requisition)
    {

         $data = request()->all();
         $data["initial_validity"] = request("initial_validity") != "" ? Carbon::createFromFormat('d-m-Y', request("initial_validity"))->format('Y-m-d') : NULL ;
         $data["final_validity"] = request("final_validity") != "" ? Carbon::createFromFormat('d-m-Y', request("final_validity"))->format('Y-m-d') : NULL ;
         $data["receipt_expiration"] = request("receipt_expiration") != "" ? Carbon::createFromFormat('d-m-Y', request("receipt_expiration"))->format('Y-m-d') : NULL ;
         $data["shipping_date"] = request("shipping_date") != "" ? Carbon::createFromFormat('d-m-Y', request("shipping_date"))->format('Y-m-d') : NULL ;

         $requisition->update( $data );

         return redirect()->route('admin.requisition.index')->with('success', 'Endoso actualizado');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(requisition $requisition)
    {
         
         $requisition->delete();
         return back()->with('success', 'Usuario eliminado');

    }

}
