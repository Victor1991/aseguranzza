<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class UsersExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    private $date;

    public function View(): View
    {
         return view('exports.users',[
              'users' => User::get()
         ]);
    }

    public function collection()
    {
        return User::all();
    }
}
