<!-- Modal -->
<div class="modal fade" id="modal_file" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Archivos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row" id="archivo_cards">
                                
                            </div>
                          
                        </div>
                      </div>
                </div>
                <div class="col-md-12">
                    <label for="archivo">Archivo</label>
                    <input type="file"  id="archivo_policy">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        let global_policie_id;

        function ver_modal_archivos(policie_id, number_tel){
            console.log(policie_id);
            console.log(number_tel);
            $('#archivo_cards').html(`<div class="text-center col-md-12">
                                                <i class='fas fa-spinner fa-spin fa-3x'></i>
                                            </div>`);
            $.ajax({
                processData: false,
                contentType: false,
                type: "GET",
                dataType: "json",
                url: "{{ route('admin.files_policys.index') }}?id="+policie_id,
                success: function(result) {
                    if(result.length > 0){
                    $('#archivo_cards').html('');
                    result.forEach(function(elemento) {
                            let url_file  = "{{url('/')}}"+elemento.file;
                            $( "#archivo_cards" ).append( `<div class="col-md-4">
                                    <div class="card text-center">
                                        <a href="${elemento.file}" target="_blank">
                                            <i class="fa fa-file fa-5x" style="margin: 0 auto; padding: 10px;" aria-hidden="true"></i>
                                            <p>${elemento.name}</p>
                                        </a>
                                        <br>
                                        <div class="row" style="margin:10px">
                                            <div class="col-6">
                                                <a href="https://api.whatsapp.com/send?phone=52${number_tel}&text=En el siguiente link podrás descargar el archivo ${url_file}" target="_blank" class="btn btn-success btn-block btn-sm"><i class="fab fa-whatsapp"></i></a>
                                            </div>
                                            <div class="col-6">
                                                <button class="btn btn-sm btn-block btn-danger" onclick="eliminar_archivo(${elemento.id})">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>` );
                        });
                   }else{
                        $('#archivo_cards').html(`<tr>
                                                <td colspan="3" class="text-center">
                                                    Sin archivos
                                                </td>
                                            </tr>`);
                   }
                },
                error:function(x,xs,xt){
                        
                }
            });

            global_policie_id = policie_id;
            $('#modal_file').modal('show');
        }

        function eliminar_archivo(id){
            swal({
                title: "Eliminar",
                text: "¿ Desea eliminar ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Eliminar',
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function(res) {
                if(res.value){
                    $.ajax({
                        type: "DELETE",
                        dataType: "json",
                        url: "{{ url('admin/files_policys/') }}/"+id,
                        // url: "/admin/contacts/{{ $user->id }}",
                        success: function(result) {
                            swal({
                                title: "Éxito",
                                text: 'Se elimino correctamente',
                                type: 'success',
                                confirmButtonColor  : "#32c5d2",
                                confirmButtonText   : "Ok",
                                allowOutsideClick: false
                            }).then(function() {
                                ver_modal_archivos(global_policie_id);
                            });
                        },
                        error:function(x,xs,xt){
                            var text = '';
                            var error_ret = x.responseJSON.errors;
                            $.each(error_ret, function(index, value) {
                                text += '<span class="strg">'+index + '</span> : '+ value[0] + '<br>';
                            });
                            swal({
                                title: "Error",
                                html: text,
                                type: 'error',
                            });
                        }
                    });
                }
            });
        }

        $('#archivo_policy').change(function(e) {
            var data = new FormData();
            let fileName = e.target.files[0];
            data.append("archivo", fileName);
            data.append("policie_id", global_policie_id);
            $.ajax({
                    processData: false,
                    contentType: false,
                    type: "POST",
                    dataType: "json",
                    url: "{{ route('admin.files_policys.store') }}",
                    data: data, // serializes the form's elements.
                    success: function(result) {
                         swal({
                              title: "Éxito",
                              text: 'Archivo registrado correctamente',
                              type: 'success',
                              confirmButtonColor  : "#32c5d2",
                              confirmButtonText   : "Ok",
                              allowOutsideClick: false
                         }).then(function() {
                            $('#archivo_policy').val('');
                            $('.bootstrap-filestyle input').val('');
                            ver_modal_archivos(global_policie_id);
                         });
                    },
                    error:function(x,xs,xt){
                         var text = '';
                         var error_ret = x.responseJSON.errors;
                         $.each(error_ret, function(index, value) {
                              text += '<span class="strg">'+index + '</span> : '+ value[0] + '<br>';
                         });
                         swal({
                              title: "Error",
                              html: text,
                              type: 'error',
                         });
                    }
               });

        });
    </script>
    
@endpush
@push('style')

@endpush