<table>
     <thead>
          <tr>
               <th>Creada por </th>
               <th>Título</th>
               <th>Descripción</th>
               <th>Fecha de asignación</th>
               <th>Fecha entrega</th>
               <th>Estatus</th>
               <th>Asignada a</th>
               <th>Fecha cambio a pendiente</th>
               <th>Usuario cambio a pendiente</th>
               <th>Fecha finaliza tarea</th>
               <th>Comentario final</th>
          </tr>
     </thead>
     <tbody>
          @foreach($tasks as $task)
          <tr>
               <td>{{ $task->creado_por->name }} {{ $task->creado_por->last_name }} {{ $task->creado_por->mother_last_name }}</td>
               <td>{{ $task->name }}</td>
               <td>{{ $task->description }}</td>
               <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $task->created_at)->format('d-m-Y') }}</td>
               <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->end_date)->format('d-m-Y') }}</td>
               <td>{{ $task->estatus->name }}</td>
               <td>{{ $task->asignado->name }} {{ $task->asignado->last_name }} {{ $task->asignado->mother_last_name }}</td>
               <td>
                    <?php if ($task->user_pending_date): ?>
                         {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->user_pending_date)->format('d-m-Y') }}
                    <?php endif; ?>
               </td>
               <td>
                    <?php if ($task->coment_pending): ?>
                         {{ $task->coment_pending }}
                    <?php endif; ?>
               </td>
               <td>
                    <?php if ($task->user_termination_date): ?>
                         {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->user_termination_date)->format('d-m-Y') }}
                    <?php endif; ?>
               </td>
               <td>
                    <?php if ($task->coment_termination): ?>
                         {{ $task->coment_termination }}
                    <?php endif; ?>
               </td>

          </tr>
          @endforeach
     </tbody>
</table>
