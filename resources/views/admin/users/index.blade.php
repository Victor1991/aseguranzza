@extends('admin.layout')

@section('content')
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h3 class="card-title">Contactos</h3>
                <div class="card-tools">
                    <a href="{{ route('admin.user.create') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i> Nuevo
                    </a>
                </div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover table-striped table_js">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Teléfono</th>
                            <th>Correo</th>
                            <th class="text-center">WhatsApp</th>
                            <th class="text-center">Enviar correo</th>
                            <th class="text-center"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }} {{ $user->last_name }} {{ $user->mother_last_name }}</td>

                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->email }}</td>
                                <td class="text-center">
                                    <?php if ($user->cell_phone): ?>
                                    <a href="https://api.whatsapp.com/send?phone=+52{{ $user->cell_phone }}"
                                        target="_blank" class="btn btn-success btn-sm"><i class="fab fa-whatsapp"></i></a>
                                    <?php endif; ?>
                                </td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-secondary btn-sm"
                                        onclick="ver_modal_mail('{{ $user->email }}')">
                                        <i class="fas fa-envelope-open-text"></i>
                                    </button>
                                </td>
                                <td class="text-center">

                                    <a href="{{ route('admin.user.edit', $user) }}"
                                        class="btn btn-sm btn-info btn-outline"><i class="fas fa-user-edit"></i></a>

                                    <form method="POST" action="{{ route('admin.user.destroy', $user) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger btn-outline"
                                            onclick="return confirm('¿ Estás seguro de querer desactivar este contacto ?')">
                                            <i class="fas fa-trash"></i>
                                        </button>

                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('admin.users.modal_mail')
@stop
