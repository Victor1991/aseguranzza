@extends('admin.layout')

@section('content')
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h3 class="card-title">Tipos de Pólizas</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach ($policys as $key => $item)
                        <div class="col-md-6">
                            <a href="{{ url('admin/'.$item->id.'/policys/') }}">
                                <div class="info-box mb-3 bg-warning">
                                    <span class="info-box-icon">{!! $item->icon !!}</span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">{!! $item->name !!}</span>
                                        <span class="info-box-number">{!! $item->policys->count() !!}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
@push('scripts')
@endpush

@push('style')
    <style>
        .perf {
            font-size: 13px;
        }

        .bg-light {
            width: 100%;
        }

        .nav-tabs .nav-item.show .nav-link,
        .nav-tabs .nav-link.active {
            color: #fff;
            background-color: #3088f9;
        }

    </style>
@endpush
