@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card ">
          <div class="card-header ">
               <h3 class="card-title">Tareas</h3>
               <div class="card-tools">
                    <a href="{{ route('task.excel') }}" class="btn btn-warning btn-sm" id="btn_calend" data-toggle="modal" data-target="#calendario">
                         <i class="fas fa-calendar"></i> Calendario
                    </a>
                    <a href="{{ route('task.excel') }}" class="btn btn-info btn-sm" data-toggle="modal" data-target="#grafica">
                         <i class="fas fa-chart-bar"></i> Grafica
                    </a>
                    <a href="{{ route('task.excel') }}" class="btn btn-secondary btn-sm">
                         <i class="fas fa-file-excel" style="color:#65d290;"></i> Reporte
                    </a>
                    <a href="{{ route('admin.task.create') }}" class="btn btn-success btn-sm">
                         <i class="fa fa-plus"></i> Nuevo
                    </a>
               </div>
          </div>
          <div class="card-body">
               <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                    <li class="nav-item">
                         <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Ultimas 5 tareas</a>
                    </li>
                    <?php $en_lista = array();  ?>
                    <?php foreach ($tasks as $key => $task): ?>
                         <?php if (!in_array($task->user_id, $en_lista)): ?>
                              <li class="nav-item">
                                   <a class="nav-link" id="task_user_{{$task->user_id}}-tab" data-toggle="pill" href="#task_user_{{$task->user_id}}" role="tab" aria-controls="custom-content-above-home" aria-selected="true">{{ $task->asignado->name }} {{ $task->asignado->last_name }} {{ $task->asignado->mother_last_name }}</a>
                              </li>
                              <?php array_push($en_lista, $task->user_id); ?>
                         <?php endif; ?>
                    <?php endforeach; ?>

               </ul>
               <div class="tab-content" id="custom-content-above-tabContent" style="padding: 20px; border: 1px solid #ddd; background-color: #e3e3e3;">
                    <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                         <div class="row d-flex align-items-stretch justify-content-md-center">
                              <?php $con = 0; ?>
                              <?php foreach ($tasks as $key => $task): ?>
                                   <?php if ($task->status != 3): ?>
                                        <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                                             <div class="card bg-light">
                                                  <div class="card-header text-muted border-bottom-0">
                                                       {{ $task->name }}
                                                  </div>
                                                  <div class="card-body pt-0">
                                                       <div class="row">
                                                            <div class="col-12">
                                                                 <h2 class="lead"><b>{{ $task->asignado->name }} {{ $task->asignado->last_name }} {{ $task->asignado->mother_last_name }}</b>
                                                                 </h2>
                                                                 <p class="text-muted text-sm" style=" text-align:justify;"><b>Tarea: </b> {{ $task->description }} </p>
                                                                 <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                      <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> <b>Creado por : </b> {{ $task->creado_por->name }} {{ $task->creado_por->last_name }} {{ $task->creado_por->mother_last_name }}</li>
                                                                      <li class="small"><span class="fa-li"><i class="fas fa-lg fa-calendar"></i></span> <b> Fecha de cración : </b> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $task->created_at)->format('d-m-Y') }} </li>
                                                                      <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user-tag"></i></span> <b> Contacto : </b> {{ $task->contacto->name }} {{ $task->contacto->last_name }} {{ $task->contacto->mother_last_name }} </li>
                                                                      <?php if (!is_null($task->end_date) && $task->end_date != '0000-00-00'): ?>
                                                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-calendar-check"></i></span><b>Fecha de entrega :</b> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->end_date)->format('d-m-Y') }} </li>
                                                                      <?php endif; ?>
                                                                      <?php if ($task->imagen): ?>
                                                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-file"></i></span><b>Archivo adjunto :</b> <a href="{{$task->imagen}}" target="_blank"> Ver archivo </a> </li>
                                                                      <?php endif; ?>
                                                                 </ul>
                                                                 <?php if ($task->coment_pending): ?>
                                                                      <hr>
                                                                      <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-tag"></i></span> <b>Comentario en estatus pendiente: </b> <br> {{ $task->coment_pending }} <br> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->user_pending_date)->format('d-m-Y') }} </li>
                                                                      </ul>
                                                                 <?php endif; ?>
                                                                 <?php if ($task->coment_termination): ?>
                                                                      <hr>
                                                                      <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                           <li class="small"><span class="fa-li"><i class="fas fa-lg fa-tag"></i></span> <b>Comentario al finalizar: </b> <br> {{ $task->coment_termination }} <br> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->user_termination_date)->format('d-m-Y') }} </li>
                                                                      </ul>
                                                                 <?php endif; ?>
                                                            </div>
                                                       </div>
                                                  </div>
                                                  <div class="card-footer">
                                                       <?php if ($task->status == 2): ?>
                                                                 <div class="alert alert-warning text-center" role="alert" style="padding: .15rem 0.45rem; margin-bottom: 0px; width: 200px; float: left;">
                                                                      <i class="fa fa-check" aria-hidden="true"></i> Tarea pendiente
                                                                 </div>
                                                                 <a href="{{ route('admin.task.edit', $task) }}" class="btn btn-sm btn-info text-right" style="float: right;">
                                                                      <i class="fas fa-edit"></i> Editar
                                                                 </a>
                                                       <?php elseif($task->status == 3): ?>
                                                            <center>
                                                                 <div class="alert alert-success text-center" role="alert" style="padding: .15rem 0.45rem; margin-bottom: 0px; width: 300px;">
                                                                      <i class="fa fa-check" aria-hidden="true"></i> Tarea finalizada
                                                                 </div>
                                                            </center>
                                                       <?php else: ?>
                                                            <a href="{{ route('admin.task.edit', $task) }}" class="btn btn-sm btn-info text-right" style="float: right;">
                                                                 <i class="fas fa-edit"></i> Editar
                                                            </a>
                                                       <?php endif; ?>


                                                  </div>
                                             </div>
                                        </div>
                                        <?php $con++; ?>
                                        <?php if ($con == 5): ?>
                                             <?php break; ?>
                                        <?php endif; ?>
                                   <?php endif; ?>
                              <?php endforeach; ?>
                         </div>
                    </div>
                    @foreach ($users as $user)
                        @if ($user->tareas_asignadas->count() > 0)
                          <div class="tab-pane fade" id="task_user_{{$user->id}}" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                              <div class="row d-flex align-items-stretch justify-content-md-center">
                                   @foreach ($user->tareas_asignadas as $task)
                                   <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                                        <div class="card bg-light">
                                             <div class="card-header text-muted border-bottom-0">
                                                  {{ $task->name }}
                                             </div>
                                             <div class="card-body pt-0">
                                                  <div class="row">
                                                       <div class="col-12">
                                                            <h2 class="lead"><b>{{ $task->asignado->name }} {{ $task->asignado->last_name }} {{ $task->asignado->mother_last_name }}</b>
                                                            </h2>
                                                            <p class="text-muted text-sm" style=" text-align:justify;"><b>Tarea: </b> {{ $task->description }} </p>
                                                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                 <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> <b>Creado por : </b> {{ $task->creado_por->name }} {{ $task->creado_por->last_name }} {{ $task->creado_por->mother_last_name }}</li>
                                                                 <li class="small"><span class="fa-li"><i class="fas fa-lg fa-calendar"></i></span> <b> Fecha de cración : </b> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $task->created_at)->format('d-m-Y') }} </li>
                                                                 <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user-tag"></i></span> <b> Contacto : </b> {{ $task->contacto->name }} {{ $task->contacto->last_name }} {{ $task->contacto->mother_last_name }} </li>
                                                                 <?php if (!is_null($task->end_date) && $task->end_date != '0000-00-00'): ?>
                                                                      <li class="small"><span class="fa-li"><i class="fas fa-lg fa-calendar-check"></i></span><b>Fecha de entrega :</b> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->end_date)->format('d-m-Y') }} </li>
                                                                 <?php endif; ?>
                                                                 <?php if ($task->imagen): ?>
                                                                      <li class="small"><span class="fa-li"><i class="fas fa-lg fa-file"></i></span><b>Archivo adjunto :</b> <a href="{{$task->imagen}}" target="_blank"> Ver archivo </a> </li>
                                                                 <?php endif; ?>
                                                            </ul>
                                                            <?php if ($task->coment_pending): ?>
                                                                 <hr>
                                                                 <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                      <li class="small"><span class="fa-li"><i class="fas fa-lg fa-tag"></i></span> <b>Comentario en estatus pendiente: </b> <br> {{ $task->coment_pending }} <br> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->user_pending_date)->format('d-m-Y') }} </li>
                                                                 </ul>
                                                            <?php endif; ?>
                                                            <?php if ($task->coment_termination): ?>
                                                                 <hr>
                                                                 <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                      <li class="small"><span class="fa-li"><i class="fas fa-lg fa-tag"></i></span> <b>Comentario al finalizar: </b> <br> {{ $task->coment_termination }} <br> {{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->user_termination_date)->format('d-m-Y') }} </li>
                                                                 </ul>
                                                            <?php endif; ?>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="card-footer">
                                                  <?php if ($task->status == 2): ?>
                                                            <div class="alert alert-warning text-center" role="alert" style="padding: .15rem 0.45rem; margin-bottom: 0px; width: 200px; float: left;">
                                                                 <i class="fa fa-check" aria-hidden="true"></i> Tarea pendiente
                                                            </div>
                                                            <a href="{{ route('admin.task.edit', $task) }}" class="btn btn-sm btn-info text-right" style="float: right;">
                                                                 <i class="fas fa-edit"></i> Editar
                                                            </a>
                                                  <?php elseif($task->status == 3): ?>
                                                       <center>
                                                            <div class="alert alert-success text-center" role="alert" style="padding: .15rem 0.45rem; margin-bottom: 0px; width: 300px;">
                                                                 <i class="fa fa-check" aria-hidden="true"></i> Tarea finalizada
                                                            </div>
                                                       </center>
                                                  <?php else: ?>
                                                       <a href="{{ route('admin.task.edit', $task) }}" class="btn btn-sm btn-info text-right" style="float: right;">
                                                            <i class="fas fa-edit"></i> Editar
                                                       </a>
                                                  <?php endif; ?>


                                             </div>
                                        </div>
                                   </div>   
                                   @endforeach
                              </div>
                         </div>
                        @endif
                    @endforeach
               </div>
          </div>
     </div>
</div>


<div class="modal fade" id="grafica" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h4 class="modal-title">Grafica de tareas</h4>
               </div>
               <div class="modal-body">
                    @include('admin.task.chart')
               </div>
               <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               </div>
          </div>
     </div>
</div>


<!-- Modal -->
<div class="modal fade" id="calendario" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
               <div class="modal-body">
                    <div style="z-index: 2; background: #18408b94; color:#fff; width: 96%; height: 100%; position: absolute; display:none;">
                         <div style="margin-top: 30%; text-align:center;">
                              <h3>Cargando</h3> 
                              <i class="fas fa-spinner fa-spin fa-3x"></i>
                         </div>
                    </div>
                    @include('admin.task.calendar')
               </div>
               <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
               </div>
          </div>
     </div>
</div>



@stop
@push('scripts')
@endpush

@push('style')
<style>
.perf{
     font-size: 13px;
}
.bg-light{
     width: 100%;
}
.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #fff;
    background-color: #3088f9;
}
</style>
@endpush
