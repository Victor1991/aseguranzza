<!-- Modal -->
<div class="modal fade" id="modal_form_notificacion" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2a7cf0; color: #fff;">
                <h5 class="modal-title">Formulario Notificacion<span></span></h5>
            </div>
            <div class="row" style="padding: 10px;">
                <div class="col-md-12">
                    <label for="nota">Nota</label>
                    <textarea class="form-control" id="message_modal" cols="30" rows="10"></textarea>
                </div>

                <div class="col-md-12">
                    <label for="archivo">Archivo</label>
                    <input type="file"  id="archivo">
                </div>

                <div class="col-md-12">
                    <hr>
                </div>

                <div class="col-6 text-left">
                    <button class="btn btn-sm btn-default" onclick="cancelar_form()"> Cancelar </button>
                </div>

                <div class="col-6 text-right">
                    <button class="btn btn-sm btn-success" onclick="guardar_modal()"> Guardar </button>
                </div>

            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function ver_modal_notificacion_form(id = false) {
            $('#modal_notificacion').modal('hide');
            $('#modal_form_notificacion').modal('show');
        }

        function guardar_modal(){
            var data = new FormData();
            var card_fiscal = $('#archivo')[0].files[0];
            data.append("archivo", card_fiscal);
            data.append('message', $('#message_modal').val());
            data.append('policie_id', policie_id);

            $.ajax({
                    processData: false,
                    contentType: false,
                    type: "POST",
                    dataType: "json",
                    url: "{{ route('admin.notification_policy.store') }}",
                    // url: "/admin/contacts/{{ $user->id }}",
                    data: data, // serializes the form's elements.
                    success: function(result) {
                         swal({
                              title: "Éxito",
                              text: 'Registrado correctamente',
                              type: 'success',
                              confirmButtonColor  : "#32c5d2",
                              confirmButtonText   : "Ok",
                              allowOutsideClick: false
                         }).then(function() {
                            $('#modal_form_notificacion').modal('hide');
                            ver_modal_notificacion(policie_id);
                         });
                    },
                    error:function(x,xs,xt){
                         var text = '';
                         var error_ret = x.responseJSON.errors;
                         $.each(error_ret, function(index, value) {
                              text += '<span class="strg">'+index + '</span> : '+ value[0] + '<br>';
                         });
                         swal({
                              title: "Error",
                              html: text,
                              type: 'error',
                         });
                    }
               });


        }

        function cancelar_form(){
            $('#modal_form_notificacion').modal('hide');

            $('#modal_notificacion').modal('show');
        }
    </script>

@endpush
@push('style')

@endpush
