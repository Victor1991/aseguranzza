<?php

use App\TypePerson;
use Illuminate\Database\Seeder;

class TypePersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypePerson::truncate();
        $fisica = new TypePerson;
        $fisica->name = 'fisica';
        $fisica->save();

        $moral = new TypePerson;
        $moral->name = 'moral';
        $moral->save();
    }
}
