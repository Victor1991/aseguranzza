<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contac extends Mailable
{
    use Queueable, SerializesModels;

    public $correo;
    public $subject;
    public $correo_oculto;
    public $content;
    public $file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($correo, $subject, $correo_oculto, $content, $file)
    {
        $this->correo = $correo;
        $this->subject = $subject;
        $this->correo_oculto = $correo_oculto;
        $this->content = $content;
        $this->file = $file;
        // dump($file);
        // dump($correo);
        // dump($subject);
        // dump($correo_oculto);
        // dump($file);
        // dump($content);
        // die;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->file != ""){
            $rootPath = $_SERVER['DOCUMENT_ROOT'];
            $thisPath = dirname($_SERVER['PHP_SELF']);
            // $onlyPath = $rootPath. $thisPath.'public/'.$this->file;
            $onlyPath = $rootPath. $thisPath.$this->file;
            $this->attach($onlyPath);
        }
       
        if($this->correo_oculto != ""){
            foreach ($this->correo_oculto as $key => $correo) {
                $this->bcc(trim($correo));
            }
        }

        $this->cc("josias@aseguranzza.com");
        
        return $this->markdown('emails.contact')->subject( $this->subject );
        // return $this->markdown('emails.contact');
    }
}
