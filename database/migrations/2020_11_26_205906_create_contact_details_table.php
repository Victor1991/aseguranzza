<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactDetailsTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
          Schema::create('contact_details', function (Blueprint $table) {
               $table->increments('id');
               $table->integer('user_id');
               $table->integer('type_address_id');
               $table->integer('name');

               $table->integer('state_id');
               $table->integer('municipality_id');
               $table->integer('location_id');
               $table->integer('postal_code')->nullable();
               $table->string('street')->nullable();
               $table->string('reference')->nullable();
               $table->string('email')->unique();
               $table->string('phone');
               $table->string('cell_phone');
               $table->string('rfc')->nullable();
               $table->string('stall')->nullable();
               $table->string('reason_social')->nullable();


               $table->timestamps();
          });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
          Schema::dropIfExists('contact_details');
     }
}
