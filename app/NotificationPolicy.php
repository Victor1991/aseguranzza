<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationPolicy extends Model
{
    //
    protected $fillable = [
        "policie_id", "message", "file", 'created_by'
    ];

    public function creado_por()
    {
      return $this->hasOne( User::class, 'id', 'created_by' );
      
    }

}
