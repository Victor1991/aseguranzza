<!-- Modal -->
<div class="modal fade" id="modal_mail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2a7cf0; color: #fff;">
                <h5 class="modal-title">Enviar correo <span></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" enctype="multipart/form-data" action="{{ route('admin.send_mail') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="in_correo" name="correo" value="">
                        <div class="col-md-12">
                            <label for="">Correo :</label>
                            <p id="tx_correo"></p>
                        </div>
                        <div class="col-lg-12">
                            <label for="">Asunto correo</label>
                            <input type="text" name="subject" value="" class="form-control" placeholder="Asunto correo"
                                required>
                        </div>
                        <div class="col-lg-12" style="margin-top: 15px;">
                            <label for="">Copia oculta</label>
                            <input type="text" name="correo_oculto" value="" class="form-control"
                                placeholder="Ejemplo - contacto@aseguranzza.com, envio@aseguranzza.com">
                        </div>
                        <div class="col-lg-12" style="margin-top: 15px;">
                            <label for="">Cuerpo del correo</label>
                            <textarea name="content" class="form-control" rows="8" cols="80" required></textarea>
                        </div>
                        <div class="col-lg-12" style="margin-top: 15px;">
                            <label for="">Archivo adjunto</label><br>
                            <input type="file" name="file" data-name="file" class="filestyle"
                                data-buttonName="btn-primary" data-badgeName="badge-danger"
                                data-buttonText="Adjuntar archivo">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('scripts')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/2.1.0/bootstrap-filestyle.min.js"></script>
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('content');
            $(":file").filestyle({
                buttonName: "btn-primary",
                buttonText: "Adjuntar archivo",
                badgeName: "badge-danger"
            });

            $('.buttonText').text('Adjuntar archivo');
        });

        function ver_modal_mail(mail) {
            $('#in_correo').val(mail);
            $('#tx_correo').text(mail);
            $('#modal_mail').modal('show');
        }


        $('.filestyle').on('change', function() {
            var FileSize = this.files[0].size / 1024 / 1024; // in MiB
            if (FileSize > 10) {
                swal({
                    title: "Error",
                    html: 'El tamaño del archivo supera los 10 MB permitidos',
                    type: 'error',
                });
                $(this).val(''); //for clearing with Jquery
            }
        });

    </script>

@endpush
@push('style')

@endpush
