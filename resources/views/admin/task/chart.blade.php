<div class="col-md-12">
     <div id="container"></div>
</div>
<br>
<table id="datatable" class="table table-striped">
     <thead>
          <tr>
               <th></th>
               <th>Tareas asignadas</th>
               <th>Tareas pendientes</th>
               <th>Tareas finalizadas</th>
          </tr>
     </thead>
     <tbody>
          <?php foreach ($users as $key => $user): ?>
               <?php if ($user->tareas_asignadas->count() > 0): ?>
                    <tr>
                         <th>{{ $user->name }} {{ $user->last_name }} {{ $user->mother_last_name }}</th>
                         <td class="text-center">{{ $user->tareas_asignadas->count() }}</td>
                         <td class="text-center">{{ $user->tareas_pendientes->count() }}</td>
                         <td class="text-center">{{ $user->tareas_finalizadas->count() }}</td>
                    </tr>
               <?php endif; ?>
          <?php endforeach; ?>
     </tbody>
</table>


@push('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<!-- <script src="https://code.highcharts.com/modules/exporting.js"></script> -->
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
Highcharts.chart('container', {
     data: {
          table: 'datatable'
     },
     chart: {
          type: 'column'
     },
     title: {
          text: ''
     },
     yAxis: {
          allowDecimals: false,
          title: {
               text: 'Tareasr'
          }
     },
     tooltip: {
          formatter: function () {
               return '<b>' + this.series.name + '</b><br/>' +
               this.point.y + ' ' + this.point.name.toLowerCase();
          }
     }
});
</script>
@endpush
@push('style')

@endpush
