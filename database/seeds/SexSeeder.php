<?php


use App\Sex;
use Illuminate\Database\Seeder;

class SexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sex::truncate();
        $masculino = new Sex;
        $masculino->name = 'masculino';
        $masculino->save();

        $femenino = new Sex;
        $femenino->name = 'femenino';
        $femenino->save();
    }
}
