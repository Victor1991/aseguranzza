@extends('admin.layout')

@section('content')
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h3 class="card-title">Contactos</h3>
                <div class="card-tools">
                    <a href="#" data-toggle="modal" data-target="#modal_created" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i> Nuevo
                    </a>
                </div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover table-striped table_js">
                    <thead>
                        <tr>
                            <th>Fecha de Emisión</th>
                            <th>Póliza</th>
                            <th>Asegurado</th>
                            <th>Endoso Numero.</th>
                            <th>Creado</th>
                            <th class="text-center"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($requisitions as $requisition)
                            <tr>
                                <td class="text-center">{{ $requisition->created_at->format('d-m-Y')}}</td>
                                <td class="text-center">{{ $requisition->poliza->request_number }}</td>
                                <td class="text-center">{{ $requisition->poliza->asegurado->name }} {{ $requisition->poliza->asegurado->last_name }} {{ $requisition->poliza->asegurado->mother_last_name }}</td>
                                <td class="text-center">{{ $requisition->num }}</td>
                                <td lass="text-center">
                                    {{ $requisition->creado_por->name }} {{ $requisition->creado_por->last_name }} {{ $requisition->creado_por->mother_last_name }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin.requisition.edit', $requisition) }}"
                                        class="btn btn-sm btn-info btn-outline"><i class="fas fa-edit"></i></a>

                                    {{-- <form method="POST" action="{{ route('admin.requisition.destroy', $requisition) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger btn-outline"
                                            onclick="return confirm('¿ Estás seguro de querer desactivar este contacto ?')">
                                            <i class="fas fa-trash"></i>
                                        </button>

                                    </form> --}}

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('admin.requisition.modal_new')
@stop
