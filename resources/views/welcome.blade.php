<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="shortcut icon" href="https://aseguranzza.com/content/corporation/images/favicon.ico">

     <title>{{ config('app.name', 'Laravel') }}</title>

     <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

     <!-- Styles -->
     <style>
     html, body {
          background-image: url("https://images.theconversation.com/files/295811/original/file-20191007-121065-t6bvel.jpg?ixlib=rb-1.1.0&rect=1520%2C742%2C6508%2C3248&q=45&auto=format&w=1356&h=668&fit=crop");
          background-repeat: no-repeat;
          background-color: #000 !important;
          color: #636b6f;
          font-family: 'Nunito', sans-serif;
          font-weight: 200;
          height: 100vh;
          margin: 0;
          background-position: center center;
     }

     .full-height {
          height: 100vh;
     }

     .flex-center {
          align-items: center;
          display: flex;
          justify-content: center;
     }

     .position-ref {
          position: relative;
     }

     .top-right {
          position: absolute;
          right: 10px;
          top: 18px;
     }

     .content {
          background-color: #f4f4f4bd;
          border-radius: 5px;
          padding: 15px;
          text-align: center;
     }

     .title {
          font-size: 84px;
     }

     .links > a {
          color: #000;
          padding: 0 25px;
          font-size: 12px;
          font-weight: 600;
          letter-spacing: .1rem;
          text-decoration: none;
          text-transform: uppercase;
     }

     a {
          position: relative;
          display: inline-block;
          font-size: 2em;
          font-weight: 800;
          color: royalblue;
          overflow: hidden;
     }

     a {
          /* Same as before */
          background: linear-gradient(to right, #00cc3d, #00cc3d 50%, black 50%);
     }

     a {
          /* Same as before */
          background-clip: text;
          -webkit-background-clip: text;
          -webkit-text-fill-color: transparent;
          background-size: 200% 100%;
          background-position: 100%;
     }
     a {
          /* Same as before */
          transition: background-position 275ms ease;
     }
     a:hover {
          background-position: 0 100%;
     }

     .m-b-md {
          margin-bottom: 30px;
     }
     </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     
</head>
<body>
     <div class="flex-center position-ref full-height">

          <div class="content">
               <div class="title">
                    <img src="https://aseguranzza.com/content/corporation/images/corporation.png" alt="">
               </div>
               <div class="links">
                    @if (Route::has('login'))
                    @auth
                    <a href="{{ route('dashboard') }}">Entrar</a>
                    @else
                    <a href="{{ route('login') }}">Iniciar sesión</a>
                    @endauth
                    @endif
               </div>
          </div>
     </div>
</body>
</html>
