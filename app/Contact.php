<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name', 'email', 'password', 'type_person_id', 'promoter_id',
        'last_name', 'mother_last_name', 'stall', 'rfc', 'birthdate', 'sex_id', 'curp',
        'type_seller', 'state_id', 'municipality_id', 'location_id', 'street', 'n_ext',
        'n_int', 'phone', 'cell_phone', 'card_fiscal', 'identification_official',
        'voucher_home', 'minutes_constitutive', 'powers', 'business_folio', 'other',
        'postal_code', 'reference', 'passport', 'identification_official_validity', 'passport_validity',
        'status','notes', 'insured_grouo_id', 'executive_id', 'reference_user', 'business', 'occupation', 'insured_grouo', 'assigned_number'
   ];

   /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
   protected $hidden = [
        'password', 'remember_token',
   ];

   public function perfil()
   {
        return $this->belongsTo( TypeSeller::class, 'type_seller', 'id' );
   }

   public static function identification_official_expire()
   {
        return \DB::table('users')->where('identification_official_validity', '!=' , '0000-00-00')->WhereNotNull('identification_official_validity')->get();
   }

   public static function passport_expire()
   {
        return \DB::table('users')->where('passport_validity', '!=' , '0000-00-00')->WhereNotNull('passport_validity')->get();
   }

   public function contactos()
   {
        return $this->hasMany( InformationContact::class);
   }

   public function tareas_asignadas()
   {
        return $this->hasMany( Task::class, 'user_id', 'id' )->orderBy('created_at', 'desc');
   }

   public function tareas_pendientes()
   {
        return $this->hasMany( Task::class, 'user_id', 'id' )->where('tasks.status', '2');
   }

   public function tareas_asignadas_no_finalizadas()
   {
        return $this->hasMany( Task::class, 'user_id', 'id' )->where('tasks.status', '!=', '3')->orderBy('created_at', 'desc');
   }


   public function tareas_finalizadas()
   {
        return $this->hasMany( Task::class, 'user_id', 'id' )->where('tasks.status', '3');
   }

   public function facturacion()
   {
        return $this->hasMany( Billing::class );
   }

   public function grupo_asegurado()
   {
        return $this->belongsTo( InsuredGroup::class, 'insured_grouo_id', 'id');
   }

   public function direction_cp()
   {
        return $this->hasMany( PostalCode::class, 'id', 'location_id' );
   }

   public function polizas()
   {
          return $this->hasMany( Policy::class, 'contratante', 'id' );
   }
}
