<div class="row">
    <div class="col-md-12">
        <div id="calendar"></div>
    </div>
</div>

@push('scripts')
<script src="{{ asset('/theme/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/es.min.js" integrity="sha512-rW9yo+wCSOiuvTcjeYtoihJoZHWXYWqPNdNs1hpMLn+Gz8THZaHpU10qbxdNlcydbUZghThvVR5KevjTHWM2ww==" crossorigin="anonymous"></script> -->
<!-- Page specific script -->
<script>
var calendar;
$(function () {
     /* initialize the calendar
     -----------------------------------------------------------------*/
     //Date for the calendar events (dummy data)
     var date = new Date()
     var d    = date.getDate(),
     m    = date.getMonth(),
     y    = date.getFullYear()

     console.log(d);
     console.log(m);
     console.log(y);


     var Calendar = FullCalendar.Calendar;
     var Draggable = FullCalendarInteraction.Draggable;

     var containerEl = document.getElementById('external-events');
     var checkbox = document.getElementById('drop-remove');
     var calendarEl = document.getElementById('calendar');

     // initialize the external events
     // -----------------------------------------------------------------

    calendar = new Calendar(calendarEl, {
          plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
          header    : {
          //      today: 'Hoy'
          // },
               // left  : 'prev,next today',
               // center: 'title',
               // right : 'dayGridMonth,timeGridWeek,timeGridDay',
               // right : 'prev,next today',

          },
          buttonText:{
               prev:"Ant",
               next:"Sig",
               today:"Hoy",
               month:"Mes",
               week:"Semana",
               day:"DÃ­a",
               list:"Agenda"
          },
          locale: 'es',
          'themeSystem': 'bootstrap',
          //Random default events
          eventClick:  function(info) {
            window.open('./task/'+info.event.id+'/edit' ,'_blank');
 		 },
          eventSources: [{
                    url: "{{ url('api/full_calendar_task') }}",
                }],
          editable  : true,
          droppable : true, // this allows things to be dropped onto the calendar !!!

     });

     calendar.render();
    
});

$('#btn_calend').click(function() {
    window.setTimeout(clickToday, 500);
});

function clickToday() {
    $('button.fc-today-button.btn.btn-primary').prop( "disabled", false );
    $('button.fc-today-button.btn.btn-primary').click();
}

</script>

@endpush

@push('style')
<!-- fullCalendar -->
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<style>
.info_card{
     text-align:center;
     background-color: rgba(10, 10, 10, 0.66);
     margin-top:80px;
     margin-bottom:80px;
     padding: 80px;
     border-radius: 15px;
}
</style>
@endpush
