<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use App\FilesPolicys;
use Response;
use Auth;

class FilesPolicysController extends Controller
{
    //
    public function index(Request $request)
    {
        $id =  request("id");
        $result = FilesPolicys::where('policie_id', $id)->get();
        return response()->json($result->load('creado_por'));

    }

    public function store(Request $request)
    {

        $data = $request->validate([
              'policie_id' => 'required'
        ]);

        if ($request->file('archivo') != null) {
            // var_dump($request->file('archivo')->originalName);
            $nota = $request->file('archivo')->store('public');
            $data['file'] = Storage::url($nota);
            $data['name'] = $request->file('archivo')->getClientOriginalName();
       }

       $data['created_by'] = Auth::user()->id;

        $result = FilesPolicys::create($data);

        return response()->json($result);

    }

    public function destroy($id)
    {   
        $files_policys = FilesPolicys::where('id', $id)->first();
         $files_policys->delete();
        return response()->json($files_policys);
    }

}
