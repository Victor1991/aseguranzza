<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Contact;
use App\TypePerson;
use App\Sex;
use App\TypeSeller;
use App\State;
use Response;
use Carbon\Carbon;
use App\Municipality;
use App\Location;
use App\PostalCode;
use App\Events\UserWasCreated;
use App\Events\TaskWasCreated;
use App\InsuredGroup;

use App\Billing;
use App\InformationContact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class ContacController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $type = $request->query('type');
         if(is_null($type) || $type == "all" ){
              $users = Contact::all();
         }else{
              if($type == 1){
                    $users = Contact::where('status', 1)->get();
              }else{
                    $users = Contact::where('status', 0)->get();
              }
         }
         return view('admin.contact.index', compact('users', 'type'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
         $users = User::all();
         $user = new Contact;
         $type_persons = TypePerson::all();
         $promoters = Contact::all();
         $sexs = Sex::all();
         $types_sellers = TypeSeller::all();
         $insured_groups = InsuredGroup::all();
         $states = PostalCode::groupBy('estado_id')->orderBy('estado_id','ASC')->get();
         $municipalitys = PostalCode::where('estado_id', 1)->groupBy('municipio_id')->orderBy('municipio_id','ASC')->get();
         return view('admin.contact.create', compact('user', 'type_persons', 'promoters', 'sexs', 'types_sellers', 'states', 'municipalitys', 'insured_groups', 'users'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

         $data = $request->validate([
              'name' => 'required|string|max:255',
              'email' => 'required',
              'rfc' => 'required|unique:contacts,rfc',
         ],[],[
               'name' => 'Nombre',
               'email' => 'Correo',
               'rfc' => 'RFC',
         ]);

         $data["type_person_id"] = request("type_person_id");
         $data["promoter_id"] = request("promoter_id");
         $data["name"] = strtoupper(request("name"));
         $data["last_name"] = strtoupper(request("last_name"));
         $data["mother_last_name"] = strtoupper(request("mother_last_name"));
         $data["rfc"] = strtoupper(request("rfc"));
         $data["birthdate"] = Carbon::createFromFormat('d/m/Y', request("birthdate"))->format('Y-m-d');
         $data["sex_id"] = request("sex_id");
         $data["curp"] = strtoupper(request("curp"));
         $data["type_seller"] = request("type_seller");
         $data["state_id"] = request("state_id");
         $data["municipality_id"] = request("municipality_id");
         $data["location_id"] = request("location_id");
         $data["postal_code"] = request("postal_code");
         $data["street"] = strtoupper(request("street"));
         $data["n_ext"] = request("n_ext");
         $data["n_int"] = request("n_int");
         $data["reference"] = strtoupper(request("reference"));
         $data["email"] = strtoupper(request("email"));
         $data["phone"] = request("phone");
         $data["cell_phone"] = request("cell_phone");
         $data["reference_user"] = strtoupper(request("reference_user"));
         $data["notes"] = strtoupper(request("notes"));
         $data["insured_grouo"] = strtoupper(request("insured_grouo"));

         $data["business"] = strtoupper(request("business"));
         $data["occupation"] = strtoupper(request("occupation"));

         $data["executive_id"] = strtoupper(request("executive_id"));
         $data["assigned_number"] = strtoupper(request("assigned_number"));

         if( request("passport_validity") != '' ){
              $data["passport_validity"] =  Carbon::createFromFormat('d/m/Y', request("passport_validity"))->format('Y-m-d');
         }

         if( request("identification_official_validity") != ''){
              $data["identification_official_validity"] =  Carbon::createFromFormat('d/m/Y', request("identification_official_validity"))->format('Y-m-d');
         }

         if (request("eliminar_passport") == 1) {
              $data['passport'] = '';
         }
         if ($request->file('passport') != null) {
              $passport = $request->file('passport')->store('public');
              $data['passport'] = Storage::url($passport);
         }

         if (request("eliminar_card_fiscal") == 1) {
              $data['card_fiscal'] = '';
         }
         if ($request->file('card_fiscal') != null) {
              $card_fiscal = $request->file('card_fiscal')->store('public');
              $data['card_fiscal'] = Storage::url($card_fiscal);
         }

         if (request("eliminar_identification_official") == 1) {
              $data['identification_official'] = '';
         }
         if ($request->file('identification_official') != null) {
              $identification_official = $request->file('identification_official')->store('public');
              $data['identification_official'] = Storage::url($identification_official);
         }

         if (request("eliminar_voucher_home") == 1) {
              $data['voucher_home'] = '';
         }
         if ($request->file('voucher_home') != null) {
              $voucher_home = $request->file('voucher_home')->store('public');
              $data['voucher_home'] = Storage::url($voucher_home);
         }

         if (request("eliminar_minutes_constitutive") == 1) {
              $data['minutes_constitutive'] = '';
         }
         if ($request->file('minutes_constitutive') != null) {
              $minutes_constitutive = $request->file('minutes_constitutive')->store('public');
              $data['minutes_constitutive'] = Storage::url($minutes_constitutive);
         }

         if (request("eliminar_powers") == 1) {
              $data['powers'] = '';
         }
         if ($request->file('powers') != null) {
              $powers = $request->file('powers')->store('public');
              $data['powers'] = Storage::url($powers);
         }

         if (request("eliminar_business_folio") == 1) {
              $data['business_folio'] = '';
         }
         if ($request->file('business_folio') != null) {
              $business_folio = $request->file('business_folio')->store('public');
              $data['business_folio'] = Storage::url($business_folio);
         }

         if (request("eliminar_other") == 1) {
              $data['other'] = '';
         }
         if ($request->file('other') != null) {
              $other = $request->file('other')->store('public');
              $data['other'] = Storage::url($other);
         }
         

         $data['password'] = bcrypt('12345678');

         $user = Contact::create($data);


         if(request("name_contacto")){
              // contacto secundario
              foreach (request("name_contacto") as $key => $contacto) {
                   $contacto_save['user_id'] = $user->id ;
                   $contacto_save['name'] = strtoupper($contacto);
                   $contacto_save['phone'] = request("phone_contacto")[$key];
                   $contacto_save['email'] = strtoupper(request("emai_contacto")[$key]);
                   InformationContact::create($contacto_save);
              }
         }

         if(request("business_name_facturacion")){
              // contacto secundario
              foreach (request("business_name_facturacion") as $key => $business_name) {
                   $facturacion_save['user_id'] = $user->id ;
                   $facturacion_save['business_name'] = strtoupper($business_name) ;
                   $facturacion_save['rfc'] = strtoupper(request("rfc_facturacion")[$key]);
                   // $facturacion_save['address'] = request("emai_contacto")[$key];
                   $facturacion_save['mail'] = strtoupper(request("mail_facturacion")[$key]);
                   $facturacion_save['phone'] = request("phone_facturacion")[$key];

                   $facturacion_save['postal_code'] = request("postal_code_facturacion")[$key];
                   $facturacion_save['state_id'] = request("state_id_facturacion")[$key];
                   $facturacion_save['municipality_id'] = request("municipality_id_facturacion")[$key];
                   $facturacion_save['location_id'] = request("location_id_facturacion")[$key];
                   $facturacion_save['street'] = strtoupper(request("street_facturacion")[$key]);
                   $facturacion_save['n_ext'] = request("n_ext_facturacion")[$key];
                   $facturacion_save['n_int'] = request("n_int_facturacion")[$key];

                   Billing::create($facturacion_save);
              }
         }

     //     UserWasCreated::dispatch($user, $data['password']);
         return Response::json($user);
         // return redirect()->route('admin.user.index')->with('success', 'Administrador registrado');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Contact $user)
    {
         $type_persons = TypePerson::all();
         $promoters = Contact::all();
         $sexs = Sex::all();
         $types_sellers = TypeSeller::all();
         $states = State::all();
         return view('admin.contact.show', compact('user','type_persons', 'promoters', 'sexs', 'types_sellers', 'states'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Contact $contact)
    {
         $users = User::all();
         $user = $contact ;
         $type_persons = TypePerson::all();
         $promoters = Contact::all();
         $sexs = Sex::all();
         $types_sellers = TypeSeller::all();
         $states = State::all();
         $insured_groups = InsuredGroup::all();

         $states = PostalCode::groupBy('estado_id')->orderBy('estado_id','ASC')->get();
         $municipalitys = PostalCode::where('estado_id', 1)->groupBy('municipio_id')->orderBy('municipio_id','ASC')->get();

         $data_cp = $this->code_postal($user->postal_code);

         return view('admin.contact.edit', compact('user','type_persons', 'promoters', 'sexs', 'types_sellers', 'states', 'municipalitys', 'data_cp', 'insured_groups', 'users'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Contact $contact)
    {

         $data = $this->validate( $request, [
              'name' => 'required|string|max:255',
              'email' => 'required',
              'insured_grouo' => 'required',
              'rfc' => "required|unique:contacts,rfc,{$contact->id}",
         ], [],
         [
               'name' => 'Nombre',
               'email' => 'Correo',
               'insured_grouo' => 'Grupo de Asegurados',
               'rfc' => "RFC"
         ]);

         if($request->filled('password')) {
              $data =  $request->validate([
                   'password' => 'confirmed', 'min:7',
              ],[],[
                    'password' => 'Contraseña',
              ]);
         }

         $data["type_person_id"] = request("type_person_id");
         $data["promoter_id"] = request("promoter_id");
         $data["name"] = strtoupper(request("name"));
         $data["last_name"] = strtoupper(request("last_name"));
         $data["mother_last_name"] = strtoupper(request("mother_last_name"));
         $data["rfc"] = strtoupper(request("rfc"));
         $data["birthdate"] = Carbon::createFromFormat('d/m/Y', request("birthdate"))->format('Y-m-d');
         $data["sex_id"] = request("sex_id");
         $data["curp"] = strtoupper(request("curp"));
         $data["type_seller"] = request("type_seller");
         $data["state_id"] = request("state_id");
         $data["municipality_id"] = request("municipality_id");
         $data["location_id"] = request("location_id");
         $data["postal_code"] = request("postal_code");
         $data["street"] = strtoupper(request("street"));
         $data["n_ext"] = request("n_ext");
         $data["n_int"] = request("n_int");
         $data["reference"] = strtoupper(request("reference"));
         $data["email"] = strtoupper(request("email"));
         $data["phone"] = request("phone");
         $data["cell_phone"] = request("cell_phone");
         $data["reference_user"] = strtoupper(request("reference_user"));
         $data["notes"] = strtoupper(request("notes"));

         $data["insured_grouo"] = strtoupper(request("insured_grouo"));
         $data["executive_id"] = strtoupper(request("executive_id"));
       
         $data["business"] = strtoupper(request("business"));
         $data["occupation"] = strtoupper(request("occupation"));
         $data["assigned_number"] = strtoupper(request("assigned_number"));


         if( request("passport_validity") != '' ){
              $data["passport_validity"] =  Carbon::createFromFormat('d/m/Y', request("passport_validity"))->format('Y-m-d');
         }else{
              $data["passport_validity"] = '';
         };

         if( request("identification_official_validity") != ''){
              $data["identification_official_validity"] =  Carbon::createFromFormat('d/m/Y', request("identification_official_validity"))->format('Y-m-d');
         }else {
              $data["identification_official_validity"] = '';
         };

         if (request("eliminar_card_fiscal") == 1) {
              $data['card_fiscal'] = '';
         }
         if ($request->file('card_fiscal') != null) {
              $card_fiscal = $request->file('card_fiscal')->store('public');
              $data['card_fiscal'] = Storage::url($card_fiscal);
         }

         if (request("eliminar_identification_official") == 1) {
              $data['identification_official'] = '';
         }
         if ($request->file('identification_official') != null) {
              $identification_official = $request->file('identification_official')->store('public');
              $data['identification_official'] = Storage::url($identification_official);
         }

         if (request("eliminar_voucher_home") == 1) {
              $data['voucher_home'] = '';
         }
         if ($request->file('voucher_home') != null) {
              $voucher_home = $request->file('voucher_home')->store('public');
              $data['voucher_home'] = Storage::url($voucher_home);
         }

         if (request("eliminar_passport") == 1) {
              $data['passport'] = '';
         }
         if ($request->file('passport') != null) {
              $passport = $request->file('passport')->store('public');
              $data['passport'] = Storage::url($passport);
         }


         if (request("eliminar_minutes_constitutive") == 1) {
              $data['minutes_constitutive'] = '';
         }
         if ($request->file('minutes_constitutive') != null) {
              $minutes_constitutive = $request->file('minutes_constitutive')->store('public');
              $data['minutes_constitutive'] = Storage::url($minutes_constitutive);
         }

         if (request("eliminar_powers") == 1) {
              $data['powers'] = '';
         }
         if ($request->file('powers') != null) {
              $powers = $request->file('powers')->store('public');
              $data['powers'] = Storage::url($powers);
         }

         if (request("eliminar_business_folio") == 1) {
              $data['business_folio'] = '';
         }
         if ($request->file('business_folio') != null) {
              $business_folio = $request->file('business_folio')->store('public');
              $data['business_folio'] = Storage::url($business_folio);
         }

         if (request("eliminar_other") == 1) {
              $data['other'] = '';
         }
         if ($request->file('other') != null) {
              $other = $request->file('other')->store('public');
              $data['other'] = Storage::url($other);
         }



         $contact->update( $data );

         $existen = array();
         if(request("name_contacto")){
              // contacto secundario
              foreach (request("name_contacto") as $key => $contacto) {
                   $existen[] = $key;
                   $info = InformationContact::where('id', $key)->first();
                   $contacto_save['name'] = strtoupper($contacto) ;
                   $contacto_save['phone'] = request("phone_contacto")[$key];
                   $contacto_save['email'] = strtoupper(request("emai_contacto")[$key]);
                   $info->update($contacto_save);
              }
         }

         InformationContact::where('contact_id', $contact->id )->whereNotIn('id', $existen)->delete();

         $existen = array();
         if(request("business_name_facturacion")){
              // contacto secundario
              foreach (request("business_name_facturacion") as $key => $business_name) {
                   $existen[] = $key;

                   $info = Billing::where('id', $key)->first();
                   $facturacion_save['contact_id'] = $contact->id ;
                   $facturacion_save['business_name'] = strtoupper($business_name) ;
                   $facturacion_save['rfc'] = strtoupper(request("rfc_facturacion")[$key]);
                   // $facturacion_save['address'] = request("emai_contacto")[$key];
                   $facturacion_save['mail'] = strtoupper(request("mail_facturacion")[$key]);
                   $facturacion_save['phone'] = request("phone_facturacion")[$key];

                   $facturacion_save['postal_code'] = request("postal_code_facturacion")[$key];
                   $facturacion_save['state_id'] = request("state_id_facturacion")[$key];
                   $facturacion_save['municipality_id'] = request("municipality_id_facturacion")[$key];
                   $facturacion_save['location_id'] = request("location_id_facturacion")[$key];
                   $facturacion_save['street'] = strtoupper(request("street_facturacion")[$key]);
                   $facturacion_save['n_ext'] = request("n_ext_facturacion")[$key];
                   $facturacion_save['n_int'] = request("n_int_facturacion")[$key];

                   $info->update($facturacion_save);
              }
         }

         Billing::where('contact_id', $contact->id )->whereNotIn('id', $existen)->delete();

         if(request("new_name_contacto")){
              // contacto secundario
              foreach (request("new_name_contacto") as $key => $contacto) {
                   $contacto_save['contact_id'] = $contact->id ;
                   $contacto_save['name'] = strtoupper($contacto);
                   $contacto_save['phone'] = request("new_phone_contacto")[$key];
                   $contacto_save['email'] = strtoupper(request("new_emai_contacto")[$key]);
                   InformationContact::create($contacto_save);
              }
         }

         if(request("new_business_name_facturacion")){
              // contacto secundario
              foreach (request("new_business_name_facturacion") as $key => $business_name) {
                   $facturacion_save['contact_id'] = $contact->id ;
                   $facturacion_save['business_name'] = strtoupper($business_name) ;
                   $facturacion_save['rfc'] = strtoupper(request("new_rfc_facturacion")[$key]);
                   // $facturacion_save['address'] = request("emai_contacto")[$key];
                   $facturacion_save['mail'] = strtoupper(request("new_mail_facturacion")[$key]);
                   $facturacion_save['phone'] = request("new_phone_facturacion")[$key];

                   $facturacion_save['postal_code'] = request("new_postal_code_facturacion")[$key];
                   $facturacion_save['state_id'] = request("new_state_id_facturacion")[$key];
                   $facturacion_save['municipality_id'] = request("new_municipality_id_facturacion")[$key];
                   $facturacion_save['location_id'] = request("new_location_id_facturacion")[$key];
                   $facturacion_save['street'] = request("new_street_facturacion")[$key];
                   $facturacion_save['n_ext'] = request("new_n_ext_facturacion")[$key];
                   $facturacion_save['n_int'] = request("new_n_int_facturacion")[$key];
                   Billing::create($facturacion_save);
              }
         }


         return Response::json($contact);

         // return redirect()->route('admin.user.index')->with('success', 'Administrador actualizado');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Contact $contact)
    {
     
         if ($contact->status == 1) {
              $data['status'] = 0;
              $mn = 'Usuario desactivado';
         }else {
              $data['status'] = 1;
              $mn = 'Usuario activado';
         }
         $contact->update( $data );
         // $user->delete();
         return back()->with('success', $mn);

    }

    public function code_postal($code_postal)
    {
         $datas = PostalCode::where('cp', $code_postal)->orderBy('colonia','ASC')->get();
         $states = array();
         $municipalitys = array();
         $locations = array();
         if ($datas) {
              foreach ($datas as $key => $value) {
                   $states[$value->estado_id] = $value->estado;
                   $municipalitys[$value->municipio_id] = $value->municipio;
                   $locations[$value->id] = $value->colonia;
              }
         }

         return array('estados' => $states, 'municipios' => $municipalitys, 'colonias' => $locations);
    }

    public function test()
    {
         echo "1";
         $user = Contact::where('id', 1)->first();
         dump(TaskWasCreated::dispatch($user, 'Tarea de prueba 2', '2020-12-17', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'));

    }

    public function contact_json(Contact $contact)
    {
         return Response::json($contact->load('direction_cp'));
    }
    
}
