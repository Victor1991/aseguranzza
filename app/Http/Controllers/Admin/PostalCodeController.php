<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use App\Rules\PostalCodeValidation;
use App\PostalCode;


class PostalCodeController extends Controller
{

     public function store(Request $request)
     {
          $data = $request->validate([
               'codigo_postal' => ['required','digits:5',new PostalCodeValidation($request)],
               'nombre_colonia' => 'required'
          ]);

          if(request('municipality_modal_id') == 'otro') {
               $data =  $request->validate([
                    'nombre_ciudad' => 'required'
               ]);
          }

          $data["estado_id"] = request("state_modal_id");
          $estado = PostalCode::where('estado_id', request("state_modal_id"))->first();
          $data["estado"] = $estado->estado;

          if (request("municipality_modal_id") == 'otro') {
               $max_mun = PostalCode::max('municipio_id');
               $data["municipio_id"] = $max_mun+1;
               $data["municipio"] = request("nombre_ciudad");
          }else {
               $postal_code = PostalCode::where('estado_id', request("state_modal_id"))->where('municipio_id', request("municipality_modal_id"))->first();
               $data["municipio_id"] = request("municipality_modal_id");
               $data["municipio"] = $postal_code->municipio;     
          }

          $data["colonia"] = request("nombre_colonia");
          $data["cp"] = request("codigo_postal");

          $postal_code =  PostalCode::create($data);

          return Response::json($postal_code);

     }

}
