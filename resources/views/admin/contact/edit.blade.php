@extends('admin.layout')

@section('content')
<div class="col-md-12 ml-auto mr-auto">
     <div class="card">
          <div class="card-header ">
               <h3 class="card-title">Editar contacto</h3>
          </div>
          <div class="card-body">

               <div id="demo">
                    <div class="step-app">
                         <ul class="step-steps">
                              <li><a href="#tab1"><span class="number">1</span> Contacto </a></li>
                              <li><a href="#tab2"><span class="number">2</span> Datos del contacto</a></li>
                              <!-- <li><a href="#tab3"><span class="number">2</span> Datos co3tratante</a></li> -->
                              <li><a href="#tab3"><span class="number">3</span> Documentación</a></li>
                         </ul>
                         <div class="step-content">
                              <div class="step-tab-panel" id="tab1">
                                   <form name="frmInfo" id="frmInfo">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PUT">
    
                                        <div class="row">
                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Tipo de persona *</label>
                                                       <select class="form-control" name="type_person_id"  id="type_person_id">
                                                            <option value="">-- Seleccione uno --</option>
                                                            <?php foreach ($type_persons as $key => $type_person) : ?>
                                                                 <option
                                                                 @if($user->type_person_id == $type_person->id) selected @endif
                                                                 value="<?=$type_person->id ?>"><?=$type_person->name ?></option>
                                                            <?php endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Perfil </label>
                                                       <select class="form-control" name="type_seller" required>
                                                            <option value="">-- Seleccione uno --</option>
                                                            <?php foreach ($types_sellers as $key => $types_seller) : ?>
                                                                 <option
                                                                 @if($user->type_seller == $types_seller->id) selected @endif
                                                                  value="<?=$types_seller->id ?>"><?=$types_seller->name ?></option>
                                                            <?php endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                            

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Nombre </label>
                                                       <input type="text" name="name" value="{{ $user->name }}"  class="form-control" required>
                                                  </div>
                                             </div>

                                             <div class="col-md-4 person">
                                                  <div class="form-group">
                                                       <label>Apellido paterno </label>
                                                       <input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" required>
                                                  </div>
                                             </div>

                                             <div class="col-md-4 person">
                                                  <div class="form-group">
                                                       <label>Apellido materno </label>
                                                       <input type="text" name="mother_last_name"  value="{{ $user->mother_last_name }}" class="form-control" >
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Correo *</label>
                                                       <input type="email" name="email" value="{{ $user->email }}" class="form-control" required>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>RFC </label>
                                                       <input type="text" name="rfc" value="{{ $user->rfc }}"  class="form-control" required >
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Fecha de nacimiento *</label>
                                                       <input type="text" name="birthdate" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthdate)->format('d-m-Y') }}" class="form-control datemask" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask required>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Sexo </label>
                                                       <select class="form-control" name="sex_id">
                                                            <option value="">-- Seleccione uno --</option>
                                                            <?php foreach ($sexs as $key => $sex) : ?>
                                                                 <option
                                                                 @if($user->sex_id == $sex->id) selected @endif
                                                                  value="<?=$sex->id ?>"><?=$sex->name ?></option>
                                                            <?php endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                             {{-- <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>CURP </label>
                                                       <input type="text" name="curp" value="{{ $user->curp }}" class="form-control" required >
                                                  </div>
                                             </div> --}}


                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Grupo de Asegurados </label>
                                                       <input type="text" class="form-control" name="insured_grouo" value="{{ old('insured_grouo', $user->insured_grouo) }}">
                                                  </div>
                                             </div>

                                             <!--<div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Ejecutivo </label>
                                                       <select name="executive_id" class="form-control">
                                                            @foreach ($users as $item)
                                                                 <option @if($user->executive_id == $item->id) selected @endif value="{{$item->id}}"> {{$item->name}} {{$item->last_name}} {{$item->mother_last_name}}</option> 
                                                            @endforeach
                                                       </select>
                                                  </div>
                                             </div>-->

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Empresa </label>
                                                       <input type="text" name="business" class="form-control"  value="{{ old('business', $user->business) }}">
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Ocupación </label>
                                                       <input type="text" name="occupation" class="form-control"  value="{{ old('occupation', $user->occupation) }}">
                                                  </div>
                                             </div>

                                             {{-- 
                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Número asignado </label>
                                                       <input type="text" name="assigned_number" class="form-control"  value="{{ old('assigned_number' , $user->assigned_number) }}">
                                                  </div>
                                             </div>
                                             --}}
                                        </div>
                                   </form>
                              </div>
                              <div class="step-tab-panel" id="tab2">
                                   <form name="frmForm" id="frmForm">
                                        <div class="row">

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>C.P.</label>
                                                       <div class="input-group mb-3">
                                                            <input type="text" name="postal_code" id="postal_code" value="{{ $user->postal_code }}" class="form-control" required onkeyup="get_postal_code()" >
                                                            <div class="input-group-append" >
                                                                 <span class=" btn btn-success"  data-toggle="modal" data-target="#modal-default"> <i class="fa fa-plus"></i> </span>
                                                            </div>
                                                       </div>

                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Estado</label>
                                                       <select class="form-control" name="state_id" id="state_id" required>
                                                            <?php foreach ($data_cp['estados'] as $key => $state) : ?>
                                                                 <option
                                                                 @if($user->state_id == $key) selected @endif
                                                                 value="<?=$key ?>"><?=$state ?></option>
                                                            <?php endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Ciudad</label>
                                                       <select class="form-control" name="municipality_id" id="municipality_id" onchange="get_localidad()" required>
                                                            <?php foreach ($data_cp['municipios'] as $key => $municipality) : ?>
                                                                 <option
                                                                 @if($user->municipality_id == $key) selected @endif
                                                                 value="<?=$key?>"><?=$municipality ?></option>
                                                            <?php endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Colonia</label>
                                                       <select class="form-control" name="location_id"  id="location_id" required>
                                                            <?php foreach ($data_cp['colonias'] as $key => $locations) : ?>
                                                                 <option
                                                                 @if($user->location_id == $key) 
                                                                      selected 
                                                                 @endif
                                                                 value="<?=$key ?>"><?=$locations ?></option>
                                                            <?php endforeach; ?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Calle</label>
                                                       <input type="text" name="street" value="{{ $user->street }}" class="form-control" required>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>No. Exterior</label>
                                                       <input type="text" name="n_ext" value="{{ $user->n_ext }}" class="form-control" required>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>No. Interior</label>
                                                       <input type="text" name="n_int" value="{{ $user->n_int }}" class="form-control">
                                                  </div>
                                             </div>


                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Teléfono fijo</label>
                                                       <input type="text" name="phone" value="{{ $user->phone }}" class="form-control">
                                                  </div>
                                             </div>
                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Teléfono móvil *</label>
                                                       <input type="text" name="cell_phone" value="{{ $user->cell_phone }}" class="form-control" required>
                                                  </div>
                                             </div>

                                             <div class="col-md-4">
                                                  <div class="form-group">
                                                       <label>Referencia de contacto</label>
                                                       <input type="text" name="reference" value="{{ $user->reference }}" class="form-control" required>
                                                  </div>
                                             </div>

                                        </div>
                                        <div class="row">
                                            
                                             {{-- <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Referencia </label>
                                                       <textarea name="reference_user" class="form-control" rows="3" cols="80">{{ $user->reference_user }}</textarea>
                                                  </div>
                                             </div> --}}
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label>Notas</label>
                                                       <textarea name="notes" class="form-control" rows="3" cols="80">{{ $user->notes }}</textarea>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-12">
                                                 <hr>
                                             </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-6">
                                                 <label>Contacto secundario</label>
                                             </div>
                                             <div class="col-md-6 text-right">
                                                  <button type="button" name="button" onclick="agregar_campo_contacto()" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Agregar</button>
                                             </div>
                                             <br>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-12" id="datos_contacto">
                                                  <?php foreach ($user->contactos as $key => $contactos): ?>
                                                       <div class="col-md-12">
                                                            <div class="row div_fac">
                                                                 <div class="col-md-4">
                                                                      <label for="">Nombre</label>
                                                                      <input type="text" name="name_contacto[{{$contactos->id}}]" class="form-control" value="{{$contactos->name}}" required>
                                                                 </div>
                                                                 <div class="col-md-4">
                                                                      <label for="">Correo</label>
                                                                      <input type="email" name="emai_contacto[{{$contactos->id}}]" class="form-control" value="{{$contactos->email}}" required>
                                                                 </div>
                                                                 <div class="col-md-4">
                                                                      <label for="">Teléfono</label>
                                                                      <input type="text" name="phone_contacto[{{$contactos->id}}]" class="form-control" value="{{$contactos->phone}}" required>
                                                                 </div>
                                                                 <div class="col-md-12">
                                                                      <button type="button" class="btn btn-danger eliminar_contacto"  style="margin-top: 10px; float:right;">Eliminar</button>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  <?php endforeach; ?>
                                             </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-12">
                                                  <hr>
                                             </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-6">
                                                 <label>Datos de facturación</label>
                                             </div>
                                             <div class="col-md-6 text-right">
                                                  <button type="button" name="button" onclick="agregar_campo_facturacion()" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Agregar</button>
                                             </div>
                                             <br>
                                        </div>
                                        <div class="row">
                                             <div class="col-md-12" id="datos_facturacion">
                                                  <?php foreach ($user->facturacion as $key => $facturacion): ?>
                                                       <div class="col-md-12">
                                                            <div class="row div_fac">
                                                                 <div class="col-md-3">
                                                                      <label for="">Razón social</label>
                                                                      <input type="text" name="business_name_facturacion[{{$facturacion->id}}]" class="form-control" value="{{$facturacion->business_name}}" required>
                                                                 </div>
                                                                 <div class="col-md-3">
                                                                      <label for="">R.F.C</label>
                                                                      <input type="test" name="rfc_facturacion[{{$facturacion->id}}]" class="form-control" value="{{$facturacion->rfc}}" required>
                                                                 </div>
                                                                 <div class="col-md-3">
                                                                      <label for="">Correo</label>
                                                                      <input type="email" name="mail_facturacion[{{$facturacion->id}}]" class="form-control" value="{{$facturacion->mail}}" required>
                                                                 </div>
                                                                 <div class="col-md-3">
                                                                      <label for="">Teléfono</label>
                                                                      <input type="text" name="phone_facturacion[{{$facturacion->id}}]" class="form-control" value="{{$facturacion->phone}}" required>
                                                                 </div>
                                                                 <div class="col-md-4">
                                                                      <label>C.P.</label>
                                                                      <input type="text" class="form-control" value="{{$facturacion->postal_code}}" name="postal_code_facturacion[{{$facturacion->id}}]"  id="cp_{{$facturacion->id}}" required onkeyup="get_postal_code_facturacion(this)" >
                                                                 </div>
                                                                 <div class="col-md-4">
                                                                      <div class="form-group">
                                                                           <label>Estado</label>
                                                                           <select class="form-control" name="state_id_facturacion[{{$facturacion->id}}]" id="state_id_{{$facturacion->id}}" required>
                                                                                <?php foreach ($facturacion->estados as $key => $estado): ?>
                                                                                     <option
                                                                                     @if($facturacion->state_id == $estado->estado_id) selected @endif
                                                                                     value="<?=$estado->estado_id?>"><?=$estado->estado ?></option>
                                                                                <?php endforeach; ?>
                                                                           </select>
                                                                      </div>
                                                                 </div>
                                                                 <div class="col-md-4">
                                                                      <div class="form-group">
                                                                           <label> Ciudad </label>
                                                                           <select class="form-control" name="municipality_id_facturacion[{{$facturacion->id}}]" id="municipality_id_{{$facturacion->id}}" required>
                                                                                <?php foreach ($facturacion->municipios as $key => $municipio): ?>
                                                                                     <option
                                                                                     @if($facturacion->municipality_id == $municipio->municipio_id) selected @endif
                                                                                     value="<?=$municipio->municipio_id?>"><?=$municipio->municipio ?></option>
                                                                                <?php endforeach; ?>
                                                                           </select>
                                                                      </div>
                                                                 </div>

                                                                 <div class="col-md-4">
                                                                      <div class="form-group">
                                                                           <label> Colonia </label>
                                                                           <select class="form-control" name="location_id_facturacion[{{$facturacion->id}}]" id="location_id_{{$facturacion->id}}" required>
                                                                                <?php foreach ($facturacion->colonias as $key => $colonia): ?>
                                                                                     <option
                                                                                     <?php if ($facturacion->location_id == $colonia->id) : ?> selected <?php endif; ?>
                                                                                     value="<?=$colonia->id?>"><?=$colonia->colonia ?></option>
                                                                                <?php endforeach; ?>
                                                                           </select>
                                                                      </div>
                                                                 </div>


                                                                 <div class="col-md-4">
                                                                      <div class="form-group">
                                                                           <label>Calle</label>
                                                                           <input type="text" name="street_facturacion[{{$facturacion->id}}]" value="{{$facturacion->street}}" class="form-control" required>
                                                                      </div>
                                                                 </div>

                                                                 <div class="col-md-4">
                                                                      <div class="form-group">
                                                                           <label>No. Exterior</label>
                                                                           <input type="text" name="n_ext_facturacion[{{$facturacion->id}}]" value="{{$facturacion->n_ext}}" class="form-control" required>
                                                                      </div>
                                                                 </div>

                                                                 <div class="col-md-4">
                                                                      <div class="form-group">
                                                                           <label>No. Interior</label>
                                                                           <input type="text" name="n_int_facturacion[{{$facturacion->id}}]" value="{{$facturacion->n_int}}" class="form-control">
                                                                      </div>
                                                                 </div>
                                                                 <div class="col-md-12">
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_contacto" style="margin-top: 10px; float:right;">Eliminar</button>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  <?php endforeach; ?>
                                             </div>
                                        </div>
                                   </form>
                              </div>
                              <div class="step-tab-panel" id="tab3">
                                   <form name="frmExpe" id="frmExpe">
                                        <div class="row">
                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label>Identificación Oficial</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="identification_official" id="identification_official">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->identification_official): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->identification_official}}" target="_blank"><span class="input-group-text ver_archivo" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_identification_official" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_identification_official" value="">
                                                            <?php endif; ?>

                                                                 <input type="text" class="form-control col-md-4 datemask" name="identification_official_validity"
                                                                 <?php if (!is_null($user->identification_official_validity) && $user->identification_official_validity != '0000-00-00'): ?>
                                                                      value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->identification_official_validity)->format('d-m-Y') }}"
                                                                 <?php else: ?>
                                                                      value=""
                                                                 <?php endif; ?>
                                                                 placeholder="vigencia" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>

                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="col-md-12">
                                                  <div class="form-group">
                                                       <label>Pasaporte</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="passport" id="passport">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->passport): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->passport}}" target="_blank"><span class="input-group-text ver_archivo" id="" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_passport" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_passport" value="">
                                                            <?php endif; ?>

                                                            <input type="text" class="form-control col-md-4 datemask" name="passport_validity"
                                                            <?php if (!is_null($user->passport_validity) && $user->passport_validity != '0000-00-00'): ?>
                                                                 value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->passport_validity)->format('d-m-Y') }}"
                                                            <?php else: ?>
                                                                 value=""
                                                            <?php endif; ?>
                                                            placeholder="vigencia" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Cédula Fiscal</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="card_fiscal" id="card_fiscal">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->card_fiscal): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->card_fiscal}}" target="_blank"><span class="input-group-text ver_archivo" id="" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_card_fiscal" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_card_fiscal" value="">
                                                            <?php endif; ?>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Comp. Domicilio</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="voucher_home" id="voucher_home">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->voucher_home): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->voucher_home}}" target="_blank"><span class="input-group-text ver_archivo" id="" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_voucher_home" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_voucher_home" value="">
                                                            <?php endif; ?>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Acta Constitutiva</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="minutes_constitutive" id="minutes_constitutive">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->minutes_constitutive): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->minutes_constitutive}}" target="_blank"><span class="input-group-text ver_archivo" id="" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_minutes_constitutive" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_minutes_constitutive" value="">
                                                            <?php endif; ?>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Poderes</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="powers" id="powers">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->powers): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->powers}}" target="_blank"><span class="input-group-text ver_archivo" id="" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_powers" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_powers" value="">

                                                            <?php endif; ?>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Folio mercantil</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="business_folio" id="business_folio">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->business_folio): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->business_folio}}" target="_blank"><span class="input-group-text ver_archivo" id="" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_business_folio" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_business_folio" value="">

                                                            <?php endif; ?>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="col-md-6">
                                                  <div class="form-group">
                                                       <label>Otros</label>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" class="custom-file-input" name="other" id="other">
                                                                 <label class="custom-file-label" for="exampleInputFile">Elija el archivo</label>
                                                            </div>
                                                            <?php if ($user->other): ?>
                                                                 <div class="input-group-append">
                                                                      <a href="{{$user->other}}" target="_blank"><span class="input-group-text ver_archivo" id="" > Archivo actual</span></a>
                                                                      <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_other" ><i class="fa fa-trash"></i> </button>
                                                                 </div>
                                                                 <input type="hidden" name="eliminar_other" value="">

                                                            <?php endif; ?>
                                                       </div>
                                                  </div>
                                             </div>

                                        </div>
                                   </form>
                              </div>
                         </div>
                         <div class="step-footer">
                              <button data-direction="prev" class="step-btn btn btn-warning">Anterior</button>
                              <button data-direction="next" class="step-btn btn btn-success">Siguiente</button>
                              <button data-direction="finish" class="step-btn btn btn-info">Guardar</button>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

@include('admin.postal_code.create_modal')


@stop
@push('scripts')
<script src="{{ asset('/js/wizard/jquery-steps.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="{{ asset('/js/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>

$(function() {
     ocultar_info_personal();  
    $('#type_person_id').on('change', function() {
          ocultar_info_personal();  
    });
});

function ocultar_info_personal() {
     if($('#type_person_id').val() == 2){
          $('.person').hide();
     }else{
          $('.person').show();
     }
}


$(document).on('click',".eliminar_contacto", function() {
     // var padre = $(this).remove();
      var padre = $(this).parent().parent().remove();
});

var frmInfo = $('#frmInfo');
var frmInfoValidator = frmInfo.validate();

var frmForm = $('#frmForm');
var frmFormValidator = frmForm.validate();

var frmExpe = $('#frmExpe');
var frmExpeValidator = frmExpe.validate();
$( document ).ready(function() {
     jQuery.extend(jQuery.validator.messages, {
          required: "Este campo es obligatorio."
     });

     // get_postal_code();

     //Datemask dd/mm/yyyy
     $('.datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

     $('#demo').steps({
          onChange: function (currentIndex, newIndex, stepDirection) {

               // step1
               if (currentIndex === 0) {
                    if (stepDirection === 'forward') {
                         return frmInfo.valid();
                    }
                    if (stepDirection === 'backward') {
                         frmInfoValidator.resetForm();
                    }
               }
               // step2
               if (currentIndex === 1) {
                    if (stepDirection === 'forward') {
                         return frmForm.valid();
                    }
                    if (stepDirection === 'backward') {
                         frmFormValidator.resetForm();
                    }
               }
               // step3
               if (currentIndex === 2) {
                    if (stepDirection === 'forward') {
                         return frmExpe.valid();
                    }
                    if (stepDirection === 'backward') {
                         frmExpeValidator.resetForm();
                    }
               }
               // step3
               if (currentIndex === 3) {
                    if (stepDirection === 'forward') {
                         return frmExpe.valid();
                    }
                    if (stepDirection === 'backward') {
                         frmExpeValidator.resetForm();
                    }
               }


               return true;
          },
          onFinish: function () {
               var data = new FormData();
               var forms =  $('#frmInfo,#frmForm,#frmExpe').serializeArray();
               forms.forEach(function(elemento) {
                    data.append(elemento.name, elemento.value);
               });

               var card_fiscal = $('#card_fiscal')[0].files[0];
               data.append("card_fiscal", card_fiscal);

               var identification_official = $('#identification_official')[0].files[0];
               data.append("identification_official", identification_official);

               var voucher_home = $('#voucher_home')[0].files[0];
               data.append("voucher_home", voucher_home);

               var minutes_constitutive = $('#minutes_constitutive')[0].files[0];
               data.append("minutes_constitutive", minutes_constitutive);

               var powers = $('#powers')[0].files[0];
               data.append("powers", powers);

               var passport = $('#passport')[0].files[0];
               data.append("passport", passport);

               var business_folio = $('#business_folio')[0].files[0];
               data.append("business_folio", business_folio);

               var other = $('#other')[0].files[0];
               data.append("other", other);

               $.ajax({
                    processData: false,
                    contentType: false,
                    type: "POST",
                    dataType: "json",
                    url: "{{ route('admin.contact.update', $user) }}",
                    // url: "/admin/contacts/{{ $user->id }}",
                    data: data, // serializes the form's elements.
                    success: function(result) {
                         swal({
                              title: "Éxito",
                              text: 'Registrado correctamente',
                              type: 'success',
                              confirmButtonColor  : "#32c5d2",
                              confirmButtonText   : "Ok",
                              allowOutsideClick: false
                         }).then(function() {
                              window.location = "{{ route('admin.contact.index') }}";
                         });
                    },
                    error:function(x,xs,xt){
                         var text = '';
                         var error_ret = x.responseJSON.errors;
                         $.each(error_ret, function(index, value) {
                              text += '<span class="strg">'+index + '</span> : '+ value[0] + '<br>';
                         });
                         swal({
                              title: "Error",
                              html: text,
                              type: 'error',
                         });
                    }
               });
          }
     });
});

function readURL(input) {
     console.log(input);
     console.log($(input).attr("id"));
     if (input.files && input.files[0]) {
          var reader = new FileReader();
          var filename = $(input).val(); //$("#inputGroupFile01").val();
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          console.log(filename);
          reader.onload = function(e) {
               $('#blah').attr('src', e.target.result);
               $('.custom-file-label').text(filename);

          }

          reader.readAsDataURL(input.files[0]); // convert to base64 string
     }
}



function get_municipios(sel){

     var id = 'state_id';
     var municipaly = 'municipality_id';
     if (sel.id != 'state_id') {
          id = 'state_modal_id';
          municipaly = 'municipality_modal_id';
     }
     $('#'+municipaly).html('');
     $.ajax({
          url: "{{ route('municipalitys') }}",
          data:{'state_id': $('#'+id).val()},
          type:'get',
          success:  function (response) {
               $.each(response, function (i, item) {
                    $('#'+municipaly).append($('<option>', {
                         value: item.municipio_id,
                         text : item.municipio
                    }));
               });
               if (sel.id != 'state_id') {
                    $('#'+municipaly).append('<option value="otro">Otro</option>');
               }
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}


function get_localidad(sel){
     var id = 'municipality_id';
     var municipaly = 'location_id';
     if (sel.id != 'municipality_id') {
          if (sel.value == 'otro') {
               $('#div_nueva_ciudad').show();
          }else {
               $('#div_nueva_ciudad').hide();
          }
          return false;
     }
}

function get_postal_code_facturacion(obj) {
     var id = obj.id.substring(4, 3);
     console.log(id);
     if (obj.value.length == 5) {
          $.ajax({
               url: "{{ route('code_postal') }}",
               data:{'code_postal': obj.value},
               type:'get',
               success:  function (response) {
                    console.log(response);
                    $('#state_id_'+id).html('');
                    $.each(response.estados, function (i, item) {
                         $('#state_id_'+id).append($('<option>', {
                              value: i,
                              text : item
                         }));
                    });
                    $('#municipality_id_'+id).html('');
                    $.each(response.municipios, function (i, item) {
                         $('#municipality_id_'+id).append($('<option>', {
                              value: i,
                              text : item
                         }));
                    });
                    $('#location_id_'+id).html('');
                    $.each(response.colonias, function (i, item) {
                         $('#location_id_'+id).append($('<option>', {
                              value: i,
                              text : item
                         }));
                    });
               },
               statusCode: {
                    404: function() {
                         alert('web not found');
                    }
               },
               error:function(x,xs,xt){
                    //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
               }
          });
     }
}

function get_postal_code() {
     if ($('#postal_code').val().length == 5) {
          $.ajax({
               url: "{{ route('code_postal') }}",
               data:{'code_postal': $('#postal_code').val()},
               type:'get',
               success:  function (response) {
                    $('#state_id').html('');
                    $.each(response.estados, function (i, item) {
                         $('#state_id').append($('<option>', {
                              value: i,
                              text : item
                         }));
                    });
                    $('#municipality_id').html('');
                    $.each(response.municipios, function (i, item) {
                         $('#municipality_id').append($('<option>', {
                              value: i,
                              text : item
                         }));
                    });
                    $('#location_id').html('');
                    $.each(response.colonias, function (i, item) {
                         $('#location_id').append($('<option>', {
                              value: i,
                              text : item
                         }));
                    });
               },
               statusCode: {
                    404: function() {
                         alert('web not found');
                    }
               },
               error:function(x,xs,xt){
                    //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
               }
          });
     }
}

$('.eliminar_archivo').on('click', function() {
     var id = $( this ).attr( "id" );
     $("input[name="+id+"]").val(1);
      var padre = $(this).parent().first().remove();
});



$('.custom-file-input').on('change', function() {
     var FileSize = this.files[0].size / 1024 / 1024; // in MiB
     if (FileSize > 2) {
          swal({
               title: "Error",
               html: 'El tamaño del archivo supera los 2 MB',
               type: 'error',
          });
          $(this).val(''); //for clearing with Jquery
     } else {
          let fileName = $(this).val().split('\\').pop();
          $(this).next('.custom-file-label').addClass("selected").html(fileName);
     }
});


function agregar_campo_contacto() {
     $( "#datos_contacto" ).append('<div class="col-md-12">'+
          '<div class="row div_fac">'+
               '<div class="col-md-4">'+
                    '<label for="">Nombre</label>'+
                    '<input type="text" name="new_name_contacto[]" class="form-control" value="" required>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label for="">Correo</label>'+
                    '<input type="email" name="new_emai_contacto[]" class="form-control" value="" required>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label for="">Teléfono</label>'+
                    '<input type="text" name="new_phone_contacto[]" class="form-control" value="" required>'+
               '</div>'+
               '<div class="col-md-12">'+
                    '<button type="button" class="btn btn-danger eliminar_contacto"  style="margin-top: 10px; float:right;">Eliminar</button>'+
               '</div>'+
          '</div>'+
     '</div>');
}

var n_fac = 1;
function agregar_campo_facturacion() {
     $( "#datos_facturacion" ).append('<div class="col-md-12">'+
          '<div class="row div_fac">'+
               '<div class="col-md-12">'+
                    '<button type="button" class="btn btn-primary btn-sm" onclick="copiar_datos('+n_fac+')">Copiar datos del contratante</button><br><br>'+
               '</div>'+
               '<div class="col-md-3">'+
                    '<label for="">Razón social</label>'+
                    '<input type="text" name="new_business_name_facturacion[]" class="form-control" id="business_'+n_fac+'" value="" required>'+
               '</div>'+
               '<div class="col-md-3">'+
                    '<label for="">R.F.C</label>'+
                    '<input type="test" name="new_rfc_facturacion[]" class="form-control" id="rfc_'+n_fac+'" value="" required>'+
               '</div>'+
               '<div class="col-md-3">'+
                    '<label for="">Correo</label>'+
                    '<input type="email" name="new_mail_facturacion[]" class="form-control"  id="email_'+n_fac+'" value="" required>'+
               '</div>'+
               '<div class="col-md-3">'+
                    '<label for="">Teléfono</label>'+
                    '<input type="text" name="new_phone_facturacion[]" class="form-control" value="" id="phone_'+n_fac+'" required>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label>C.P.</label>'+
                    '<input type="text" name="new_postal_code_facturacion[]" value="" class="form-control"  id="cp_'+n_fac+'"  required onkeyup="get_postal_code_facturacion(this)" >'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label>Estado</label>'+
                    '<select class="form-control" name="new_state_id_facturacion[]" id="state_id_'+n_fac+'" required>'+
                         '<option value="">-- Seleccione C.P. --</option>'+
                    '</select>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label>Ciudad</label>'+
                    '<select class="form-control" name="new_municipality_id_facturacion[]" id="municipality_id_'+n_fac+'" required>'+
                         '<option value="">-- Seleccione C.P. --</option>'+
                    '</select>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label>Colonia</label>'+
                    '<select class="form-control" name="new_location_id_facturacion[]"  id="location_id_'+n_fac+'" required>'+
                         '<option value="">-- Seleccione C.P. --</option>'+
                    '</select>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label>Calle</label>'+
                    '<input type="text" name="new_street_facturacion[]" value="" id="street_'+n_fac+'" class="form-control" required>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label>No. Exterior</label>'+
                    '<input type="text" name="new_n_ext_facturacion[]" value="" id="n_ext_'+n_fac+'" class="form-control" required>'+
               '</div>'+
               '<div class="col-md-4">'+
                    '<label>No. Interior</label>'+
                    '<input type="text" name="new_n_int_facturacion[]" value="" id="n_int_'+n_fac+'" class="form-control">'+
               '</div>'+
               '<div class="col-md-12">'+
                    '<button type="button" class="btn btn-danger btn-sm eliminar_contacto"style="margin-top: 10px; float:right;">Eliminar</button>'+
               '</div>'+
          '</div>'+
     '</div>');
     n_fac++;
}

function copiar_datos(id) {
     let rfc = $( "input[name*='rfc']" ).val();
     let email = $( "input[name*='email']" ).val();
     let phone = $( "input[name*='phone']" ).val();
     let cp = $( "#postal_code" ).val();
     let street = $( "input[name*='street']" ).val();
     let n_ext = $( "input[name*='n_ext']" ).val();
     let n_int = $( "input[name*='n_int']" ).val();
     let nm = $( "input[name*='name']" ).val()+' '+$( "input[name*='last_name']" ).val()+' '+$( "input[name*='mother_last_name']" ).val();

     $('#business_'+id).val(nm);
     $('#cp_'+id).val(cp);
     $('#rfc_'+id).val(rfc);
     $('#email_'+id).val(email);
     $('#phone_'+id).val(phone);
     $('#street_'+id).val(street);
     $('#n_ext_'+id).val(n_ext);
     $('#n_int_'+id).val(n_int);

     get_postal_code_facturacion($('#cp_'+id)[0]);     
     setTimeout( 
          function() {
               let location = $( "#location_id" ).val();
               let municipality = $( "#municipality_id" ).val();
               console.log('#location_id_'+id);
               console.log(location);
               $('#municipality_id_'+id).val(municipality);
               $('body #location_id_'+id).val(location);
          },3000);
}

// $(".custom-file-input").change(function() {
//      readURL(this);
// });

</script>
@endpush
@push('style')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery.steps@1.0.1/dist/jquery-steps.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<style >

input{ text-transform: uppercase; }
textarea{ text-transform: uppercase; }
.error{
     color: red;
}
.custom-file-input:lang(en) ~ .custom-file-label::after {
     content: 'navegar';
}
.strg {
    font-size: 20px;
    font-weight: 700;
    color: red;
}

.ver_archivo{
     background-color: #05a200;
     color: #fff;
}
.div_fac{
     padding: 10px;
border: 1px solid #9d9d9d;
margin-top: 20px;
margin-bottom: 20px;
border-radius: 5px;
background-color: #f0f0f0;
}

</style>
@endpush
