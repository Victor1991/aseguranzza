<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coverage extends Model
{
    protected $fillable = [
        'riesgo', 'suma','cuota', 'prima', 'policy_id'
   ];
}
