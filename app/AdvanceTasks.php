<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvanceTasks extends Model
{
     protected $fillable = [
        'task_id', 'user_id','description'
   ];

   public function creado_por()
   {
        return $this->belongsTo( User::class, 'user_id', 'id' );
   }
}
