<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta http-equiv="x-ua-compatible" content="ie=edge">

     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">

     <title>{{ config('app.name', 'Laravel') }}</title>


     <!-- Font Awesome Icons -->
     <link rel="stylesheet" href="{{ asset('/theme/plugins/fontawesome-free/css/all.min.css')}}">
     <!-- overlayScrollbars -->
     <link rel="stylesheet" href="{{ asset('/theme/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
     <!-- Theme style -->
     <link rel="stylesheet" href="{{ asset('/theme/dist/css/adminlte.min.css')}}">
     <!-- Google Font: Source Sans Pro -->
     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

     <link href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">

     @stack('style')

</head>
<body class="sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed control-sidebar-slide-open">
     <div class="wrapper">
          <!-- Navbar -->
          <nav class="main-header navbar navbar-expand navbar-dark navbar-primary">
               <!-- Left navbar links -->
               <ul class="navbar-nav">
                    <li class="nav-item">
                         <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
               </ul>

               <!-- Right navbar links -->
               <ul class="navbar-nav ml-auto">

                    <li class="nav-item dropdown">
                         <a class="nav-link" data-toggle="dropdown" href="#">
                              <i class="far fa-bell"></i>
                              <span class="badge badge-warning navbar-badge">0</span>
                         </a>
                         <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                              <span class="dropdown-item dropdown-header">0 Notifications</span>
                              <!-- <div class="dropdown-divider"></div>
                              <a href="#" class="dropdown-item">
                                   <i class="fas fa-users mr-2"></i> 8 friend requests
                                   <span class="float-right text-muted text-sm">12 hours</span>
                              </a>
                              <div class="dropdown-divider"></div>
                              <a href="#" class="dropdown-item">
                                   <i class="fas fa-file mr-2"></i> 3 new reports
                                   <span class="float-right text-muted text-sm">2 days</span>
                              </a>
                              <div class="dropdown-divider"></div>
                              <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a> -->
                         </div>
                    </li>
                    <li class="nav-item dropdown">
                         <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} {{ Auth::user()->last_name }} {{ Auth::user()->mother_last_name }} <span class="caret"></span>
                         </a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                              Cerrar la sesión
                         </a>

                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                         </form>
                    </div>
               </li>
          </ul>
     </nav>
     <!-- /.navbar -->

     <!-- Main Sidebar Container -->
     <aside class="main-sidebar elevation-4 sidebar-light-primary">
          <!-- Brand Logo -->
          <a href="#" class="brand-link text-center">
               <img src="https://aseguranzza.com/content/corporation/images/corporation.png" alt="AdminLTE Logo" class="brand-image elevation-3"
               style="opacity: .8; background-color:#fff;">
               <span class="brand-text font-weight-light">Aseguranzza</span>
          </a>

          <!-- Sidebar -->
          <div class="sidebar">
               <!-- Sidebar user panel (optional) -->
               <div class="user-panel mt-3 pb-3 mb-3 d-flex">

                    <div class="info">
                         <a href="#" class="d-block"> {{ Auth::user()->name }} {{ Auth::user()->last_name }} {{ Auth::user()->mother_last_name }} <span class="caret"></span></a>
                    </div>
               </div>

               @include('partials.nav')

          </div>
          <!-- /.sidebar -->
     </aside>

     <!-- Content Wrapper. Contains page content -->
     <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <div class="content-header">
               <div class="container-fluid">
                    <div class="row mb-2">
                         <div class="col-sm-6">
                              <h1 class="m-0 text-dark">Administrador de sistema</h1>
                         </div>
                         <!-- /.col -->
                         <!-- <div class="col-sm-6">
                              <ol class="breadcrumb float-sm-right">
                                   <li class="breadcrumb-item"><a href="#">Home</a></li>
                                   <li class="breadcrumb-item active">Dashboard v2</li>
                              </ol>
                         </div> -->
                         <!-- /.col -->
                    </div><!-- /.row -->
               </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->

          <!-- Main content -->
          <section class="content">
               <div class="container-fluid">

                    @include('partials.flash-message')

                    @yield('content')

               </div><!--/. container-fluid -->
          </section>
          <!-- /.content -->
     </div>
     <!-- /.content-wrapper -->


     <!-- Main Footer -->
     <footer class="main-footer">
          <strong>Copyright &copy; <?=date('Y')?> <a href="#">SumaWeb</a>.</strong>
          All rights reserved.
     </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('/theme/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{ asset('/theme/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('/theme/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/theme/dist/js/adminlte.js')}}"></script>



<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('/theme/plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{ asset('/theme/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/jquery-mapael/maps/usa_states.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{ asset('/theme/plugins/chart.js/Chart.min.js')}}"></script>

<script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

<script>
$.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});

$(document).ready( function () {
     $('.table_js').DataTable({
          language: {
               url: "{{ asset('/js/Spanish.json')}}"
          }
     });
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- PAGE SCRIPTS -->
@stack('scripts')

</body>
</html>
