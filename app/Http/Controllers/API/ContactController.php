<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Contact;


class ContactController extends Controller
{
    //

    public function contact(Request $request)
    {
        $contact = Contact::where('id', $request->id)->first();
        return response()->json($contact->load('direction_cp'));

    }
}
