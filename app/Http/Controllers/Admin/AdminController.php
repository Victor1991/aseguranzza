<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Task;
use App\InsuredGroup;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


use App\Mail\Contac;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller{

     public function index(){
          $user = User::all();
          $tasks = Task::all();
          $insured_groups = InsuredGroup::all();
          $id_expires = User::identification_official_expire();
          $passport_expires = User::passport_expire();
          return view('admin.dashboard', compact('user', 'tasks', 'id_expires', 'passport_expires', 'insured_groups'));
     }

     public function send_mail(Request $request)
     {
          $correo = request("correo");
          $subject = request("subject");
          $correo_oculto = request("correo_oculto") ? explode(',',request("correo_oculto")) : "" ;
          $content = request("content");
          $file = "";

          if ($request->file('file') != null) {
               $file_path = $request->file('file')->store('public');     
               $file = Storage::url($file_path);
          }
         
          Mail::to($correo)->send(new Contac($correo, $subject, $correo_oculto, $content, $file ));
   
          return redirect()->route('admin.user.index')->with('success', 'Correo enviado correctamente');
          
     } 

}
