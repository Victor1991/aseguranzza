<?php

namespace App\Rules;
use App\PostalCode;

use Illuminate\Contracts\Validation\Rule;

class PostalCodeValidation implements Rule
{
     public $request;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
         // dump(request("state_modal_id"));
         //
         // $data =  PostalCode::where('estado_id', request("state_modal_id") )->where( 'municipio_id',request("municipio_id"))->where('colonia',request("colonia"))->get();
         // dump($data);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
         $postal_code = PostalCode::query();
         $postal_code->where('cp', request("codigo_postal"));
         $postal_code->where('estado_id',  request("state_modal_id"));

         if (request("municipality_modal_id") != 'otro' ) {
              $postal_code->where('municipio_id',  request("municipality_modal_id"));
         }else {
              $postal_code->where('municipio',   'like', '%' . request('nombre_ciudad') . '%');
         }

         $postal_code->where('colonia',   'like', '%' . request('nombre_colonia') . '%');

         $data = $postal_code->first();
         if (is_null($data)) {
              return true;
         }else {
              return false;
         }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El :attribute ya existe con esa información';
       }
}
