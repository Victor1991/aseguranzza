<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = [
        'name', 'user_id','end_date', 'description', 'status', 'imagen', 'creator_user', 'user_termination_date', 'user_pending_date', 'coment_pending', 'coment_termination', 'num_folio', 'contact_id'
   ];


   public function creado_por()
   {
        return $this->belongsTo( User::class, 'creator_user', 'id' );
   }

   public function asignado()
   {
        return $this->belongsTo( User::class, 'user_id', 'id' );
   }

   public function avances()
   {
        return $this->hasMany( AdvanceTasks::class );
   }

   public function estatus()
  {
       return $this->belongsTo( StatusTask::class, 'status', 'id' );
  }

  public function contacto()
  {
       return $this->belongsTo( Contact::class, 'contact_id', 'id' );
  }
}
