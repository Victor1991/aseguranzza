<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilesPolicys extends Model
{
    //
    protected $fillable = [
        'policie_id', 'name', 'file', 'created_by'
    ];

    public function creado_por()
    {
      return $this->hasOne( User::class, 'id', 'created_by' );
      
    }
}
