<!-- Modal -->
<div class="modal fade" id="modal_notificacion" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2a7cf0; color: #fff;">
                <h5 class="modal-title">Notificación contacto<span></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row" style="padding: 10px;">
                <div class="col-md-12 text-right">
                    <button class="btn btn-success btn-sm" onclick="ver_modal_notificacion_form()"> <i class="fa fa-plus" aria-hidden="true"></i> Agregar </button>
                    <br>
                    <br>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Mensaje</th>
                                <th>Creador</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table_notificacion">
                            <tr>
                                <td colspan="3" class="text-center">
                                    <i class='fas fa-spinner fa-spin fa-3x'></i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        let policie_id = '';
        function ver_modal_notificacion(id) {
            policie_id = id;
            $('#table_notificacion').html(`<tr>
                                                <td colspan="3" class="text-center">
                                                    <i class='fas fa-spinner fa-spin fa-3x'></i>
                                                </td>
                                            </tr>`);
            $.ajax({
                url : "{{ route('admin.notification_policy.index') }}",
                data : { id : id },
                type : 'GET',
                dataType : 'json',
                success : function(json) {
                   if(json.length > 0){
                    $('#table_notificacion').html('');
                        json.forEach(function(elemento) {
                            let file = elemento.file ? `<a href="${elemento.file}" target="_blank" class="btn btn-sm btn-info"> <i class="fa fa-file" aria-hidden="true"></i> </a>` : '';
                            let btn_del =`<button onclick="eli_not(${elemento.id})" class="btn btn-sm btn-danger"> <i class="fa fa-trash" aria-hidden="true"></i> </button>`;
                            $( "#table_notificacion" ).append( `<tr>
                                                                    <td>${moment(elemento.created_at).format('MM/DD/YYYY')}</td>
                                                                    <td>${elemento.message}</td>
                                                                    <td>${elemento.creado_por.name} ${elemento.creado_por.last_name} ${elemento.creado_por.mother_last_name}</td>
                                                                    <td>${file} ${btn_del}</td>
                                                                </tr>` );
                        });
                   }else{
                        $('#table_notificacion').html(`<tr>
                                                <td colspan="3" class="text-center">
                                                    Sin resultados
                                                </td>
                                            </tr>`);
                   }
                },
                error : function(jqXHR, status, error) {
                    alert('Disculpe, existió un problema');
                },
                complete : function(jqXHR, status) {
                    $('#load_notificion').hide();
                }
            });
            $('#modal_notificacion').modal('show');
        }

        function eli_not(id){
            swal({
                title: "Eliminar",
                text: "¿ Desea eliminar ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Eliminar',
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function(res) {
                if(res.value){
                    $.ajax({
                        type: "DELETE",
                        dataType: "json",
                        url: "{{ url('admin/notification_policy/') }}/"+id,
                        // url: "/admin/contacts/{{ $user->id }}",
                        success: function(result) {
                            swal({
                                title: "Éxito",
                                text: 'Se elimino correctamente',
                                type: 'success',
                                confirmButtonColor  : "#32c5d2",
                                confirmButtonText   : "Ok",
                                allowOutsideClick: false
                            }).then(function() {
                                ver_modal_notificacion(policie_id);
                            });
                        },
                        error:function(x,xs,xt){
                            var text = '';
                            var error_ret = x.responseJSON.errors;
                            $.each(error_ret, function(index, value) {
                                text += '<span class="strg">'+index + '</span> : '+ value[0] + '<br>';
                            });
                            swal({
                                title: "Error",
                                html: text,
                                type: 'error',
                            });
                        }
                    });
                }
            });
        }
    </script>

@endpush
@push('style')

@endpush
