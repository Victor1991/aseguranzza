<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformationContact extends Model
{
    //
    protected $fillable = [
        'user_id', 'name','phone', 'email'
   ];
}
