@component('mail::message')
# Tus credenciales para acceder a {{ config('app.name') }}

Utiliza estas credenciales para acceder al sistema.

@component('mail::table')
     | Tarea |  Fecha de entrega |
     |:----------|:------------|
     | {{ $name }} | {{ $date }} |

@endcomponent
@component('mail::table')
     | Descripción |
     |:----------|
     |{{ $description }} |
@endcomponent

Saludos,<br>
{{ config('app.name') }}
@endcomponent
