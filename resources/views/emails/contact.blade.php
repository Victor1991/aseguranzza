@component('mail::message')

{!! $content !!}

{{ config('app.name') }}
@endcomponent
