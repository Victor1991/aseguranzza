<!-- Sidebar Menu -->
<nav class="mt-2">
     <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->

          <li class="nav-item">
               <a href="{{ route('dashboard') }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                         Dashboard
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.user.index') }}" class="nav-link">
                    <i class="nav-icon fa fa-users" aria-hidden="true"></i>
                    <p>
                         Usuarios
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.contact.index') }}" class="nav-link">
                    <i class="nav-icon fa fa-address-card" aria-hidden="true"></i>
                    <p>
                         Contactos
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.task.index') }}" class="nav-link">
                    <i class="nav-icon far fa-calendar-alt"></i>
                    <p>
                         Tareas
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.type_policys.index') }}" class="nav-link">
                    <i class="nav-icon fa fa-archive" aria-hidden="true"></i>
                    <p>
                         Polizas
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.requisition.index') }}" class="nav-link">
                    <i class="nav-icon fa fa-file" aria-hidden="true"></i>
                    <p>
                         Solicitud
                    </p>
               </a>
          </li>
          <!-- <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                         Página
                         <i class="fas fa-angle-left right"></i>
                    </p>
               </a>
               <ul class="nav nav-treeview" >
                    <li class="nav-item">
                         <a href="../layout/top-nav.html" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Top Navigation</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="../layout/top-nav-sidebar.html" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Top Navigation + Sidebar</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="../layout/boxed.html" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Boxed</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="../layout/fixed-sidebar.html" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Fixed Sidebar</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="../layout/fixed-topnav.html" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Fixed Navbar</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="../layout/fixed-footer.html" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Fixed Footer</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="../layout/collapsed-sidebar.html" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Collapsed Sidebar</p>
                         </a>
                    </li>
               </ul>
          </li>
          <li class="nav-item">
               <a href="pages/widgets.html" class="nav-link">
                    <i class="nav-icon fas fa-cog"></i>
                    <p>
                         Configuraciones
                    </p>
               </a>
          </li> -->

     </ul>
</nav>
<!-- /.sidebar-menu -->
