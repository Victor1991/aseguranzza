<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\TypePolicys;


class TypePolicysController extends Controller
{
    public function index()
     {
          $policys = TypePolicys::all();
          return view('admin.type_policys.index', compact('policys'));
     }
}
