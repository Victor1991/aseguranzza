@extends('admin.layout')

@section('content')
    <div class="col-md-12 ml-auto mr-auto">
        <div class="card">
            <div class="card-header ">
                <h3 class="card-title">Registrar contacto</h3>
            </div>
            <div class="card-body">
                <div id="demo">
                    <div class="step-content">
                        <div class="step-tab-panel" id="tab1">
                            <form method="post" id="form_contacto" enctype="multipart/form-data" action="{{ route('admin.user.store') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nombre(s) </label>
                                            <input type="text" name="name" value="{{ old('name') }}" class="form-control"
                                                required>
                                            @if ($errors->has('name'))
                                                <span class="error" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Apellido paterno </label>
                                            <input type="text" name="last_name" value="{{ old('last_name') }}"
                                                class="form-control" required>
                                            @if ($errors->has('last_name'))
                                                <span class="error" role="alert">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Apellido materno </label>
                                            <input type="text" name="mother_last_name"
                                                value="{{ old('mother_last_name') }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Correo *</label>
                                            <input type="email" name="email" value="{{ old('email') }}"
                                                class="form-control" required>
                                            @if ($errors->has('email'))
                                                <span class="error" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Fecha de nacimiento *</label>
                                            <input type="text" name="birthdate" value="{{ old('birthdate') }}"
                                                class="form-control datemask" data-inputmask-alias="datetime"
                                                data-inputmask-inputformat="dd/mm/yyyy" data-mask required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Sexo </label>
                                            <select class="form-control" name="sex_id">
                                                <option value="">-- Seleccione uno --</option>
                                                <?php foreach ($sexs as $key => $sex): ?>
                                                <option @if (old('sex_id') == $sex->id) selected @endif value="<?= $sex->id ?>"><?= $sex->name ?></option>
                                                          <?php endforeach; ?>
                                                     </select>
                                                </div>
                                           </div>

                                           <div class="col-md-4">
                                                <div class="form-group">
                                                     <label>Teléfono fijo</label>
                                                     <input type="text" name="phone" value="{{ old('phone') }}" class="form-control">
                                                </div>
                                           </div>

                                           <div class="col-md-4">
                                                <div class="form-group">
                                                     <label>Teléfono móvil *</label>
                                                     <input type="text" name="cell_phone" value="{{ old('cell_phone') }}"   class="form-control" required>
                                                </div>
                                           </div>

                                           <div class="col-md-4">
                                                 <label>Contraseña</label>
                                                 <input id="password-field" type="password" class="form-control" name="password" >
                                                 <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            </div>
                                      </div>  
                                      <div class="row">
                                           <div class="col-md-6">
                                                 <a href="{{ route('admin.user.index') }}" class="btn btn-default"> <i class="fa fa-return" aria-hidden="true"></i> Regresar </a>
                                           </div>
                                           <div class="col-md-6 text-right">
                                                 <button type="submit" class="btn btn-success"> <i class="fa fa-save" aria-hidden="true"></i> Agregar </button>
                                           </div>
                                       </div>                                              
                                 </form>
                            </div>
                       </div>
                  </div>
             </div>
        </div>
    </div>

    @include('admin.postal_code.create_modal')


@stop
@push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
         <script src="{{ asset('/js/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
         <script>
              $( document ).ready(function() {
                   $('.datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

              });
              $(".toggle-password").click(function() {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
                    if (input.attr("type") == "password") {
                         input.attr("type", "text");
                    } else {
                         input.attr("type", "password");
                    }
                });


                let user_id = "";

                $('#form_contacto').on('submit',function(e){
                    e.preventDefault();
                    $( ".error" ).remove();

                    var form = $(this);
                    let data = form.serializeArray();;
                    let actionUrl = form.attr('action');
                    
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: actionUrl,
                        data: data, // serializes the form's elements.
                        success: function(result) {
                            user_id = result.id;
                            swal({
                                title: 'Se registró correctamente',
                                text: "¿ Deseas realizar una acción ? ",
                                type: 'success',
                                showCancelButton: true,
                                confirmButtonColor: '#1e4da3',
                                cancelButtonColor: '#156929',
                                confirmButtonText: 'Asignar una tarea',
                                cancelButtonText: 'Regresar al listado'
                            }).then(function(eve){
                                if(eve.value == true){
                                    window.location.href = "./../../admin/task/create?user_id="+user_id;
                                }else{
                                    window.location.href = "./../../admin/user";
                                }
                            });
                        },
                        error:function(x,xs,xt){
                            let error_ret = x.responseJSON.errors;
                            $.each(error_ret, function(index, value) {
                                $('[name="'+index+'"]').after("<div class='error'>"+value+"</div>");
                            });
                        }
                    });

                    

                });
         </script>
@endpush
@push('style')
    <style>
         .error{
              color:red;
              margin-top: 5px;
              font-size: 13px;
         }
         .field-icon {
               float: right;
               margin-right: 10px;
               margin-top: -27px;
               position: relative;
               z-index: 2;
          }
    </style>
@endpush
