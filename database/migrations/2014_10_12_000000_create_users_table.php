<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_person_id');
            $table->integer('promoter_id');
            $table->string('name');
            $table->string('last_name');
            $table->string('mother_last_name')->nullable();
            $table->string('rfc')->nullable();
            $table->date('birthdate');
            $table->string('sex_id')->nullable();
            $table->string('curp')->nullable();
            $table->string('type_seller')->nullable();
            $table->longText('reference_user')->nullable();


            $table->integer('state_id');
            $table->integer('municipality_id');
            $table->integer('location_id');
            $table->integer('postal_code')->nullable();
            $table->string('street')->nullable();
            $table->string('n_ext');
            $table->string('n_int')->nullable();
            $table->string('reference')->nullable();
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('cell_phone');

            $table->string('card_fiscal')->nullable();
            $table->string('identification_official')->nullable();
            $table->string('voucher_home')->nullable();
            $table->string('minutes_constitutive')->nullable();
            $table->string('powers')->nullable();
            $table->string('business_folio')->nullable();
            $table->string('other')->nullable();


            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
