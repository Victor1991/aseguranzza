<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Task extends Mailable
{
     public $user;
     public $name;
     public $date;
     public $description;

     use Queueable, SerializesModels;

     /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($user, $name, $date, $description)
     {
          $this->user = $user;
          $this->name = $name;
          $this->date = $date;
          $this->description = $description;
     }

     /**
     * Build the message.
     *
     * @return $this
     */
     public function build()
     {
          return $this->markdown('emails.task')->subject('Tarea registra (Sistema Aseguranzza)');
     }
}
