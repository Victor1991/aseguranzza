<div class="modal fade" id="modal-default">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-header">
                    <h4 class="modal-title">Agregra dirección C.P.</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                    </button>
               </div>
               <form  action="{{ route('admin.postalcode.store') }}" method="post" id="FormCP">
                    <div class="modal-body">
                         <div class="row">
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>C.P.</label>
                                        <input class="form-control" type="text" name="codigo_postal" value="">
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Estado</label>
                                        <select class="form-control" name="state_modal_id" id="state_modal_id" onchange="get_municipios(this)" required>
                                             <?php foreach ($states as $key => $state) : ?>
                                                  <option value="<?=$state->estado_id ?>"><?=$state->estado ?></option>
                                             <?php endforeach; ?>
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Ciudad</label>
                                        <select class="form-control" name="municipality_modal_id" id="municipality_modal_id" onchange="get_localidad(this)" required>
                                             <?php foreach ($municipalitys as $key => $municipality) : ?>
                                                  <option value="<?=$municipality->municipio_id ?>"><?=$municipality->municipio ?></option>
                                             <?php endforeach; ?>
                                             <option value="otro">Otro</option>
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-12" id="div_nueva_ciudad" style="display:none;">
                                   <div class="form-group">
                                        <label>Nombre Ciudad</label>
                                        <input class="form-control" type="text" name="nombre_ciudad" id="nombre_ciudad" value="" >
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Colonia</label>
                                        <input class="form-control" type="text" name="nombre_colonia" value="">
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                         <button type="submit" class="btn btn-primary">Guardar dirección</button>
                    </div>
               </form>
          </div>
          <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
</div>
@push('scripts')
<script>
$("#FormCP").submit(function(e) {

     e.preventDefault(); // avoid to execute the actual submit of the form.

     var form = $(this);
     var url = form.attr('action');

     $.ajax({
          type: "POST",
          url: url,
          data: form.serialize(), // serializes the form's elements.
          success: function(result) {
               console.log(result);
               swal({
                    title: "Éxito",
                    text: 'Registrado correctamente',
                    type: 'success',
                    confirmButtonColor  : "#32c5d2",
                    confirmButtonText   : "Ok",
                    allowOutsideClick: false
               }).then(function() {
                    $('#FormCP')[0].reset();
                    $('#modal-default').modal('hide');
               });
          },
          error:function(x,xs,xt){
               var text = '';
               var error_ret = x.responseJSON.errors;
               $.each(error_ret, function(index, value) {
                    text += value[0] + '<br>';
               });
               swal({
                    title: "Error",
                    html: text,
                    type: 'error',
               });
          }
     });


});

</script>
@endpush
