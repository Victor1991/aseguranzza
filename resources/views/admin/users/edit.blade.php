@extends('admin.layout')

@section('content')
    <div class="col-md-12 ml-auto mr-auto">
        <div class="card">
            <div class="card-header ">
                <h3 class="card-title">Editar usuarios</h3>
            </div>
            <div class="card-body">

                <div id="demo">
                    <div class="step-app">

                        <div class="step-content">
                            <div class="step-tab-panel" id="tab1">
                                <form method="post" enctype="multipart/form-data" action="{{ route('admin.user.update', $user) }}">
                                   {{ csrf_field() }} {{ method_field('PUT') }}

                                   <div class="row">



                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nombre(s) </label>
                                                <input type="text" name="name" value="{{ $user->name }}"
                                                    class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Apellido paterno </label>
                                                <input type="text" name="last_name" value="{{ $user->last_name }}"
                                                    class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Apellido materno </label>
                                                <input type="text" name="mother_last_name"
                                                    value="{{ $user->mother_last_name }}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Correo *</label>
                                                <input type="email" name="email" value="{{ $user->email }}"
                                                    class="form-control" required>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Fecha de nacimiento *</label>
                                                <input type="text" name="birthdate"
                                                    value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthdate)->format('d-m-Y') }}"
                                                    class="form-control datemask" data-inputmask-alias="datetime"
                                                    data-inputmask-inputformat="dd/mm/yyyy" data-mask required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sexo </label>
                                                <select class="form-control" name="sex_id">
                                                    <option value="">-- Seleccione uno --</option>
                                                    <?php foreach ($sexs as $key => $sex): ?>
                                                    <option @if ($user->sex_id == $sex->id) selected @endif value="<?= $sex->id ?>"><?= $sex->name ?></option>
                                                                <?php endforeach; ?>
                                                              </select>
                                                                  </div>
                                                             </div>

                                                             <div class="col-md-4">
                                                                  <div class="form-group">
                                                                       <label>Teléfono fijo</label>
                                                                       <input type="text" name="phone" value="{{ $user->phone }}" class="form-control">
                                                                  </div>
                                                             </div>
                                                             <div class="col-md-4">
                                                                  <div class="form-group">
                                                                       <label>Teléfono móvil *</label>
                                                                       <input type="text" name="cell_phone" value="{{ $user->cell_phone }}" class="form-control" required>
                                                                  </div>
                                                            </div>   
                                                            
                                                            <div class="col-md-4">
                                                                 <div class="form-group">
                                                                      <label> Contraseña </label>
                                                                      <input type="password" id="password-field" name="password" value="" class="form-control">
                                                                      <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                                </div>
                                                           </div>   
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                        <a href="{{ route('admin.user.index') }}" class="btn btn-default"> <i class="fa fa-return" aria-hidden="true"></i> Regresar </a>
                                                  </div>
                                                  <div class="col-md-6 text-right">
                                                        <button type="submit" class="btn btn-success"> <i class="fa fa-save" aria-hidden="true"></i> Agregar </button>
                                                  </div>
                                              </div>    
                                           </form>
                                         </div>
                                      
                                    </div>
                               </div>
                          </div>
                     </div>
                </div>
              </div>
                @include('admin.postal_code.create_modal')


@stop
@push('scripts')

                <script src="{{ asset('/js/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>

                <script>
                $( document ).ready(function() {

                     $('.datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

                });

                $(".toggle-password").click(function() {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
                    if (input.attr("type") == "password") {
                         input.attr("type", "text");
                    } else {
                         input.attr("type", "password");
                    }
                });
                </script>
@endpush
@push('style')
                <style>
                     .error{
                          color:red;
                          font-size: 13px;
                     }
                     .field-icon {
                        float: right;
                        margin-right: 10px;
                        margin-top: -27px;
                        position: relative;
                        z-index: 2;
                    }
                </style>
@endpush
