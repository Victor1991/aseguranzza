<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
     /**
     * Seed the application's database.
     *
     * @return void
     */
     public function run()
     {
          $this->call(UsersSeeder::class);
          $this->call(TypePersonSeeder::class);
          $this->call(SexSeeder::class);
          $this->call(TypeSellerSeeder::class);
     }
}
