@extends('admin.layout')

@section('content')
<div class="col-md-8 ml-auto mr-auto">
     <form class="form" method="post" enctype="multipart/form-data" action="{{ route('admin.task.store') }}">
          {{ csrf_field() }}

          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Registrar tarea</h3>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name">Folio</label>
                                             <input type="text" class="form-control" value="AZ-{{ ++$folio_max }}" readonly>
                                             <input type="hidden" name="num_folio" class="form-control" value="{{ $folio_max }}">
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name">asignado a:</label>
                                             <select class="form-control" name="user_id">
                                                  <?php foreach ($users as $key => $user): ?>
                                                       <option 
                                                       <?php if (old('user_id', $task->user_id) == $user->id ): ?>
                                                            selected
                                                       <?php endif; ?>
                                                       value="{{$user->id}}">{{$user->name}} {{$user->last_name}} {{$user->mother_last_name}}</option>
                                                  <?php endforeach; ?>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name"> Fecha asignación:</label>
                                            <input type="text" name="" disabled class="form-control" value="<?=date('d-m-Y')?>">
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name"> Contacto :</label>
                                             <select class="form-control" name="contact_id">
                                                  <?php foreach ($contacts as $key => $contact): ?>
                                                       <option value="{{$contact->id}}">{{$contact->name}} {{$contact->last_name}} {{$contact->mother_last_name}}</option>
                                                  <?php endforeach; ?>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name">Nombre de la tarea</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre">
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name">Fecha de entrega</label>
                                             <input type="text" name="end_date" class="form-control datemask" value="{{ old('end_date') }}" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask >
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Descripción</label>
                                             <textarea name="description" rows="4" cols="80" class="form-control">{{ old('description') }}</textarea>
                                        </div>
                                   </div>
                              </div>
                              <div class="row justify-content-center">
                                   <div class="col-md-10 ">
                                        <div class="form-group">
                                             <label for="name">Imagen</label>
                                             <div class="row">
                                                  <div class="col-md-12 div_img">
                                                       <div id='img_container'><img id="preview" src="https://i.pinimg.com/originals/3a/ab/e0/3aabe0e9a520b9ad90407a82f85adb42.jpg" alt="your image"  class="img-fluid" title=''/></div>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" name="imagen" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01" accept="image/*">
                                                                 <label class="custom-file-label" for="inputGroupFile01">Elija el archivo</label>
                                                            </div>
                                                       </div>
                                                  </div>

                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right">Agregar tarea</button>
                    <a href="{{ route('admin.task.index') }}" class="btn btn-default btn-fill float-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>

@stop
@push('scripts')
<script src="{{ asset('/js/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('/js/bootstrap-datepicker.es.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

<script>
// $( document ).ready(function() {
//      $('.datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
// });

var date = new Date();
date.setDate(date.getDate());
$('.datemask').datepicker({
     format: "dd-mm-yyyy",
     autoclose: true,
     todayHighlight: true,
     startDate: date,
     language: "es"
});


$("#inputGroupFile01").change(function(event) {
     let uploadFile = this.files[0];
     if (!(/\.(jpg|png|gif)$/i).test(uploadFile.name)) {
          swal({
               title: "Archivo no aceptado",
               html: 'El solo se aceptan imagenes',
               type: 'error',
          });
        $('#inputGroupFile01').val('');
    }else{
          RecurFadeIn();
          readURL(this);
    }
    
});
function readURL(input) {
     if (input.files && input.files[0]) {
          var reader = new FileReader();
          var filename = $("#inputGroupFile01").val();
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          reader.onload = function(e) {
               debugger;
               $('#preview').attr('src', e.target.result);
               $('#preview').hide();
               $('#preview').fadeIn(500);
               $('.custom-file-label').text(filename);
          }
          reader.readAsDataURL(input.files[0]);
     }
     $(".alert").removeClass("loading").hide();
}
function FadeInAlert(text){
     $(".alert").show();
     $(".alert").text(text).addClass("loading");
}
function RecurFadeIn(){
     console.log('ran');
     FadeInAlert("Wait for it...");
}
</script>
@endpush
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<style media="screen">

.alert{
     text-align:center;
}

#preview{
     max-height:256px;
     height:auto;
     width:auto;
     display:block;
     margin-left: auto;
     margin-right: auto;
     padding:5px;
}
#img_container{
     border-radius:5px;
     width:auto;
}

.imgInp{
     width:150px;
     margin-top:10px;
     padding:10px;
     background-color:#d3d3d3;
}
.loading{
     animation:blinkingText ease 2.5s infinite;
}
@keyframes blinkingText{
     0%{     color: #000;    }
     50%{   color: #transparent; }
     99%{    color:transparent;  }
     100%{ color:#000; }
}
.custom-file-label{
     cursor:pointer;
}

.div_img{
     background-color: #eee;
     border-radius: 5px;
     padding: 10px;
}
.custom-file-input:lang(en) ~ .custom-file-label::after {
    content: "Navegar";
}
</style>
@endpush
