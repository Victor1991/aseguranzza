<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostalCode extends Model
{
     public $timestamps = false;
    //
    protected $fillable = [
        'estado_id', 'estado','municipio_id', 'municipio', 'ciudad', 'colonia', 'cp'
   ];
}
