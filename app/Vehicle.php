<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    
    protected $fillable = [
        'descripcion_vehiculo',
        'marca_vehiculo',
        'submarca_vehiculo',
        'modelo_vehiculo',
        'cobertura_vehiculo',
        'nmotor_vehiculo',
        'nserie_vehiculo',
        'nplacas_vehiculo',
        'uso_vehiculo',
        'beneficiario_vehiculo',
        'conductor_vehiculo',
        'tipo_carga_vehiculo',
        'adaptaciones_vehiculo',
        'policy_id' 
    ];
}
