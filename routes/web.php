<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Exports\UsersExport;
use App\Exports\TasksExport;
use App\Task;

Route::get('/', function () {
    return view('welcome');
});

// Reportes Excel
Route::get('excel', function () {
     return (new UsersExport)->download('users.xlsx');
});

Route::get('task_excel', function () {
     return (new TasksExport)->download('tareas.xlsx');
})->name('task.excel');

Route::get('table', function () {
     $tasks = Task::orderBy('created_at', 'asc')->get();
     return view('exports.tasks', compact('tasks'));
});


Auth::routes();

Route::get('municipios', 'CatJsonController@municipalitys')->name('municipalitys');

Route::get('localidades', 'CatJsonController@locations')->name('locations');

Route::get('code_postal', 'CatJsonController@code_postal')->name('code_postal');

Route::get('test', 'Admin\UserController@test')->name('test');


Route::group([
     'prefix' => 'admin',
     'namespace' => 'Admin',
     'middleware' => 'auth'],
function(){
     Route::get('/', 'AdminController@index')->name('dashboard');
     Route::post('/send_mail', 'AdminController@send_mail')->name('admin.send_mail');
     Route::resource('user', 'UserController', ['as' => 'admin']);
     Route::resource('contact', 'ContacController', ['as' => 'admin']);
     Route::resource('postalcode', 'PostalCodeController', ['as' => 'admin']);
     Route::resource('task', 'TaskController', ['as' => 'admin']);
     Route::resource('type_policys', 'TypePolicysController', ['as' => 'admin']);
     Route::resource('requisition', 'RequisitionController', ['as' => 'admin']);
     Route::resource('notification_policy', 'NotificationPolicyController', ['as' => 'admin']);
     Route::resource('files_policys', 'FilesPolicysController', ['as' => 'admin']);

     Route::group(['prefix' => '{type_policy}'], function($group_id){
          Route::resource('policys', 'PolicysController', ['as' => 'admin']);
     });
     
     Route::post('user/{user}', 'UserController@update');
     Route::post('task/finish_task', 'TaskController@finish_task')->name('admin.task.finish_task');
     Route::post('task/advance_task', 'TaskController@advance_task')->name('admin.task.advance_task');
     Route::get('user/user_json/{user}', 'UserController@user_json')->name('admin.user.user_json');

});
