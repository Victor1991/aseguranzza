<?php

use App\TypeSeller;
use Illuminate\Database\Seeder;

class TypeSellerSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
          TypeSeller::truncate();
          $fisica = new TypeSeller;
          $fisica->name = 'vendedor';
          $fisica->save();

          $fisica = new TypeSeller;
          $fisica->name = 'contratante';
          $fisica->save();
     }
}
