<?php

namespace App\Listeners;

use App\Events\TaskWasCreated;
use App\Mail\Task;

use Illuminate\Support\Facades\Mail;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTask
{

     /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
     public function handle(TaskWasCreated $event)
     {

          Mail::to($event->user)->queue(
               new Task($event->user, $event->name, $event->date, $event->description )
          );
     }
}
