<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eventos</h5>
                <a type="button" class="btn btn-success btn-sm" target="_black" href="<?=url('/admin/requisition')?>">
                    <i class="fas fa-plus"></i> Endoso
                </a>
            </div>
            <div class="modal-body" style="padding:0px;">
                <div class="row"  id="table_modal">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Número Endoso</th>
                                    <th>Creado Por</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="table_endosos">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" id="load_modal">
                    <div class="col-md-12 text-center" style="padding: 15px;">
                        <i class="fas fa-circle-notch fa-spin fa-5x"></i>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info push-right" onclick="recargar()"> 
                    <i class="fa fa-retweet" aria-hidden="true"></i>
                    Recargar 
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#exampleModal').on('show.bs.modal', event => {
        var button = $(event.relatedTarget);
        var modal = $(this);
        // Use above variables to manipulate the DOM
        
    });
</script>