<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
          User::truncate();
          $admin = new User;
          $admin->name = 'Victor Hugo';
          $admin->last_name = 'Uribe';
          $admin->mother_last_name = 'Hernández';
          $admin->birthdate = '2020-08-06';
          $admin->phone = '5566557755';
          $admin->cell_phone = '5566557755';
          $admin->type_person_id = '0';
          $admin->promoter_id = '0';
          $admin->state_id = '0';
          $admin->municipality_id = '0';
          $admin->location_id = '0';
          $admin->postal_code = '0';
          $admin->n_ext = '0';
          $admin->email = 'vic.hug.urib@gmail.com';
          $admin->password = bcrypt('12345678');
          $admin->save();
     }
}
