<!-- Modal -->
<div class="modal fade" id="modal_created" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2a7cf0; color: #fff;">
                <h5 class="modal-title">Nuevo endoso <span></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" enctype="multipart/form-data" action="{{ route('admin.requisition.store') }}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 m-t-20">
                            <label for="">Solicitud de endoso</label>
                            <select class="form-control" name="policy_id" id="mod_policy" required>
                                    <option value=""> Selecciona una poliza </option>
                                    @foreach ($policys as $_policy)
                                        <option data-count=" {{ $_policy->endosos->count() }}" value="{{ $_policy->id }}">{{$_policy->request_number}}</option>
                                    @endforeach
                            </select>
                        </div>
                       
                        <div class="col-md-12">
                            <label for="">Num de endoso</label>
                            <input type="text" name="num" id="mod_num" value="" class="form-control"  required readonly>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('style')

@endpush
@push('scripts')
    <script>
        $('#mod_policy').change(function(){
            if($(this).find(':selected').val() != ""){
                let num = $(this).find(':selected').attr('data-count');
                num++;
                $('#mod_num').val(num);
            }else{
                $('#mod_num').val('');
            }
        });
    </script>

@endpush
