<?php

namespace App\Exports;

use App\Task;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TasksExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    private $date;

    public function View(): View
    {
         return view('exports.tasks',[
              'tasks' => Task::get()
         ]);
    }
}
