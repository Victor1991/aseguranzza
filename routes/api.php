<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('contact', 'API\ContactController@contact')->name('api.contact');
Route::post('policys/upload_csv', 'API\PolicysController@upload_csv')->name('api.policys.upload_csv');
Route::get('get_requisition_poilce', 'API\RequisitionController@get_requisition_poilce')->name('get_requisition_poilce');
Route::get('full_calendar_task', 'API\TaskController@full_calendar_task')->name('full_calendar_task');