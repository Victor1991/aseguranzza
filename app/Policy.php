<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    //
    protected $fillable = [
      "type_policys_id", "request_number", "year", "contratante", "titulo_contratante", "sex_id_contratante", "ncliente_contratante", "tcontacto_contratante", "ncobro_contratante", "colonia_contratante", 
      "delegacion_contratante", "cp_1_contratante", "entidad_contratante", "zona_contratante", "tdomicilio_contratante", "tel_1_contratante", "tel_2_contratante", "fax_contratante", 
      "celular_contratante", "nota_contratante", "email_contratante", "fnacimiento_contratante", "curp_contratante", "rfc_contratante", "ocupacion_contratante", "actividad_contratante", 
      "apoderado_contratante", "parentesco_contratante", "cgurnamental_contratante", "origen_contratante", "calle_contratante", "ciudad_contratante", "estado_contratante", "pais_contratante", 
      "cp_2_contratante", "telefonos_contratante", "director_contratante", "ggenral_contratante", "ageneral_contratante", "contratante_asegurado", "titulo_asegurado", "sex_id_asegurado", 
      "ncliente_asegurado", "tcontacto_asegurado", "ncobro_asegurado", "colonia_asegurado", "delegacion_asegurado", "articulo_contratante", "nacionalidad_contratante", "acumpleanis_contratante",
      "nacionalidad_contratante", "tpersona_contratante", "articulo_asegurado",
      "cp_1_asegurado", "entidad_asegurado", "zona_asegurado", 
      "tdomicilio_asegurado", "tel_1_asegurado", "tel_2_asegurado", "fax_asegurado", "celular_asegurado", "nota_asegurado", "email_asegurado", "fnacimiento_asegurado", 
      "curp_asegurado", "rfc_asegurado", "ocupacion_asegurado", "actividad_asegurado", "apoderado_asegurado", "parentesco_asegurado", "cgurnamental_asegurado", 
      "origen_asegurado", "calle_asegurado", "ciudad_asegurado", "estado_asegurado", "pais_asegurado", "cp_2_asegurado", "telefonos_asegurado", "director_asegurado", 
      "ggenral_asegurado", "ageneral_asegurado", "clave", "descripcion_vehiculo", "capacidad_vehiculo", "modelo_vehiculo", "cobertura_vehiculo", "color_vehiculo", 
      "nmotor_vehiculo", "nserie_vehiculo", "renave_vehiculo", "nplacas_vehiculo", "uso_vehiculo", "beneficiario_vehiculo", "conductor_vehiculo", "servicio_vehiculo", 
      "adaptaciones_vehiculo", "vigencia_inicial", "vigencia_final", "vencimiento_1_recibo", "mrechazo", "titular", "ntarjeta", "vencimiento", "banco", "caseguradora", 
      "nanual", "comisiont", "pcomicion", "gasegurados", "nfolio_i", "ejecutivo", "fenvio", "frecepcion", "especificaciones", "type_car"
    ];

    public function coverturas()
    {
      return $this->hasMany( Coverage::class );
      
    }

    public function vehiculos()
    {
      return $this->hasMany( Vehicle::class );
      
    }

    public function asegurado()
    {
      return $this->hasOne( Contact::class, 'id', 'contratante' );
      
    }

    public function endosos()
    {
           return $this->hasMany( Requisition::class, 'policy_id', 'id' );   
    }
    
    public function vencimiento($date)
    {
      $now = time(); // or your date as well
      $your_date = strtotime($date);
      $datediff = $your_date - $now;
      return round($datediff / (60 * 60 * 24));
    }

  }
