<?php

namespace App\Http\Controllers;

use App\Municipality;
use App\Location;
use App\PostalCode;
use Response;

use Illuminate\Http\Request;

class CatJsonController extends Controller
{

     public function municipalitys(Request $request)
     {
          $state_id = $request->get('state_id');
          if (!is_array($state_id)) {
               $data = PostalCode::where('estado_id', $state_id)->groupBy('municipio_id')->get();
          }else {
               $data = PostalCode::whereIn('estado_id', $state_id)->groupBy('municipio_id')->get();
          }

          return Response::json($data);
     }

     public function locations(Request $request)
     {
          $municipality_id = $request->get('municipality_id');

          $estado_id = $request->get('estado_id');
          if (!is_array($estado_id)) {
               $data = PostalCode::where('municipio_id', $municipality_id)->orderBy('nombre','ASC')->get();
          }else{
               $data = PostalCode::whereIn('municipio_id', $municipality_id)->orderBy('nombre','ASC')->get();
          }
          return Response::json($data);
     }

     public function code_postal(Request $request)
     {
          $code_postal = $request->get('code_postal');
          $datas = PostalCode::where('cp', $code_postal)->orderBy('colonia','ASC')->get();
          $states = array();
          $municipalitys = array();
          $locations = array();
          if ($datas) {
               foreach ($datas as $key => $value) {
                    $states[$value->estado_id] = $value->estado;
                    $municipalitys[$value->municipio_id] = $value->municipio;
                    $locations[$value->id] = $value->colonia;
               }
          }
          $return = array('estados' => $states, 'municipios' => $municipalitys, 'colonias' => $locations);
          return Response::json($return);


     }
}
