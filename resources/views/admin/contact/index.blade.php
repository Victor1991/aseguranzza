@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card ">
          <div class="card-header ">
               <h3 class="card-title">Contactos</h3>
               <div class="card-tools">
                    <a href="{{ route('admin.contact.create') }}" class="btn btn-success btn-sm">
                         <i class="fa fa-plus"></i> Nuevo
                    </a>
               </div>
          </div>
          <div class="card-body table-responsive">
               <div class="row">
                    <div class="col-md-4"> 
                         <div class="bg-lightblue ui-draggable ui-draggable-handle text-center" style="background-color: #2959b3 !important; padding: 5px; border-radius: 4px;">
                              <div class="icheck-success d-inline">
                                   <input type="radio" id="checkboxPrimary1" name="type" value="all" class="radio" 
                                   @if(is_null($type) || $type == "all")
                                        checked
                                   @endif>
                                   <label for="checkboxPrimary1" style="margin-bottom: 0px; margin-left: 10px;">
                                        Todoso
                                   </label>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-4"> 
                         <div class="bg-lightblue ui-draggable ui-draggable-handle text-center" style="background-color: #3ea556 !important; padding: 5px; border-radius: 4px;">
                              <div class="icheck-success d-inline">
                                   <input type="radio" id="checkboxPrimary2" name="type" value="1" class="radio"
                                   @if($type == "1")
                                        checked
                                   @endif>
                                   <label for="checkboxPrimary2" style="margin-bottom: 0px; margin-left: 10px;">
                                        Activos
                                   </label>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-4"> 
                         <div class="bg-lightblue ui-draggable ui-draggable-handle text-center" style="background-color: #af444e !important; padding: 5px; border-radius: 4px;">
                              <div class="icheck-success d-inline">
                                   <input type="radio" id="checkboxPrimary3" name="type" value="2" class="radio"
                                   @if($type == "2")
                                        checked
                                   @endif>
                                   <label for="checkboxPrimary3" style="margin-bottom: 0px; margin-left: 10px;">
                                        Inactivos
                                   </label>
                              </div>
                         </div>
                    </div>
               </div>
               <br>
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Grupo de asegurados</th>
                              <th>Teléfono</th>
                              {{-- <th>Correo</th> --}}
                              <th>Referencia de contacto</th>
                              <th>Polizas</th>
                              <th class="text-center">WhatsApp</th>
                              {{-- <th>Perfil Usuario</th> --}}
                              <th class="text-center">Enviar correo</th>
                              <th class="text-center"><i class="fas fa-cog"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($users as $user)
                         <tr>
                              <td>{{ $user->id }}</td>
                              <td>{{ $user->name }} {{ $user->last_name }} {{ $user->mother_last_name }}</td>
                              <td>
                                   {{ $user->insured_grouo }}
                              </td>                            
                              <td>{{ $user->cell_phone }}</td>
                              {{-- <td>{{ $user->email }}</td> --}}
                              <td>{{ $user->reference }}</td>
                              <td>
                                   @if (count($user->polizas) > 0)
                                        @foreach ($user->polizas as $polizas)
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                             <button class="btn btn-sm btn-default">
                                                  <i onclick="ver_modal_notificacion('{{ $polizas->id }}')" class="text-info fas fa-calendar"></i>
                                             </button>
                                             <a href="{{ url('admin/' . $polizas->type_policys_id . '/policys/' . $polizas->id . '') }}" target="_blank" class="btn btn-sm btn-default">
                                                  <i class="text-danger fas fa-file-pdf"></i>
                                                  {{ $polizas->request_number }}
                                             </a>
                                             @if ($polizas->vencimiento($polizas->vigencia_final) > 0)
                                                  @if ($polizas->vencimiento($polizas->vigencia_final) <= 30)
                                                       <button class="btn btn-sm btn-default">
                                                            <i class="text-warning fas fa-exclamation-triangle"></i>
                                                       </button>
                                                  @endif    
                                             @endif
                                             <button onclick="ver_modal_archivos('{{ $polizas->id }}', '{{ $user->cell_phone }}')" class="btn btn-sm btn-default">
                                                  <i class="fa fa-upload" aria-hidden="true"></i>
                                             </button>
                                        </div>
                                        @endforeach
                                   @endif
                              </td>
                              <td class="text-center">
                                   <?php if ($user->cell_phone): ?>
                                        <a href="https://api.whatsapp.com/send?phone=+52{{ $user->cell_phone }}" target="_blank" class="btn btn-success btn-sm"><i class="fab fa-whatsapp"></i></a>
                                   <?php endif; ?>
                              </td>
                              {{-- <td>{{ $user->perfil->name }}</td> --}}
                              <td class="text-center">
                                   <button type="button" class="btn btn-secondary btn-sm" onclick="ver_modal_mail('{{ $user->email }}')">
                                        <i class="fas fa-envelope-open-text"></i>
                                   </button>
                              </td>
                              <td class="text-center">

                                   <a href="{{ route('admin.contact.edit', $user) }}"
                                        class="btn btn-sm btn-info btn-outline"
                                   ><i class="fas fa-user-edit"></i></a>

                                   <form method="POST"
                                        action="{{ route('admin.contact.destroy', $user) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <?php if ($user->status == 1): ?>
                                             <button class="btn btn-sm btn-success btn-outline"
                                                  onclick="return confirm('¿ Estás seguro de querer desactivar este contacto ?')"
                                             >
                                                  <i class="fas fa-toggle-on"></i>
                                             </button>
                                        <?php else: ?>
                                             <button class="btn btn-sm btn-danger btn-outline"
                                                  onclick="return confirm('¿ Estás seguro de querer activar este contacto ?')"
                                             >
                                                  <i class="fas fa-toggle-off"></i>
                                             </button>
                                        <?php endif; ?>

                                   </form>
                                   @if ($user->tareas_asignadas_no_finalizadas->count() > 0)
                                        <button type="button" class="btn btn-primary btn-sm">
                                             TP <span class="badge badge-danger ml-2">{{ $user->tareas_asignadas_no_finalizadas->count() }}</span>
                                        </button>
                                   @endif
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>
@include('admin.contact.modal_mail')
@include('admin.notification_policys.index')
@include('admin.notification_policys.form')
@include('admin.contact.modal_files')
@stop
@push('scripts')
<script>
     $(document).ready(function() {
       $('.radio').click(function () {
         let url = window.location.origin + '/admin/contact?type='+ $(this).val();
         window.location = url;

       });

   });
</script>

@endpush
