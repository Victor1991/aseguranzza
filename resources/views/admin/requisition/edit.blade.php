@extends('admin.layout')

@section('content')
    <div class="col-md-12 ml-auto mr-auto">
        <div class="card">
            <div class="card-header ">
                <h3 class="card-title">Endoso</h3>
            </div>
            <div class="card-body">

                <div id="demo">
                    <div class="step-app">

                        <div class="step-content">
                            <div class="step-tab-panel" id="tab1">
                                <form method="post" enctype="multipart/form-data" action="{{ route('admin.requisition.update', $requisition) }}">
                                   {{ csrf_field() }} {{ method_field('PUT') }}

                                   <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                          <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"> Datos del Endoso </a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Información Adicional</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Datos Técnicos</a>
                                        </li>
                                      </ul>
                                      <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                             <div class="row">

                                                  <div class="col-md-6">
                                                      <div class="form-group">
                                                          <label >Solicitud de Endoso</label>
                                                          <select disabled name="endorsement_request" class="form-control">
                                                                 @foreach ($policys as $req)
                                                                      <option value="{{$req['id']}}" 
                                                                      @if(old('endorsement_request', $requisition->endorsement_request) == $req['id']  )
                                                                          selected
                                                                      @endif>{{$req['request_number']}}</option>
                                                                 @endforeach
                                                          </select>
                                                      </div>
                                                  </div>
          
                                             </div>
          
                                             <hr>
                                             <div class="row">
          
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Tipo de Endoso</label>
                                                            <div class="row">
                                                                 @foreach ($type_endorsements as $type)
                                                                      <div class="col-md-4">
                                                                           <label>
                                                                           <input type="radio" name="type_endorsement_id" value="<?=$type['id']?>"
                                                                           @if(old('type_endorsement_id', $requisition->type_endorsement_id) == $type['id']  )
                                                                                checked
                                                                           @endif>
                                                                           <?=$type['option']?>
                                                                           </label>
                                                                      </div>
                                                                 @endforeach
                                                            </div>
                                                       </div>
                                                  </div>
                                              
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Forma de Pago</label>
                                                            <select name="way_pay_id" class="form-control">
                                                                 @foreach ($way_pays as $type)
                                                                      <option
                                                                      @if(old('way_pay_id', $requisition->way_pay_id) == $type['id']  )
                                                                          selected
                                                                      @endif value="{{$type['id']}}">{{$type['option']}}</option>
                                                                 @endforeach
                                                            </select>
                                                       </div>
                                                  </div>
          
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Moneda</label>
                                                            <input type="text"  value="{{ old('currency', $requisition->currency) }}" name="currency"class="form-control"
                                                            placeholder="Moneda">
                                                       </div>
                                                  </div>
          
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Vigencia Inicial</label>
                                                            <input type="text"  value="{{ old('initial_validity', $requisition->initial_validity) ? \Carbon\Carbon::parse( old('initial_validity', $requisition->initial_validity) )->format('d-m-Y') : '' }}" autocomplete="off" name="initial_validity" class="form-control datetimepicker-input" id="datetimepicker1" data-toggle="datetimepicker" data-target="#datetimepicker1">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Vigencia Final</label>
                                                            <input type="text"  value="{{ old('final_validity', $requisition->final_validity) ? \Carbon\Carbon::parse( old('final_validity', $requisition->final_validity) )->format('d-m-Y') : '' }}" autocomplete="off" name="final_validity" class="form-control datetimepicker-input" id="datetimepicker2" data-toggle="datetimepicker" data-target="#datetimepicker2" >
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Vencimiento Primer Recibo</label>
                                                            <input type="text"  value="{{ old('receipt_expiration', $requisition->receipt_expiration) ? \Carbon\Carbon::parse( old('receipt_expiration', $requisition->receipt_expiration) )->format('d-m-Y') : '' }}" autocomplete="off" name="receipt_expiration" class="form-control datetimepicker-input" id="datetimepicker3" data-toggle="datetimepicker" data-target="#datetimepicker3" >
                                                       </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                       <h5>Primas</h5>
                                                       <hr>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Prima Neta Anual</label>
                                                            <input type="text"  value="{{ old('yearly_fee', $requisition->yearly_fee) }}" name="yearly_fee" class="form-control"
                                                            placeholder="Pendiente">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Primera Parcialidad</label>
                                                            <input type="text"  value="{{ old('first_partiality', $requisition->first_partiality) }}" name="first_partiality" class="form-control"
                                                            placeholder="Pendiente">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Moneda Comisión</label>
                                                            <input type="text" placeholder="Nacional"  value="{{ old('commission_currency', $requisition->commission_currency) }}" name="commission_currency
                                                            "class="form-control">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Comisión Total</label>
                                                            <input type="text" value="{{ old('total_commission', $requisition->total_commission) }}" name="total_commission" class="form-control"
                                                            placeholder="0.00">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Porcentaje de Comisión</label>
                                                            <input type="text"  value="{{ old('commission_percentage', $requisition->commission_percentage) }}" name="commission_percentage" class="form-control"
                                                            placeholder="0.00000">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                       <hr>
                                                  </div>
                                                  <div class="col-md-12">
                                                       <div class="form-group">
                                                            <label >Movimientos</label>
                                                            <input type="text"  value="{{ old('movements', $requisition->movements) }}" name="movements" class="form-control"
                                                            >
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Fecha de Envío</label>
                                                            <input type="text"  value="{{ old('shipping_date', $requisition->shipping_date) ? \Carbon\Carbon::parse( old('shipping_date', $requisition->shipping_date) )->format('d-m-Y') : '' }}"  autocomplete="off"  name="shipping_date" class="form-control datetimepicker-input" id="datetimepicker4" data-toggle="datetimepicker" data-target="#datetimepicker4" >
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="tab-pane fade " id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                             <div class="row">
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Clave del Vehículo</label>
                                                            <input type="text"  value="{{ old('vehicle_key', $requisition->vehicle_key) }}" name="vehicle_key" class="form-control"
                                                            placeholder="Clave del Vehículo">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                       <div class="form-group">
                                                            <label >Descripción del Vehículo</label>
                                                            <input type="text"  value="{{ old('vehicle_description', $requisition->vehicle_description) }}" name="vehicle_description" class="form-control"
                                                            placeholder="Descripción del Vehículo">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Marca</label>
                                                            <input type="text"  value="{{ old('brand', $requisition->brand) }}" name="brand"class="form-control"
                                                            placeholder="Marca/Sub Marca">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Sub Marca</label>
                                                            <input type="text"  value="{{ old('sub_brand', $requisition->sub_brand) }}" name="sub_brand"class="form-control"
                                                            placeholder="Sub Marca">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Capacidad</label>
                                                            <input type="text"  value="{{ old('capacity', $requisition->capacity) }}" name="capacity"class="form-control"
                                                            placeholder="Capacidad">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Modelo</label>
                                                            <input type="text"  value="{{ old('model', $requisition->model) }}" name="model" class="form-control"
                                                            placeholder="Modelo">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Cobertura</label>
                                                            <input type="text"  value="{{ old('coverage', $requisition->coverage) }}" name="coverage"class="form-control"
                                                            placeholder="Cobertura">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Color</label>
                                                            <input type="text"  value="{{ old('colour', $requisition->colour) }}" name="colour"class="form-control"
                                                            placeholder="Color">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Nº de motor</label>
                                                            <input type="text"  value="{{ old('engine_no', $requisition->engine_no) }}" name="engine_no"class="form-control"
                                                            placeholder="Nº de motor">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Nº de serie</label>
                                                            <input type="text"  value="{{ old('serial_number', $requisition->serial_number) }}" name="serial_number" class="form-control"
                                                            placeholder="Nº de serie">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Re.Na.Ve./Re.Pu.Ve</label>
                                                            <input type="text"  value="{{ old('re_na_ve', $requisition->re_na_ve) }}" name="re_na_ve"class="form-control"
                                                            placeholder="Re.Na.Ve./Re.Pu.Ve">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Número de placas</label>
                                                            <input type="text"  value="{{ old('number_plates', $requisition->number_plates) }}" name="number_plates"class="form-control"
                                                            placeholder="Número de placas">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Uso del Vehículo</label>
                                                            <input type="text"  value="{{ old('vehicle_use', $requisition->vehicle_use) }}" name="vehicle_use"class="form-control"
                                                            placeholder="Uso del Vehículo">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Beneficiario preferente</label>
                                                            <input type="text"  value="{{ old('preferred_beneficiary', $requisition->preferred_beneficiary) }}" name="preferred_beneficiary" class="form-control"
                                                            placeholder="Beneficiario preferente">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Conductor habitual</label>
                                                            <input type="text"  value="{{ old('regular_driver', $requisition->regular_driver) }}" name="regular_driver" class="form-control"
                                                            placeholder="Conductor habitual">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Servicio</label>
                                                            <input type="text"  value="{{ old('service', $requisition->service) }}" name="service"class="form-control"
                                                            placeholder="Servicio">
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Tipo de carga</label>
                                                            <select name="type_load" class="form-control">
                                                                 @foreach ($type_load as $type)
                                                                      <option
                                                                      @if(old('type_load', $requisition->type_load) == $type['id']  )
                                                                          selected
                                                                      @endif value="{{$type['id']}}">{{$type['option']}}</option>
                                                                 @endforeach
                                                            </select>
                                                       </div>
                                                  </div>
                                                 
                                                  <div class="col-md-6">
                                                       <div class="row">
                                                            <div class="col-md-6 text-center">
                                                                 <div class="form-group">
                                                                      <label >¿Doble Remolque?</label>
                                                                      <input type="checkbox" value="1" @if($requisition->double_trailer == "1") checked @endif name="double_trailer"class="form-control"
                                                                      placeholder="¿Doble Remolque?">
                                                                 </div>
                                                            </div>
                                                            <div class="col-md-6 text-center">
                                                                 <div class="form-group">
                                                                      <label >¿Con Carga?</label>
                                                                      <input type="checkbox" value="1"  @if($requisition->with_load == "1") checked @endif name="with_load"class="form-control"
                                                                      placeholder="¿Con Carga?">
                                                                 </div>
                                                            </div>
                                                           
                                                       </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Equipo Especial</label>
                                                            <input type="text"  value="{{ old('special_team', $requisition->special_team) }}" name="special_team"class="form-control"
                                                            placeholder="Equipo Especial">
                                                       </div>
                                                  </div>
                                                  
                                                  <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label >Adaptaciones</label>
                                                            <input type="text"  value="{{ old('adaptations', $requisition->adaptations) }}" name="adaptations"class="form-control"
                                                            placeholder="Adaptaciones">
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                             <div class="row">
                                                  <div class="col-md-12">
                                                       <div class="form-group">
                                                            <textarea name="technical_data" class="form-control" cols="30" rows="10">{{ old('technical_data', $requisition->technical_data) }}</textarea>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                             <div class="col-md-12">
                                                  <hr>
                                             </div>
                                             <div class="col-md-6">
                                                  <a href="{{ route('admin.requisition.index') }}" class="btn btn-default btn-fill">
                                                       <i class="fas fa-refresh"></i>
                                                       Regresar
                                                  </a>
                                             </div>    
                                             <div class="col-md-6 text-right">
                                                  <button type="submit" class="btn btn-success">
                                                       <i class="fas fa-save"></i> Guardar</button>
                                             </div>
                                      </div>
                                  
                                  
                                 

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/js/tempusdominus-bootstrap-4.min.js" integrity="sha512-k6/Bkb8Fxf/c1Tkyl39yJwcOZ1P4cRrJu77p83zJjN2Z55prbFHxPs9vN7q3l3+tSMGPDdoH51AEU8Vgo1cgAA==" crossorigin="anonymous"></script>
{{-- <script src="{{ asset('/js/bootstrap-datepicker.es.min.js')}}"></script> --}}

<script>
     let option = {
          format: 'DD-MM-YYYY',
          locale: 'es'
     }
     $( document ).ready(function() {
          // var date = new Date();
          // date.setDate(date.getDate());
          // $('.datemask').datepicker({
          //      format: "dd-mm-yyyy",
          //      autoclose: true,
          //      todayHighlight: true,
          //      startDate: date,
          //      language: "es"
          // });
          $('#datetimepicker1').datetimepicker( option );
          $('#datetimepicker2').datetimepicker( option );
          $('#datetimepicker3').datetimepicker( option );
          $('#datetimepicker4').datetimepicker( option );

     });  

</script>
@endpush
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/css/tempusdominus-bootstrap-4.min.css" integrity="sha512-3JRrEUwaCkFUBLK1N8HehwQgu8e23jTH4np5NHOmQOobuC4ROQxFwFgBLTnhcnQRMs84muMh0PnnwXlPq5MGjg==" crossorigin="anonymous" />
@endpush


