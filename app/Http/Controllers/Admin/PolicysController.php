<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Policy;
use App\TypePolicys;
use App\Sex;
use App\User;
use App\Contact;
use App\Coverage;
use App\Vehicle;
use App\Requisition;
use Response;



class PolicysController extends Controller
{
    public function index($type_policy = false)
    {
        $requisitions = Requisition::all();
        $policys = Policy::all();

        if ($type_policy) {
            $type_policys = TypePolicys::where('id', $type_policy)->first();
             return view('admin.policys.index', compact('type_policys', 'requisitions', 'policys'));
        }else {
             return response(view('errors.404'), 404);
        }
    }

    public function create($type_policy)
    {
        $sexs = Sex::all();
        $users = Contact::all();        
        $type_policys = TypePolicys::where('id', $type_policy)->first();
        return view('admin.policys.create', compact('type_policys', 'sexs', 'users'));
    }

    public function store(Request $request, $type_policy)
    {
        $data = request()->all();
       
        $data_convarge = request("cobertura");

        $descripcion_vehiculo = $data['descripcion_vehiculo'];
        $marca_vehiculo = $data['marca_vehiculo'];
        $submarca_vehiculo = $data['submarca_vehiculo'];
        $modelo_vehiculo = $data['modelo_vehiculo'];
        $cobertura_vehiculo = $data['cobertura_vehiculo'];
        $nmotor_vehiculo = $data['nmotor_vehiculo'];
        $nserie_vehiculo = $data['nserie_vehiculo'];
        $nplacas_vehiculo = $data['nplacas_vehiculo'];
        $uso_vehiculo = $data['uso_vehiculo'];
        $beneficiario_vehiculo = $data['beneficiario_vehiculo'];
        $conductor_vehiculo = $data['conductor_vehiculo'];
        $tipo_carga_vehiculo = $data['tipo_carga_vehiculo'];
        $adaptaciones_vehiculo = $data['adaptaciones_vehiculo'];


        unset($data['coverages']);
        unset($data['descripcion_vehiculo']);
        unset($data['marca_vehiculo']);
        unset($data['submarca_vehiculo']);
        unset($data['modelo_vehiculo']);
        unset($data['cobertura_vehiculo']);
        unset($data['nmotor_vehiculo']);
        unset($data['nserie_vehiculo']);
        unset($data['nplacas_vehiculo']);
        unset($data['uso_vehiculo']);
        unset($data['beneficiario_vehiculo']);
        unset($data['conductor_vehiculo']);
        unset($data['tipo_carga_vehiculo']);
        unset($data['adaptaciones_vehiculo']);

        $policy = Policy::create($data);

        foreach ($descripcion_vehiculo as $key => $d) {
            $save = array(
                'descripcion_vehiculo' => $descripcion_vehiculo[$key],
                'marca_vehiculo' => $marca_vehiculo[$key],
                'submarca_vehiculo' => $submarca_vehiculo[$key],
                'modelo_vehiculo' => $modelo_vehiculo[$key],
                'cobertura_vehiculo' => $cobertura_vehiculo[$key],
                'nmotor_vehiculo' => $nmotor_vehiculo[$key],
                'nserie_vehiculo' => $nserie_vehiculo[$key],
                'nplacas_vehiculo' => $nplacas_vehiculo[$key],
                'uso_vehiculo' => $uso_vehiculo[$key],
                'beneficiario_vehiculo' => $beneficiario_vehiculo[$key],
                'conductor_vehiculo' => $conductor_vehiculo[$key],
                'tipo_carga_vehiculo' => $tipo_carga_vehiculo[$key],
                'adaptaciones_vehiculo' => $adaptaciones_vehiculo[$key],
                'policy_id' => $policy->id
            );
            Vehicle::create($save);
        }

        if($data_convarge['riesgo']){
            foreach ($data_convarge['riesgo'] as $key => $d) {
                $save = array(
                    'riesgo' => $data_convarge['riesgo'][$key],
                    'suma' => $data_convarge['suma'][$key],
                    'cuota' => $data_convarge['cuota'][$key],
                    'prima' => $data_convarge['prima'][$key],
                    'policy_id' => $policy->id
                );
                Coverage::create($save);
            }
        }

        return Response::json($policy);

    }

    public function show( $type_policy,  Policy $policy)
    {
        $sexs = Sex::all();
        $users = Contact::all();        
        $type_policys = TypePolicys::where('id', $type_policy)->first();
        return view('admin.policys.edit', compact('sexs','users', 'type_policys', 'policy'));
     }


     public function update( $type_policy, Request $request, Policy $policy )
     {
        $data = request()->all();
        unset($data['coverages']);
        $data_convarge = request("cobertura");


        $descripcion_vehiculo = $data['descripcion_vehiculo'];
        $marca_vehiculo = $data['marca_vehiculo'];
        $submarca_vehiculo = $data['submarca_vehiculo'];
        $modelo_vehiculo = $data['modelo_vehiculo'];
        $cobertura_vehiculo = $data['cobertura_vehiculo'];
        $nmotor_vehiculo = $data['nmotor_vehiculo'];
        $nserie_vehiculo = $data['nserie_vehiculo'];
        $nplacas_vehiculo = $data['nplacas_vehiculo'];
        $uso_vehiculo = $data['uso_vehiculo'];
        $beneficiario_vehiculo = $data['beneficiario_vehiculo'];
        $conductor_vehiculo = $data['conductor_vehiculo'];
        $tipo_carga_vehiculo = $data['tipo_carga_vehiculo'];
        $adaptaciones_vehiculo = $data['adaptaciones_vehiculo'];


        unset($data['coverages']);
        unset($data['descripcion_vehiculo']);
        unset($data['marca_vehiculo']);
        unset($data['submarca_vehiculo']);
        unset($data['modelo_vehiculo']);
        unset($data['cobertura_vehiculo']);
        unset($data['nmotor_vehiculo']);
        unset($data['nserie_vehiculo']);
        unset($data['nplacas_vehiculo']);
        unset($data['uso_vehiculo']);
        unset($data['beneficiario_vehiculo']);
        unset($data['conductor_vehiculo']);
        unset($data['tipo_carga_vehiculo']);
        unset($data['adaptaciones_vehiculo']);

         $policy->update($data); 

         $all_convarge = Coverage::where('policy_id', $policy->id )->delete();
         $all_convarge = Vehicle::where('policy_id', $policy->id )->delete();

         foreach ($descripcion_vehiculo as $key => $d) {
            $save = array(
                'descripcion_vehiculo' => $descripcion_vehiculo[$key],
                'marca_vehiculo' => $marca_vehiculo[$key],
                'submarca_vehiculo' => $submarca_vehiculo[$key],
                'modelo_vehiculo' => $modelo_vehiculo[$key],
                'cobertura_vehiculo' => $cobertura_vehiculo[$key],
                'nmotor_vehiculo' => $nmotor_vehiculo[$key],
                'nserie_vehiculo' => $nserie_vehiculo[$key],
                'nplacas_vehiculo' => $nplacas_vehiculo[$key],
                'uso_vehiculo' => $uso_vehiculo[$key],
                'beneficiario_vehiculo' => $beneficiario_vehiculo[$key],
                'conductor_vehiculo' => $conductor_vehiculo[$key],
                'tipo_carga_vehiculo' => $tipo_carga_vehiculo[$key],
                'adaptaciones_vehiculo' => $adaptaciones_vehiculo[$key],
                'policy_id' => $policy->id
            );
            Vehicle::create($save);
        }

        if($data_convarge['riesgo']){

            foreach ($data_convarge['riesgo'] as $key => $d) {
                $save = array(
                    'riesgo' => $data_convarge['riesgo'][$key],
                    'suma' => $data_convarge['suma'][$key],
                    'cuota' => $data_convarge['cuota'][$key],
                    'prima' => $data_convarge['prima'][$key],
                    'policy_id' => $policy->id
                );
                Coverage::create($save);
            }
        }

         return Response::json($policy); 
    }

    public function files_policys(Request $request)
    {
        dump($request);
    }

}
