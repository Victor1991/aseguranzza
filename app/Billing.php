<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
     protected $fillable = [
         'user_id', 'business_name','rfc', 'address', 'mail', 'phone', 'postal_code'
         , 'state_id', 'municipality_id', 'location_id', 'street', 'n_ext', 'n_int'
    ];

    public function estados()
    {
         return $this->hasMany( PostalCode::class,  'cp', 'postal_code')->groupBy('estado');
    }

    public function municipios()
    {
         return $this->hasMany( PostalCode::class,  'cp', 'postal_code')->groupBy('municipio');
    }

    public function colonias()
    {
         return $this->hasMany( PostalCode::class,  'cp', 'postal_code')->groupBy('colonia');
    }
}
