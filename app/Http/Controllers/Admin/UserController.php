<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\TypePerson;
use App\Sex;
use App\TypeSeller;
use App\State;
use Response;
use Carbon\Carbon;
use App\Municipality;
use App\Location;
use App\PostalCode;
use App\Events\UserWasCreated;
use App\Events\TaskWasCreated;
use App\InsuredGroup;

use App\Billing;
use App\InformationContact;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $users = User::all();
          return view('admin.users.index', compact('users'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $user = new User;
          $type_persons = TypePerson::all();
          $promoters = User::all();
          $sexs = Sex::all();
          $types_sellers = TypeSeller::all();
          $insured_groups = InsuredGroup::all();
          $states = PostalCode::groupBy('estado_id')->orderBy('estado_id','ASC')->get();
          $municipalitys = PostalCode::where('estado_id', 1)->groupBy('municipio_id')->orderBy('municipio_id','ASC')->get();
          return view('admin.users.create', compact('user', 'type_persons', 'promoters', 'sexs', 'types_sellers', 'states', 'municipalitys', 'insured_groups'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users',
               'password' => 'required| min:6'
          ],[],[
               'name' => 'Nombre(s)',
               'email' => 'Correo',
               'password' => 'Contraseña',
          ]);

          $data = request()->all();

          $data['password'] = bcrypt(request("password"));
          $data["birthdate"] = Carbon::createFromFormat('d/m/Y', request("birthdate"))->format('Y-m-d');

          $user = User::create($data);

          return response()->json($user);

          // return redirect()->route('admin.user.index')->with('success', 'Usuario registrado');
     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(User $user)
     {
          $type_persons = TypePerson::all();
          $promoters = User::all();
          $sexs = Sex::all();
          $types_sellers = TypeSeller::all();
          $states = State::all();
          return view('admin.users.show', compact('user','type_persons', 'promoters', 'sexs', 'types_sellers', 'states'));
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(User $user)
     {
          $type_persons = TypePerson::all();
          $promoters = User::all();
          $sexs = Sex::all();
          $types_sellers = TypeSeller::all();
          $states = State::all();
          $insured_groups = InsuredGroup::all();

          $states = PostalCode::groupBy('estado_id')->orderBy('estado_id','ASC')->get();
          $municipalitys = PostalCode::where('estado_id', 1)->groupBy('municipio_id')->orderBy('municipio_id','ASC')->get();

          $data_cp = $this->code_postal($user->postal_code);

          return view('admin.users.edit', compact('user','type_persons', 'promoters', 'sexs', 'types_sellers', 'states', 'municipalitys', 'data_cp', 'insured_groups'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, User $user)
     {

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
          ]);

          $data = request()->all();
          $data["birthdate"] = Carbon::createFromFormat('d/m/Y', request("birthdate"))->format('Y-m-d');


          if($request->filled('password')) {
               $data['password'] = bcrypt(request("password"));
          }else{
               unset($data['password']);
          }
          
          $user->update( $data );

          return redirect()->route('admin.user.index')->with('success', 'Administrador actualizado');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(User $user)
     {
          
          $user->delete();
          return back()->with('success', 'Usuario eliminado');

     }

     public function code_postal($code_postal)
     {
          $datas = PostalCode::where('cp', $code_postal)->orderBy('colonia','ASC')->get();
          $states = array();
          $municipalitys = array();
          $locations = array();
          if ($datas) {
               foreach ($datas as $key => $value) {
                    $states[$value->estado_id] = $value->estado;
                    $municipalitys[$value->municipio_id] = $value->municipio;
                    $locations[$value->id] = $value->colonia;
               }
          }

          return array('estados' => $states, 'municipios' => $municipalitys, 'colonias' => $locations);
     }

     public function test()
     {
          echo "1";
          $user = User::where('id', 1)->first();
          dump(TaskWasCreated::dispatch($user, 'Tarea de prueba 2', '2020-12-17', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'));

     }

     public function user_json(User $user)
     {
          return Response::json($user->load('direction_cp'));
     }
     
}
