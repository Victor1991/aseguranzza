<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Task;


class TaskController extends Controller
{
    //
    public function full_calendar_task(Request $request)
    {
        // dump(request('start'));
        // dump(request('end'));
        $tasks = Task::whereBetween('end_date', [ date('Y-m-d', strtotime(request('start'))), date('Y-m-d', strtotime(request('end'))) ])->get();
        $_response = array();
        foreach ($tasks as $key => $task) {
            $_response[] = array(
                'id' => $task->id,
                'title' => $task->name,
                "start" => $task->end_date,
                "end" => $task->end_date,
                // "url" => route('admin.task.edit', $task),
                "contact" => $task->asignado->name . ' ' . $task->asignado->last_name . ' ' .$task->asignado->mother_last_name  ,
                
            );
        }

        return response()->json($_response);   
    }
}
