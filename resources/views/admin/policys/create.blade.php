@extends('admin.layout')

@section('content')
    <div class="col-md-12 ml-auto mr-auto">
        <div class="card">
            <div class="card-header ">
                <h3 class="card-title">Registrar contacto</h3>
            </div>
            <div class="card-body">

                <div id="demo">
                    <div class="step-app">
                        <ul class="step-steps">
                            <li><a href="#tab1"><span class="number">1</span> Asegurado </a></li>
                            <li><a href="#tab2"><span class="number">2</span> Vehículo</a></li>
                            <!-- <li><a href="#tab3"><span class="number">2</span> Datos co3tratante</a></li> -->
                            <li><a href="#tab3"><span class="number">3</span> Administrativo</a></li>
                            <li><a href="#tab4"><span class="number">4</span> Cobertura</a></li>
                            <li><a href="#tab5"><span class="number">5</span> Especificaciones</a></li>
                        </ul>
                        @php
                            $fol = isset($type_policys->end_policy) ? ++$type_policys->end_policy->id : 1 ;
                        @endphp
                        <div class="step-content">
                            <div class="step-tab-panel" id="tab1">
                                <form name="frmInfo" id="frmInfo">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Solicitud de póliza </label>
                                                <input type="text" name="request_number"
                                                    value="AZ{{ $fol }}" readonly
                                                    class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Año de emisión </label>
                                                <input type="text" name="year" value="{{ date('Y') }}" readonly
                                                    class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="type_policys_id" value="{{ $type_policys->id }}">
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul class="nav nav-pills" id="myTab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                        role="tab" aria-controls="home" aria-selected="true">Contratante</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile"
                                                        role="tab" aria-controls="profile"
                                                        aria-selected="false">Asegurado</a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="tab-content" id="myTabContent">
                                                        <div class="tab-pane fade show active" id="home" role="tabpanel"
                                                            aria-labelledby="home-tab">

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h3>Básicos</h3>
                                                                    <hr>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="name"> Contratante:</label>
                                                                        <select name="contratante" id="contratante" class="form-control" onchange="informacion(1)">
                                                                            <option hidden>- Selecciona una opción - </option>
                                                                            @foreach ($users as $user)
                                                                                <option value="{{$user->id}}">{{$user->name}} {{$user->last_name}} {{$user->mother_last_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="name"> Título:</label>
                                                                        <input type="text" name="titulo_contratante"
                                                                            class="form-control" value="">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="name">Sexo:</label>
                                                                        <select class="form-control" name="sex_id_contratante">
                                                                            <option value="">-- Seleccione uno --</option>
                                                                            <?php foreach ($sexs as $key =>
                                                                            $sex): ?>
                                                                            <option @if (old('sex_id') == $sex->id) selected @endif
                                                                                value="<?= $sex->id ?>"><?= $sex->name ?></option>
                                                                                        <?php endforeach; ?>
                                                                                   </select>
                                                                                </div>
                                                                            </div>

                                                                            {{--                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="name"> Nro. del cliente:</label>
                                                                                    <input type="text" name="ncliente_contratante"
                                                                                        class="form-control" value="">
                                                                                </div>
                                                                            </div>
                                                                            --}}

                                                                            {{-- <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="name">Tipo de contacto:</label>
                                                                                    <input class="form-control" name="tcontacto_contratante">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-6 text-center">
                                                                                <div class="form-group ">
                                                                                    <label for="name">¿informa datos de artículo 140?
                                                                                    <input class="form-control" type="checkbox" value="1" name="articulo_contratante">
                                                                                </label>
                                                                                </div>
                                                                            </div> --}}

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name"> Calle y No.(de cobro):</label>
                                                                                    <input type="text" name="ncobro_contratante"
                                                                                        class="form-control" value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="name"> Colonia:</label>
                                                                                    <input type="text" name="colonia_contratante"
                                                                                        class="form-control" value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="name">Delegación:</label>
                                                                                    <input type="text" name="delegacion_contratante"
                                                                                        class="form-control" value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">C.P:</label>
                                                                                    <input type="text" name="cp_1_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-8">
                                                                                <div class="form-group">
                                                                                    <label for="name">Entidad:</label>
                                                                                    <input type="text" name="entidad_contratante"
                                                                                        class="form-control" value="">
                                                                                </div>
                                                                            </div>

                                                                            {{-- <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">Zona:</label>
                                                                                    <input type="text" name="zona_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div> --}}

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Tipo de domicilio:</label>
                                                                                    <input class="form-control" name="tdomicilio_contratante">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Teléfonos:</label>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="name">Tel 1:</label>
                                                                                    <input type="text" name="tel_1_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div>


                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="name">Tel 2:</label>
                                                                                    <input type="text" name="tel_2_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="name">Fax:</label>
                                                                                    <input type="text" name="fax_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="name">Celular:</label>
                                                                                    <input type="text" name="celular_contratante"
                                                                                        class="form-control" value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="nota">Nota</label>
                                                                                    <textarea name="nota_contratante" class="form-control" cols="30" rows="4"></textarea>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">EMAIL:</label>
                                                                                    <input type="email" name="email_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div>


                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">Fecha de nacimiento:</label>
                                                                                    <input type="text" name="fnacimiento_contratante"
                                                                                        class="form-control datepicker"  id="datetimepicker1" data-toggle="datetimepicker" data-target="#datetimepicker1"  value="">
                                                                                </div>
                                                                            </div>


                                                                       

                                                                       
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">CURP:</label>
                                                                                    <input type="text" name="curp_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">R.F.C:</label>
                                                                                    <input type="text" name="rfc_contratante" class="form-control"
                                                                                        value="">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <h3>Artículo 140</h3>
                                                                                <hr>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Ocupación o Profeción:</label>
                                                                                    <input type="text" name="ocupacion_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Actividad o Giro:</label>
                                                                                    <input type="text" name="actividad_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-7">
                                                                                <div class="form-group">
                                                                                    <label for="name">Apoderado:</label>
                                                                                    <input type="text" name="apoderado_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label for="name">Parentesco:</label>
                                                                                    <input type="text" name="parentesco_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Cargo Gubernamental:</label>
                                                                                    <input type="text" name="cgurnamental_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Origen de ingresos:</label>
                                                                                    <input type="text" name="origen_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                
                                                                            <div class="col-md-12 text-center">
                                                                                <div class="form-group div_check" style="padding: 10px;"> 
                                                                                    <label for="name">Nacionalidad</label>
                                                                                    <div class="row">
                                                                                        <div class="col">
                                                                                            <input type="radio" name="nacionalidad_contratante" value="Mexicana">
                                                                                            <label for="name">Mexicana</label>
                                                                                        </div>
                                                                                        <div class="col">
                                                                                            <input type="radio" name="nacionalidad_contratante" value="Extrangera">
                                                                                            <label for="name">Extranjera</label>
                                                                                        </div>                                                                                       
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">DOMICILIO DE ORIGEN:</label>
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Calle y Número:</label>
                                                                                    <input type="text" name="calle_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-7">
                                                                                <div class="form-group">
                                                                                    <label for="name">Ciudad:</label>
                                                                                    <input type="text" name="ciudad_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label for="name">Estado:</label>
                                                                                    <input type="text" name="estado_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-9">
                                                                                <div class="form-group">
                                                                                    <label for="name">País:</label>
                                                                                    <input type="text" name="pais_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="name">C.P:</label>
                                                                                    <input type="text" name="cp_2_contratante"  class="form-control" value="" id="postal_code">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Teléfonos:</label>
                                                                                    <input type="text" name="telefonos_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                
                                                                            <div class="col-md-12 text-center">
                                                                                <div class="form-group div_check"> 
                                                                                <label for="name">Tipo</label>
                                                                                    <div class="row">
                                                                                        <div class="col">
                                                                                            <input type="radio" name="tpersona_contratante" value="Moral">
                                                                                            <label for="name">Persona moral</label>
                                                                                        </div>
                                                                                        <div class="col">
                                                                                            <input type="radio" name="tpersona_contratante" value="Admi">
                                                                                            <label for="name">Cjo.administración</label>
                                                                                        </div>
                                                                                        <div class="col">
                                                                                            <input type="radio" name="tpersona_contratante" value="Unico">
                                                                                            <label for="name">Administrador Único</label>
                                                                                        </div>
                                                                                        <div class="col">
                                                                                            <input type="radio" name="tpersona_contratante" value="Fisica">
                                                                                            <label for="name">Persona Física</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                
                            
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="name">Administradores (Nombre y Nacionalidad):</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">Director:</label>
                                                                                    <input type="text" name="director_contratante"  class="form-control" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">Gerente General: </label>
                                                                                    <input type="text" name="ggenral_contratante"  class="form-control" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">Apoderado General: </label>
                                                                                    <input type="text" name="ageneral_contratante"  class="form-control" value="">
                                                                                </div>  
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <h3>Básicos</h3>
                                                                                <hr>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name"> Asegurado:</label>
                                                                                    <select name="contratante_asegurado" id="contratante_asegurado" class="form-control" onchange="informacion(2)">
                                                                                        <option hidden>- Selecciona una opción - </option>
                                                                                        @foreach ($users as $user)
                                                                                            <option value="{{$user->id}}">{{$user->name}} {{$user->last_name}} {{$user->mother_last_name}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
            
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name"> Título:</label>
                                                                                    <input type="text" name="titulo_asegurado"
                                                                                        class="form-control" value="">
                                                                                </div>
                                                                            </div>
            
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label for="name">Sexo:</label>
                                                                                    <select class="form-control" name="sex_id_asegurado">
                                                                                        <option value="">-- Seleccione uno --</option>
                                                                                        <?php foreach ($sexs as $key => $sex): ?>
                                                                                        <option @if (old('sex_id') == $sex->id) selected @endif
                                                                                            value="<?= $sex->id ?>"><?= $sex->name ?></option>
                                                                                                <?php endforeach; ?>
                                                                                           </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    {{-- 
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="name"> Nro. del cliente:</label>
                                                                                            <input type="text" name="ncliente_asegurado"
                                                                                                class="form-control" value="">
                                                                                        </div>
                                                                                    </div>
                                                                                    --}}

                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Tipo de contacto:</label>
                                                                                            <input class="form-control" name="tcontacto_asegurado">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    {{-- <div class="col-md-6 text-center">
                                                                                        <div class="form-group">
            
                                                                                            <label for="name">¿informa datos de artículo 140?
                                                                                                <input type="checkbox" class="form-control" value="1" name="articulo_asegurado">
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name"> Calle y No.(de cobro):</label>
                                                                                            <input type="text" name="ncobro_asegurado"
                                                                                                class="form-control" value="">
                                                                                        </div>
                                                                                    </div> --}}
            
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="name"> Colonia:</label>
                                                                                            <input type="text" name="colonia_asegurado"
                                                                                                class="form-control" value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-5">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Delegación:</label>
                                                                                            <input type="text" name="delegacion_asegurado"
                                                                                                class="form-control" value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="name">C.P:</label>
                                                                                            <input type="text" name="cp_1_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-8">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Entidad:</label>
                                                                                            <input type="text" name="entidad_asegurado"
                                                                                                class="form-control" value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    {{-- <div class="col-md-4">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Zona:</label>
                                                                                            <input type="text" name="zona_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div> --}}
            
            
            
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Tipo de domicilio:</label>
                                                                                            <input class="form-control" name="tdomicilio_asegurado">
                                                                                        </div>
                                                                                    </div>
            
            
            
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Teléfonos:</label>
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Tel 1:</label>
                                                                                            <input type="text" name="tel_1_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div>
            
            
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Tel 2:</label>
                                                                                            <input type="text" name="tel_2_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Fax:</label>
                                                                                            <input type="text" name="fax_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Celular:</label>
                                                                                            <input type="text" name="celular_asegurado"
                                                                                                class="form-control" value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="nota">Nota</label>
                                                                                            <textarea name="nota_asegurado" class="form-control" cols="30" rows="4"></textarea>
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">EMAIL:</label>
                                                                                            <input type="text" name="email_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-4">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Fecha de nacimiento:</label>
                                                                                            <input type="text" name="fnacimiento_asegurado"
                                                                                                class="form-control datepicker" id="datetimepicker2" data-toggle="datetimepicker" data-target="#datetimepicker2" value="">
                                                                                        </div>
                                                                                    </div>
                                                                                   
                                                                                    <div class="col-md-4">
                                                                                        <div class="form-group">
                                                                                            <label for="name">CURP:</label>
                                                                                            <input type="text" name="curp_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div>
            
                                                                                    <div class="col-md-4">
                                                                                        <div class="form-group">
                                                                                            <label for="name">R.F.C:</label>
                                                                                            <input type="text" name="rfc_asegurado" class="form-control"
                                                                                                value="">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h3>Artículo 140</h3>
                                                                                        <hr>
                                                                                    </div>
            
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Ocupación o Profesión:</label>
                                                                                            <input type="text" name="ocupacion_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Actividad o Giro:</label>
                                                                                            <input type="text" name="actividad_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-7">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Apoderado:</label>
                                                                                            <input type="text" name="apoderado_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-5">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Parentesco:</label>
                                                                                            <input type="text" name="parentesco_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Cargo Gubernamental:</label>
                                                                                            <input type="text" name="cgurnamental_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Origen de ingresos:</label>
                                                                                            <input type="text" name="origen_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                        
                                                                                    <div class="col-md-12 text-center">
                                                                                        <div class="form-group div_check" style="padding: 10px;"> 
                                                                                            <label for="name">Nacionalidad</label>
                                                                                            <div class="row">
                                                                                                <div class="col">
                                                                                                    <input type="radio" name="nacionalidad_asegurado" value="Mexicana">
                                                                                                    <label for="name">Mexicana</label>
                                                                                                </div>
                                                                                                <div class="col">
                                                                                                    <input type="radio" name="nacionalidad_asegurado" value="Extrangera">
                                                                                                    <label for="name">Extranjera</label>
                                                                                                </div>                                                                                       
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                        
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">DOMICILIO DE ORIGEN:</label>
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Calle y Número:</label>
                                                                                            <input type="text" name="calle_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-7">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Ciudad:</label>
                                                                                            <input type="text" name="ciudad_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-5">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Estado:</label>
                                                                                            <input type="text" name="estado_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-9">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Pais:</label>
                                                                                            <input type="text" name="pais_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="name">C.P:</label>
                                                                                            <input type="text" name="cp_2_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Teléfonos:</label>
                                                                                            <input type="text" name="telefonos_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                        
                                                                                    <div class="col-md-12 text-center">
                                                                                        <div class="form-group div_check"> 
                                                                                        <label for="name">Tipo</label>
                                                                                            <div class="row">
                                                                                                <div class="col">
                                                                                                    <input type="radio" name="tpersona_asegurado" value="Moral">
                                                                                                    <label for="name">Persona moral</label>
                                                                                                </div>
                                                                                                <div class="col">
                                                                                                    <input type="radio" name="tpersona_asegurado" value="Admi">
                                                                                                    <label for="name">Cjo.administración</label>
                                                                                                </div>
                                                                                                <div class="col">
                                                                                                    <input type="radio" name="tpersona_asegurado" value="Unico">
                                                                                                    <label for="name">Administrador Único</label>
                                                                                                </div>
                                                                                                <div class="col">
                                                                                                    <input type="radio" name="tpersona_asegurado" value="Fisica">
                                                                                                    <label for="name">Persona Física</label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                        
                                    
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Administradores (Nombre y Nacionalidad):</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Director:</label>
                                                                                            <input type="text" name="director_asegurado"  class="form-control" value="">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Gerente General:</label>
                                                                                            <input type="text" name="ggenral_asegurado"  class="form-control" value="">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <div class="form-group">
                                                                                            <label for="name">Apoderado General:</label>
                                                                                            <input type="text" name="ageneral_asegurado"  class="form-control" value="">
                                                                                        </div>  
                                                                                    </div>
                                                                                </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                            </form>
                                        </div>
                                        <div class="step-tab-panel" id="tab2">
                                            <form name="frmForm" id="frmForm">
                                                <div class="row" style=" margin-top: 20px;">
                                                    {{-- <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Clave del vehículo</label>
                                                            <div class="input-group mb-3">
                                                                <input type="text" class="form-control" value="{{ old('clave') }}" name="clave"  >
                                                               
                                                            </div>

                                                        </div>
                                                    </div> --}}
                                                    
                                                    <div class="col-md-4 text-center">
                                                        <label for="automovil">
                                                            <input type="radio"  class="form-control" name="type_car" value="1">
                                                            Automóvil
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        <label for="camion">
                                                            <input type="radio"  class="form-control" name="type_car" value="2">
                                                            Camión
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        <label for="Flotilla">
                                                            <input type="radio"  class="form-control" name="type_car" value="3">
                                                            Flotilla
                                                        </label>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <hr>
                                                    </div>
                                                    
                                                    <div class="col-md-12 text-right" id="option_flotilla" style="display:none;"> 
                                                        <a href="{{ asset('example_autos.csv')}}" target="_blank" class="btn btm-sm btn-info"> 
                                                            <i class="fas fa-file-csv"></i>
                                                            Ejemplo CSV 
                                                        </a>

                                                        <button type="button" class="btn btm-sm btn-success" onclick="add_vehiculo()"> 
                                                            <i class="fas fa-plus"></i>
                                                            Agregar </button>
                                                        <input type="file" name="file" id="file" class="inputfile"  onchange="csv_file(this)"/>
                                                        <label for="file"> <i class="fas fa-file-csv"></i>
                                                            Cargar CSV</label>
                                                        <br>
                                                        <br>
                                                    </div>
                                                    
                                                    <div class="col-md-12" id="div_vehiculos">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="name">Descripción del vehículo:</label>
                                                                            <input class="form-control" name="descripcion_vehiculo[]" id="descrip">
                                                                         </div>
                                                                    </div>
                                                                     
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="name">Marca:</label>
                                                                            <input type="text" class="form-control" name="marca_vehiculo[]" id="marca">
                                                                        </div>
                                                                    </div> 
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="name">Submarca:</label>
                                                                            <input  type="text" class="form-control" name="submarca_vehiculo[]" id="submarca">
                                                                        </div>
                                                                    </div> 
                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                          <label for="name">Modelo:</label>
                                                                          <input type="text" class="form-control" name="modelo_vehiculo[]" id="modelo">
                                                                        </div>
                                                                      </div> 
                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="name">Cobertura:</label>
                                                                            <input type="text" class="form-control" name="cobertura_vehiculo[]" id="cobertur">
                                                                        </div>
                                                                     </div> 
                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="name">No. de motor:</label>
                                                                            <input type="text" class="form-control" name="nmotor_vehiculo[]" id="motor">
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                        <label for="name">No. de serie:</label>
                                                                        <input type="text"  class="form-control" name="nserie_vehiculo[]" id="serie">
                                                                     </div>
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                        <label for="name">No. de placas:</label>
                                                                        <input type="text"  class="form-control" name="nplacas_vehiculo[]" id="placas">
                                                                     </div>
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                        <label for="name">Uso del Vehículo:</label>
                                                                        <select name="uso_vehiculo[]" class="form-control" id="uso_vehiculo">
                                                                            <option value="">Selecciona una opción</option>
                                                                            <option value="Familiar">Familiar</option>
                                                                            <option value="Carga Particular">Carga Particular</option>
                                                                            <option value="Carga Federal">Carga Federal</option>
                                                                            <option value="Utilitario">Utilitario</option>
                                                                            <option value="Taxi">Taxi</option>
                                                                            <option value="Plataformas">Plataformas</option>
                                                                        </select>
                                                                    </div>
                                                                     </div>
                                                                     <div class="col-md-12">
                                                                        <div class="form-group">
                                                                        <label for="name">Beneficiario Preferente:</label>
                                                                        <input type="text"  class="form-control" name="beneficiario_vehiculo[]" id="beneficiario">
                                                                     </div>
                                                                     </div>
                                                                     <div class="col-md-12">
                                                                        <div class="form-group">
                                                                        <label for="name">Conductor o Titular:</label>
                                                                        <input type="text"  class="form-control" name="conductor_vehiculo[]" id="conductor">
                                                                     </div>
                                                                     </div>
                                                                     <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="name">Tipo de Carga:</label>
                                                                            <select name="tipo_carga_vehiculo[]" class="form-control" id="tipo_carga">
                                                                                <option value="">Selecciona una opción</option>
                                                                                <option value="No Peligrosa">(A)No Peligrosa</option>
                                                                                <option value="Peligrosa">(B) Peligrosa</option>
                                                                                <option value="Muy Peligrosa">(C) Muy Peligrosa</option>
                                                                            </select>
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-md-4 text-center">
                                                                        <div class="form-group">
                                                                            <label for="name">¿Doble Remolque?
                                                                                <input name="d_remolque_vehiculo[]" type="checkbox" value="1" class="form-control" id="remolque">
                                                                              </label>
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-md-4 text-center">
                                                                        <div class="form-group">
                                                                            <label for="name">¿Con Carga?
                                                                            <input name="con_carga_vehiculo[]" type="checkbox" value="1" class="form-control" id="con_carga">
                                                                        </label>
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="name">Adaptaciones:</label>
                                                                            <input type="text" class="form-control" name="adaptaciones_vehiculo[]" id="adaptaciones">
                                                                        </div>
                                                                     </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="step-tab-panel" id="tab3">
                                            <form name="frmExpe" id="frmExpe">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                        <label for="name">Forma de pago:</label>
                                                        <select class="form-control" name="fpago">
                                                            <option value="">Selecciona una opción</option>
                                                            <option value="Anual">Anual</option>
                                                            <option value="Semestral">Semestral</option>
                                                            <option value="Trimestral">Trimestral</option>
                                                            <option value="Mensual">Mensual</option>
                                                        </select>
                                                     </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label>Vigencia inicial:</label>
                                                       <input type="text" name="vigencia_inicial"  class="form-control datepicker" id="datetimepicker3" data-toggle="datetimepicker" data-target="#datetimepicker3" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label>Vigencia Final:</label>
                                                       <input type="text" name="vigencia_final"  class="form-control datepicker" id="datetimepicker4" data-toggle="datetimepicker" data-target="#datetimepicker4" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label for="name">Vencimiento del 1er recibo:</label>
                                                       <input type="text" name="vencimiento_1_recibo"  class="form-control datepicker" id="datetimepicker5" data-toggle="datetimepicker" data-target="#datetimepicker5" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     
                                                     
                                                     <div class="col-md-4 text-center">                                                      
                                                        <label for="name">D.P se fraciona
                                                            <input type="checkbox" class="form-control" value="1" name="dpfraciona">
                                                        </label>
                                                     </div>
                                                     
                                                     {{-- <div class="col-md-4 text-center">
                                                       <div class="form-group">
                                                          
                                                         <label for="name">DP. afecta Comisión
                                                             <input type="checkbox" class="form-control" value="1" name="dpcomision">
                                                        </label>
                                                       </div>
                                                     </div> --}}
                                                     
                                                     
                                                     <div class="col-md-4 text-center">
                                                       <div class="form-group">
                                                          
                                                         <label for="name">¿Descuento por nómina?
                                                            <input type="checkbox" class="form-control" value="1" name="dnomina">
                                                        </label>
                                                       </div>
                                                     </div>
                                                     
                                                     
                                                     
                                                     
                                                     {{-- <div class="col-md-8">
                                                     <div class="form-group">
                                                        <label for="name">Motivo de Rechazo:</label>
                                                            <input type="text" name="mrechazo"  class="form-control" value="">
                                                     </div>  
                                                     </div> --}}
                                                     
                                                     
                                                     
                                                     <div class="col-md-6 text-center">
                                                       <div class="form-group">
                                                         <label for="name">Cargo Automático
                                                         <input type="checkbox" class="form-control" value="1" name="cautomatico">
                                                        </label>
                                                       </div>
                                                     </div>
                                                     
                                                     {{-- <div class="col-md-6 text-center">
                                                       <div class="form-group">
                                                          
                                                         <label for="name">¿El cargo afecta a la prima neta?
                                                         <input type="checkbox" class="form-control" value="1" name="priman">
                                                        </label>
                                                       </div>
                                                     </div> --}}
                                                     
                                                     {{-- <div class="col-sm-12">
                                                       <div class="form-group">
                                                         <label form="name">Datos de la tarjeta de crédito</label>
                                                       </div>
                                                     </div> --}}
                                                     
                                                     
                                                     {{-- <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Titular:</label>
                                                       <input type="text" name="titular"  class="form-control" value="">
                                                     </div>  
                                                     </div> --}}
                                                     
                                                     {{-- <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Nro.:</label>
                                                       <input type="text" name="ntarjeta"  class="form-control" value="">
                                                     </div>  
                                                     </div> --}}
                                                     
                                                     {{-- <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Vencimiento:</label>
                                                       <input type="text" name="vencimiento"  class="form-control datepicker" id="datetimepicker6" data-toggle="datetimepicker" data-target="#datetimepicker6" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Banco/Inst:</label>
                                                       <input type="text" name="banco"  class="form-control" value="">
                                                     </div>  
                                                     </div> --}}
                                                     
                                                     
                                                     <div class="col-md-4">
                                                        <div class="form-group">
                                                        <label for="name">Moneda:</label>
                                                        <select class="form-control" name="moneda">
                                                            <option value="">-- Seleciona una opción</option>
                                                            <option value="Pesos">Pesos</option>
                                                            <option value="Dolares">Dólares</option>
                                                            <option value="Udis">Udis</option>
                                                        </select>
                                                     </div>
                                                     </div>
                                                     
                                                     <div class="col-md-12">
                                                        <div class="form-group">
                                                        <label for="name">Clave de agente:</label>
                                                        <select class="form-control" name="cgerente"></select>
                                                     </div>
                                                     </div>
                                                     
                                                     <div class="col-md-12">
                                                     <div class="form-group">
                                                        <label for="name">Cia aseguradora:</label>
                                                       <input type="text" name="caseguradora"  class="form-control" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     <div class="col-md-12">
                                                     <div class="form-group">
                                                        <label for="name">Prima neta anual:</label>
                                                       <input type="text" name="nanual"  class="form-control" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-4">
                                                        <div class="form-group">
                                                        <label for="name">Moneda Comisión:</label>
                                                        <select class="form-control" name="mcomision">
                                                     </select>
                                                     </div>
                                                     </div>
                                                     
                                                     <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label for="name">Comisión total:</label>
                                                       <input type="text" name="comisiont"  class="form-control" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label for="name">% Comisión:</label>
                                                       <input type="text" name="pcomicion"  class="form-control" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Grupo de Asegurados:</label>
                                                       <input type="text" name="gasegurados"  class="form-control" value="">
                                                     </div>  
                                                     </div>
                                                     
                                                     {{-- <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Nro. de Folio interno:</label>
                                                       <input type="text" name="nfolio_i"  class="form-control" value="">
                                                     </div>  
                                                     </div> --}}
                                                     
                                                     
                                                     <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Ejecutivo:</label>
                                                       <input type="text" name="ejecutivo" class="form-control" value="">
                                                     </div>  
                                                     </div>
                                                     
                                        
                                                     
                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label for="name">Vendedor:</label>
                                                        <select class="form-control" name="vendedor">
                                                     </select>
                                                     </div>
                                                     </div>
                                                     
                                                     
                                                     {{-- <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Fecha de Envío:</label>
                                                       <input type="text" name="fenvio"  class="form-control datepicker" id="datetimepicker7" data-toggle="datetimepicker" data-target="#datetimepicker7" value="">
                                                     </div>  
                                                     </div> --}}
                                                     
                                                     {{-- <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="name">Fecha de Recepción:</label>
                                                       <input type="text" name="frecepcion"  class="form-control datepicker" id="datetimepicker8" data-toggle="datetimepicker" data-target="#datetimepicker8" value="">
                                                     </div>  
                                                     </div> --}}

                                                </div>
                                            </form>
                                        </div>
                                        <div class="step-tab-panel" id="tab4">
                                            <form name="frmExpeCober" id="frmExpeCober">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="button" onclick="cobertura()" style=" float: right; margin: 10px; width: 250px;"  class="btn btn-success btn-sm pull-left">Agregar</button>
                                                    </div>
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Riesgo</th>
                                                                <th>Suma Asegurada</th>
                                                                <th>Cuota</th>
                                                                <th>Prima</th>
                                                                <th>Opcion</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbl_cobertura">
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="step-tab-panel" id="tab5">
                                            <form name="frmExpe" id="frmExpecifi">
                                                <div class="row">
                                                    <textarea name="especificaciones" class="form-control" cols="30" rows="10" style="margin: 10px;"></textarea>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="step-footer">
                                        <button data-direction="prev" class="step-btn btn btn-warning">Anterior</button>
                                        <button data-direction="next" class="step-btn btn btn-success">Siguiente</button>
                                        <button data-direction="finish" class="step-btn btn btn-info">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


@stop
@push('scripts')
                <script src="{{ asset('/js/wizard/jquery-steps.js') }}"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
                <script src="{{ asset('/js/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
                {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script> --}}
                {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.es.min.js" integrity="sha512-5pjEAV8mgR98bRTcqwZ3An0MYSOleV04mwwYj2yw+7PBhFVf/0KcE+NEox0XrFiU5+x5t5qidmo5MgBkDD9hEw==" crossorigin="anonymous"></script>  --}}
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
                {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
                            <script src="{{ asset('/theme/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
                            <script src="{{ asset('/theme/plugins/tempusdominus-bootstrap-4/js/es-mx.js') }}"></script>

            <script>


$(function() {
    $('input:radio[name="type_car"]').change(function() {
        console.log($(this).val());
        if ($(this).val() == '3') {
            $('#option_flotilla').show();
        } else {
            $('#option_flotilla').hide();
            $('.flotilla').remove();
        }
    });
});


function cobertura() {
     $('#tbl_cobertura').append(`<tr>
                              <td><input name="cobertura[riesgo][]" class="form-control"></td>
                              <td><input name="cobertura[suma][]" class="form-control"></td>
                              <td><input name="cobertura[cuota][]" class="form-control" onkeyup="input_numero(this)"></td>
                              <td><input name="cobertura[prima][]" class="form-control" onkeyup="input_precio(this)"></td>
                              <td class="text-center"><button type="button" onclick="eliminar_row(this)" class="eliminar btn btn-danger btn-3d btn-sm">Eliminar</button></td>
                         </tr>`);
}

function eliminar_row(obj) {
     console.log($(obj));
      $(obj).parent().parent().remove();
}

function eliminar_row2(obj) {
     console.log($(obj));
      $(obj).parent().parent().parent().remove();
}

let num_position = 1;

function add_vehiculo(pos = false) {
    let template = `<div class="card flotilla">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" onclick="eliminar_row2(this)" class="eliminar pull-left btn btn-danger btn-3d btn-sm" style="float: right;">Eliminar</button>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Descripción del vehículo:</label>
                                        <input class="form-control" name="descripcion_vehiculo[]" id="descrip_${num_position}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Marca:</label>
                                        <input type="text" class="form-control" name="marca_vehiculo[]" id="marca_${num_position}">
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Submarca:</label>
                                        <input  type="text" class="form-control" name="submarca_vehiculo[]" id="submarca_${num_position}">
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Modelo:</label>
                                        <input type="text" class="form-control" name="modelo_vehiculo[]" id="modelo_${num_position}">
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Cobertura:</label>
                                        <input type="text" class="form-control" name="cobertura_vehiculo[]" id="cobertur_${num_position}">
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">No. de motor:</label>
                                        <input type="text" class="form-control" name="nmotor_vehiculo[]" id="motor_${num_position}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">No. de serie:</label>
                                        <input type="text"  class="form-control" name="nserie_vehiculo[]" id="serie_${num_position}">
                                    </div>
                                </div>                                                                                                                                 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">No. de placas:</label>
                                        <input type="text"  class="form-control" name="nplacas_vehiculo[]" id="placas_${num_position}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Uso del Vehículo:</label>
                                        <select name="uso_vehiculo[]" class="form-control" id="uso_vehiculo_${num_position}">
                                            <option value="">Selecciona una opción</option>
                                            <option value="Familiar">Familiar</option>
                                            <option value="Carga Particular">Carga Particular</option>
                                            <option value="Carga Federal">Carga Federal</option>
                                            <option value="Utilitario">Utilitario</option>
                                            <option value="Taxi">Taxi</option>
                                            <option value="Plataformas">Plataformas</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Beneficiario Preferente:</label>
                                        <input type="text"  class="form-control" name="beneficiario_vehiculo[]" id="beneficiario_${num_position}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Conductor o Titular:</label>
                                        <input type="text"  class="form-control" name="conductor_vehiculo[]" id="conductor_${num_position}">
                                    </div>
                                </div>                                     
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Tipo de Carga:</label>
                                        <select name="tipo_carga_vehiculo[]" class="form-control" id="tipo_carga_${num_position}">
                                            <option value="">Selecciona una opción</option>
                                            <option value="No Peligrosa">(A)No Peligrosa</option>
                                            <option value="Peligrosa">(B) Peligrosa</option>
                                            <option value="Muy Peligrosa">(C) Muy Peligrosa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="form-group">
                                        <label for="name">¿Doble Remolque?
                                        <input name="d_remolque_vehiculo[]" type="checkbox" value="1" class="form-control" id="remolque_${num_position}">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="form-group">
                                        <label for="name">¿Con Carga?
                                        <input name="con_carga_vehiculo[]" type="checkbox" value="1" class="form-control" id="con_carga_${num_position}">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Adaptaciones:</label>
                                        <input type="text" class="form-control" name="adaptaciones_vehiculo[]" id="adaptaciones_${num_position}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
        $( "#div_vehiculos" ).prepend( template );

        num_position++;
}
let archivo = '';

function csv_file(input) {
    console.log(input.files[0].type);
    if(input.files[0].type != "text/csv"){
        swal({
            icon: 'error',
            title: 'Error tipo de archivo',
            text: 'Solo se puede cargar archivos CSV',
        });
    }else{
        archivo = input.files[0];
        subir_archivo();
    }
}

function subir_archivo() {
    const data = new FormData();
    data.append('csv', archivo);
    $.ajax({
        url : `/api/policys/upload_csv`,
        data : data,
        type : 'POST',
        dataType : 'json',
        processData: false,
        contentType: false,
        success : function(json) {
            $.each(json, function(i, item) {
                if( $("#descrip").val() == ""){
                    $(`#descrip`).val(item[0]);
                    $(`#marca`).val(item[1]);
                    $(`#submarca`).val(item[2]);
                    $(`#modelo`).val(item[3]);
                    $(`#cobertur`).val(item[4]);
                    $(`#motor`).val(item[5]);
                    $(`#serie`).val(item[6]);
                    $(`#placas`).val(item[7]);
                    $(`#uso_vehiculo option[value='${item[8]}']`).attr("selected", true);
                    // $(`#uso_vehiculo`).val(item[9]);
                    $(`#beneficiario`).val(item[0]);
                    $(`#conductor`).val(item[10]);
                    $(`#tipo_carga option[value='${item[11]}']`).attr("selected", true);
                    // $(`#tipo_carga`).val(item[11]);
                    // $(`#remolque`).val(item[12]);
                    if(item[12] == 'SI' || item[12] == 'Si' || item[12] == 'si'){
                        $( `#remolque` ).prop( "checked", true );
                    }
                    if(item[13] == 'SI' || item[13] == 'Si' || item[13] == 'si'){
                        $( `#con_carga` ).prop( "checked", true );
                    }
                    // $(`#con_carga`).val(item[13]);
                    $(`#adaptaciones`).val(item[14]);
                }else{
                    add_vehiculo();
                    let pos = num_position;
                    --pos;
                    $(`#descrip_${pos}`).val(item[0]);
                    $(`#marca_${pos}`).val(item[1]);
                    $(`#submarca_${pos}`).val(item[2]);
                    $(`#modelo_${pos}`).val(item[3]);
                    $(`#cobertur_${pos}`).val(item[4]);
                    $(`#motor_${pos}`).val(item[5]);
                    $(`#serie_${pos}`).val(item[6]);
                    $(`#placas_${pos}`).val(item[7]);
                    $(`#uso_vehiculo_${pos} option[value='${item[8]}']`).attr("selected", true);
                    // $(`#uso_vehiculo_${pos}`).val(item[9]);
                    $(`#beneficiario_${pos}`).val(item[0]);
                    $(`#conductor_${pos}`).val(item[10]);
                    $(`#tipo_carga_${pos} option[value='${item[11]}']`).attr("selected", true);
                    // $(`#tipo_carga_${pos}`).val(item[11]);
                    // $(`#remolque_${pos}`).val(item[12]);
                    if(item[12] == 'SI' || item[12] == 'Si' || item[12] == 'si'){
                        $( `#remolque_${pos}` ).prop( "checked", true );
                    }
                    if(item[13] == 'SI' || item[13] == 'Si' || item[13] == 'si'){
                        $( `#con_carga_${pos}` ).prop( "checked", true );
                    }
                    // $(`#con_carga_${pos}`).val(item[13]);
                    $(`#adaptaciones_${pos}`).val(item[14]);
                }
            });
        },
        error : function(xhr, status) {
            alert('Disculpe, existió un problema');
        },
        complete : function(xhr, status) {
            // alert('Petición realizada');
        }
    });
}


                    var frmInfo = $('#frmInfo');
                    var frmInfoValidator = frmInfo.validate();

                    var frmForm = $('#frmForm');
                    var frmFormValidator = frmForm.validate();

                    var frmExpe = $('#frmExpe');
                    var frmExpeValidator = frmExpe.validate();
                    $(document).ready(function() {
                        jQuery.extend(jQuery.validator.messages, {
                            required: "Este campo es obligatorio."
                        });

                        get_postal_code();
                        //Datemask dd/mm/yyyy
                        $('.datemask').inputmask('dd/mm/yyyy', {
                            'placeholder': 'dd/mm/yyyy'
                        })

                        // $('.datepicker').datepicker({
                        //     format: 'dd-mm-yyyy',
                        //     language: 'es',
                        // });

                        $('.datepicker').datetimepicker({
                            format: 'DD-MM-YYYY',
                            locale: 'es'
                        });

                        $('#demo').steps({
                            onChange: function(currentIndex, newIndex, stepDirection) {
                                // step1
                                if (currentIndex === 0) {
                                    if (stepDirection === 'forward') {
                                        return frmInfo.valid();
                                    }
                                    if (stepDirection === 'backward') {
                                        frmInfoValidator.resetForm();
                                    }
                                }
                                // step2
                                if (currentIndex === 1) {
                                    if (stepDirection === 'forward') {
                                        return frmForm.valid();
                                    }
                                    if (stepDirection === 'backward') {
                                        frmFormValidator.resetForm();
                                    }
                                }
                                // step3
                                if (currentIndex === 2) {
                                    if (stepDirection === 'forward') {
                                        return frmExpe.valid();
                                    }
                                    if (stepDirection === 'backward') {
                                        frmExpeValidator.resetForm();
                                    }
                                }
                                // step3
                                if (currentIndex === 3) {
                                    if (stepDirection === 'forward') {
                                        return frmExpe.valid();
                                    }
                                    if (stepDirection === 'backward') {
                                        frmExpeValidator.resetForm();
                                    }
                                }
                                return true;
                            },
                            onFinish: function() {
                                var data = new FormData();
                                var forms = $('#frmInfo,#frmForm,#frmExpe,#frmExpeCober, #frmExpecifi').serializeArray();
                                forms.forEach(function(elemento) {
                                    data.append(elemento.name, elemento.value);
                                });

                                $.ajax({
                                    processData: false,
                                    contentType: false,
                                    type: "POST",
                                    dataType: "json",
                                    url: "{{ url('admin/'.$type_policys->id.'/policys' ) }}",
                                    data: data, // serializes the form's elements.
                                    success: function(result) {
                                        swal({
                                            title: "Éxito",
                                            text: 'Registrado correctamente',
                                            type: 'success',
                                            confirmButtonColor: "#32c5d2",
                                            confirmButtonText: "Ok",
                                            allowOutsideClick: false
                                        }).then(function() {
                                            window.location =
                                                "{{ url('admin/'.$type_policys->id.'/policys/') }}";
                                        });
                                    },
                                    error: function(x, xs, xt) {
                                        var text = '';
                                        var error_ret = x.responseJSON.errors;
                                        $.each(error_ret, function(index, value) {
                                            text += '<span class="strg">' + index +
                                                '</span> : ' + value[0] + '<br>';
                                        });
                                        swal({
                                            title: "Error",
                                            html: text,
                                            type: 'error',
                                        });
                                    }
                                });
                            }
                        });
                    });


                    function informacion(type) {
                        let id = type == 1 ? $('#contratante').val() : $('#contratante_asegurado').val() ;
                        $.ajax({
                            url: "{{ url('api/contact/') }}/",
                            data: {id : id},
                            dataType: "json",
                            type: 'get',
                            success: function(response) {
                                console.table(response);
                                let myDate = new Date(response.birthdate);
                                if(type == 1){
                                    $( "select[name*='sex_id_contratante']" ).val(response.sex_id);
                                    $( "input[name*='ncobro_contratante']" ).val(response.street);
                                    $( "input[name*='colonia_contratante']" ).val(response.direction_cp[0].colonia);
                                    $( "input[name*='delegacion_contratante']" ).val(response.direction_cp[0].municipio);
                                    $( "input[name*='cp_1_contratante']" ).val(response.postal_code);
                                    $( "input[name*='tel_1_contratante']" ).val(response.phone);
                                    $( "input[name*='celular_contratante']" ).val(response.cell_phone);
                                    $( "input[name*='email_contratante']" ).val(response.email);
                                    $( "input[name*='fnacimiento_contratante']" ).val(moment(myDate).format("DD-MM-YYYY"));
                                    $( "input[name*='curp_contratante']" ).val(response.curp);
                                    $( "input[name*='rfc_contratante']" ).val(response.rfc);
                                    $( "input[name*='entidad_contratante']" ).val(response.direction_cp[0].estado);
                                    $( "input[name*='ncliente_contratante']" ).val(response.assigned_number);
                                    $( "input[name*='ocupacion_contratante']" ).val(response.occupation);
                                }else{
                                    $( "select[name*='sex_id_asegurado']" ).val(response.sex_id);
                                    $( "input[name*='ncobro_asegurado']" ).val(response.street);
                                    $( "input[name*='colonia_asegurado']" ).val(response.direction_cp[0].colonia);
                                    $( "input[name*='delegacion_asegurado']" ).val(response.direction_cp[0].municipio);
                                    $( "input[name*='cp_1_asegurado']" ).val(response.postal_code);
                                    $( "input[name*='tel_1_asegurado']" ).val(response.phone);
                                    $( "input[name*='celular_asegurado']" ).val(response.cell_phone);
                                    $( "input[name*='email_asegurado']" ).val(response.email);
                                    $( "input[name*='fnacimiento_asegurado']" ).val(moment(myDate).format("DD-MM-YYYY"));
                                    $( "input[name*='curp_asegurado']" ).val(response.curp);
                                    $( "input[name*='rfc_asegurado']" ).val(response.rfc);
                                    $( "input[name*='entidad_asegurado']" ).val(response.direction_cp[0].estado);
                                    $( "input[name*='ncliente_asegurado']" ).val(response.assigned_number);
                                    $( "input[name*='ocupacion_asegurado']" ).val(response.occupation);
                            
                                }
                            },
                            statusCode: {
                                404: function() {
                                    alert('web not found');
                                }
                            },
                            error: function(x, xs, xt) {
                                //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
                            }
                        });
                    }


                    function get_municipios(sel) {

                        var id = 'state_id';
                        var municipaly = 'municipality_id';
                        if (sel.id != 'state_id') {
                            id = 'state_modal_id';
                            municipaly = 'municipality_modal_id';
                        }
                        $('#' + municipaly).html('');
                        $.ajax({
                            url: "{{ route('municipalitys') }}",
                            data: {
                                'state_id': $('#' + id).val()
                            },
                            type: 'get',
                            success: function(response) {
                                $.each(response, function(i, item) {
                                    $('#' + municipaly).append($('<option>', {
                                        value: item.municipio_id,
                                        text: item.municipio
                                    }));
                                });
                                if (sel.id != 'state_id') {
                                    $('#' + municipaly).append('<option value="otro">Otro</option>');
                                }
                            },
                            statusCode: {
                                404: function() {
                                    alert('web not found');
                                }
                            },
                            error: function(x, xs, xt) {
                                //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
                            }
                        });
                    }


                    function get_postal_code_facturacion(obj) {
                        var id = obj.id.substring(4, 3);
                        console.log(id);
                        if (obj.value.length == 5) {
                            $.ajax({
                                url: "{{ route('code_postal') }}",
                                data: {
                                    'code_postal': obj.value
                                },
                                type: 'get',
                                success: function(response) {
                                    console.log(response);
                                    $('#state_id_' + id).html('');
                                    $.each(response.estados, function(i, item) {
                                        $('#state_id_' + id).append($('<option>', {
                                            value: i,
                                            text: item
                                        }));
                                    });
                                    $('#municipality_id_' + id).html('');
                                    $.each(response.municipios, function(i, item) {
                                        $('#municipality_id_' + id).append($('<option>', {
                                            value: i,
                                            text: item
                                        }));
                                    });
                                    $('#location_id_' + id).html('');
                                    $.each(response.colonias, function(i, item) {
                                        $('#location_id_' + id).append($('<option>', {
                                            value: i,
                                            text: item
                                        }));
                                    });
                                },
                                statusCode: {
                                    404: function() {
                                        alert('web not found');
                                    }
                                },
                                error: function(x, xs, xt) {
                                    //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
                                }
                            });
                        }
                    }


                    function get_postal_code() {
                        if ($('#postal_code').val().length == 5) {
                            $.ajax({
                                url: "{{ route('code_postal') }}",
                                data: {
                                    'code_postal': $('#postal_code').val()
                                },
                                type: 'get',
                                success: function(response) {
                                    $('#state_id').html('');
                                    $.each(response.estados, function(i, item) {
                                        $('#state_id').append($('<option>', {
                                            value: i,
                                            text: item
                                        }));
                                    });
                                    $('#municipality_id').html('');
                                    $.each(response.municipios, function(i, item) {
                                        $('#municipality_id').append($('<option>', {
                                            value: i,
                                            text: item
                                        }));
                                    });
                                    $('#location_id').html('');
                                    $.each(response.colonias, function(i, item) {
                                        $('#location_id').append($('<option>', {
                                            value: i,
                                            text: item
                                        }));
                                    });
                                },
                                statusCode: {
                                    404: function() {
                                        alert('web not found');
                                    }
                                },
                                error: function(x, xs, xt) {
                                    //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
                                }
                            });
                        }

                    }

                </script>
@endpush
@push('style')
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery.steps@1.0.1/dist/jquery-steps.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
                {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"> --}} 
                {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" integrity="sha512-63+XcK3ZAZFBhAVZ4irKWe9eorFG0qYsy2CaM5Z+F3kUn76ukznN0cp4SArgItSbDFD1RrrWgVMBY9C/2ZoURA==" crossorigin="anonymous" referrerpolicy="no-referrer" /> --}}
                <link rel="stylesheet" href="/theme/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
               
               <style>
                    .error {
                        color: red;
                    }

                    .custom-file-input:lang(en)~.custom-file-label::after {
                        content: 'navegar';
                    }

                    .strg {
                        font-size: 20px;
                        font-weight: 700;
                        color: red;
                    }

                    input {
                        text-transform: uppercase;
                    }

                    textarea {
                        text-transform: uppercase;
                    }

                    .div_fac {
                        padding: 10px;
                        border: 1px solid #9d9d9d;
                        margin-top: 20px;
                        margin-bottom: 20px;
                        border-radius: 5px;
                        background-color: #f0f0f0;
                    }

                    .div_check{
                        border: 1px solid #ddd;
                        border-radius: 5px;
                        margin-top: -5px;
                        padding-bottom: 1px;
                        padding-top: 6px;
                    }

                    .bootstrap-datetimepicker-widget{
                        z-index: 9999;
                    }
                    
                    .inputfile {
                        width: 0.1px;
                        height: 0.1px;
                        opacity: 0;
                        overflow: hidden;
                        position: absolute;
                        z-index: -1;
                    }
                    .inputfile + label {
                        font-size: 16px;
                        font-weight: 500 !important; 
                        color: white;
                        background-color: #27a745;
                        display: inline-block;
                        padding: 5px 9px 8px 10px;
                        border-radius: 5px;
                    }

                    .inputfile:focus + label,
                    .inputfile + label:hover {
                        background-color: red;
                    }
                    .inputfile + label {
                        cursor: pointer; /* "hand" cursor */
                    }
                    .inputfile:focus + label {
                        outline: 1px dotted #000;
                        outline: -webkit-focus-ring-color auto 5px;
                    }
                </style>
@endpush
