<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
    body {
        background-image: url("https://images.theconversation.com/files/295811/original/file-20191007-121065-t6bvel.jpg?ixlib=rb-1.1.0&rect=1520%2C742%2C6508%2C3248&q=45&auto=format&w=1356&h=668&fit=crop");
        background-repeat: no-repeat;
          background-color: #000;
          color: #636b6f;
          font-family: 'Nunito', sans-serif;
          font-weight: 200;
          height: 100vh;
          margin: 0;
          background-position: center center;
    }


    .full-height {
          height: 100vh;
     }


    .flex-center {
          align-items: center;
          display: flex;
          justify-content: center;
     }

     .position-ref {
          position: relative;
     }


     .card{
          background-color: #f4f4f4bd;
     }
     .btn-link{
          color: #00cc3d;
     }
     .btn-primary{
          background-color:  #00cc3d;
          color: #fff;
          border: 1px solid  #00cc3d;
     }
    </style>
</head>
<body>
    <!-- <div id="app"> -->
    <div class="flex-center position-ref full-height">
        <div class="content">

        <!-- <main class="py-5" > -->
            @yield('content')
        <!-- </main> -->
        </div>
    </div>
</body>
</html>
