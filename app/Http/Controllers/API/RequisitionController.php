<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Requisition;


class RequisitionController extends Controller
{
    //
    public function get_requisition_poilce(Request $request )
    {
        $requisitions = Requisition::where('policy_id', request('policy_id'))->get();
        return response()->json($requisitions->load('creado_por'));

    }
}
