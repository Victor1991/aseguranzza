<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisition extends Model
{
    //
    protected $fillable = [
        "num", "policy_id" ,"endorsement_request","type_endorsement_id","way_pay_id","currency","initial_validity",
        "final_validity","receipt_expiration","yearly_fee","first_partiality","commission_currency","total_commission",
        "commission_percentage","movements","shipping_date","vehicle_key","vehicle_description","brand","sub_brand",
        "capacity","model","coverage","colour","engine_no","serial_number","re_na_ve","number_plates","vehicle_use",
        "preferred_beneficiary","regular_driver","service","type_load","double_trailer","with_load","special_team",
        "adaptations","technical_data", "creator_user"
   ];
 
   public function poliza()
   {
        return $this->hasOne( Policy::class, 'id', 'policy_id');
   }
   
   public function creado_por()
   {
        return $this->belongsTo( User::class, 'creator_user', 'id' );
   }


}
