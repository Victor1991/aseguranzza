<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\NotificationPolicy;
use Illuminate\Support\Facades\Storage;

use Auth;



class NotificationPolicyController extends Controller
{
    //
    public function index(Request $request)
    {
        $id =  request("id");
        $result = NotificationPolicy::where('policie_id', $id)->get();
        return response()->json($result->load('creado_por'));

    }

    public function store(Request $request)
    {

        $data = $request->validate([
              'message' => 'required',
              'policie_id' => 'required'
        ],[],[
               'message' => 'Nota',
        ]);

        if ($request->file('archivo') != null) {
            $nota = $request->file('archivo')->store('public');
            $data['file'] = Storage::url($nota);
       }

       $data['created_by'] = Auth::user()->id;

        $result = NotificationPolicy::create($data);

        return response()->json($result);

    }

    public function destroy(NotificationPolicy $notification_policy)
    {
         $notification_policy->delete();
        return response()->json($notification_policy);
    }
}
