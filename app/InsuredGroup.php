<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuredGroup extends Model
{
    protected $fillable = [
        'name'
   ];
}
