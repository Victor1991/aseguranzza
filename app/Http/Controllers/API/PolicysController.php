<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use League\Csv\Reader;

class PolicysController extends Controller
{
    public function upload_csv(Request $request){
        
        $request->validate([
            'csv' => 'required|file',
        ]);
                
        $file = $request->file('csv');
        
        // Create list name
        $name = time().'-'.$file->getClientOriginalName();

        $reader = Reader::createFromFileObject($file->openFile());
        
        ini_set('max_execution_time', 0);
        
        $return = array();
        // echo 'paso';
        
        foreach ($reader as $index => $row) {
            if($index >= 1){
                $return[] = $row;
                
            }
        }

        return response()->json($return);

    }
}
