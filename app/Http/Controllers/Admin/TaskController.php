<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Task;
use App\AdvanceTasks;
use App\Contact;
use App\User;
use Carbon\Carbon;
use App\Events\TaskWasCreated;


class TaskController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $users = User::all();
          $tasks = Task::orderBy('created_at', 'desc')->get();

          return view('admin.task.index', compact('tasks', 'users'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Request $request)
     {
          $task = new Task;
          $task->user_id = request('user_id');
          $users = User::all();
          $contacts = Contact::all();
          $folio_max = Task::max('num_folio');

          return view('admin.task.create', compact('task','users', 'contacts','folio_max'));

     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255',
               'user_id' => 'required',
          ]);


          $data["end_date"] = Carbon::createFromFormat('d-m-Y', request("end_date"))->format('Y-m-d');
          $data["description"] = request("description");
          $data["num_folio"] = request("num_folio");
          $data["creator_user"] = \Auth::id();
          $data["status"] = 1;
          $data["contact_id"] = request("contact_id");

          
          if ($request->file('imagen') != null) {
               $passport = $request->file('imagen')->store('public');
               $data['imagen'] = Storage::url($passport);
          }

          $task = Task::create($data);

          $user = User::where('id',  request("user_id"))->first();
          TaskWasCreated::dispatch($user, request("name"), request("end_date"), request("description"));

          return redirect()->route('admin.task.index')->with('success', 'Tarea registrada');

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Task $task)
     {

     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(Task $task)
     {
          $users = User::all();
          $contacts = Contact::all();
          return view('admin.task.edit', compact('task','users', 'contacts'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Task $task)
     {

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'user_id' => 'required',
          ]);

          $data["end_date"] = Carbon::createFromFormat('d-m-Y', request("end_date"))->format('Y-m-d');
          $data["description"] = request("description");
          $data["contact_id"] = request("contact_id");

          if (request("eliminar_imagen") == 1) {
               $data['imagen'] = '';
          }
          if ($request->file('imagen') != null) {
               $passport = $request->file('imagen')->store('public');
               $data['imagen'] = Storage::url($passport);
          }

          $task->update($data);
          return redirect()->route('admin.task.index')->with('success', 'Tarea actualizada');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Task $task)
     {
          $task->delete();
          return back()->with('success', 'Tarea eliminada');
     }

     public function finish_task(Request $request)
     {
          $task = Task::where('id', $request->tasks_id)->first();
          $data['status'] = $request->status_new;
          if ($request->status_new == 2) {
               $data['user_pending_date'] = date('Y-m-d');
               $data['coment_pending'] = $request->description;
               $mensaje = 'Tarea pediente';
          }else {
               $data['user_termination_date'] = date('Y-m-d');
               $data['coment_termination'] = $request->description;
               $mensaje = 'Tarea finalizada';
          }
          $task->update($data);
          return redirect()->route('admin.task.index')->with('success', $mensaje);

     }

     public function advance_task(Request $request)
     {
          $data["user_id"] = \Auth::id();
          $data["description"] = $request->description;
          $data["task_id"] = $request->tasks_id;
          $task = AdvanceTasks::create($data);
          return redirect()->route('admin.task.edit', $request->tasks_id )->with('success', 'Avance registrado');

     }

}
