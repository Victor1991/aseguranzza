<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TaskWasCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $name;
    public $date;
    public $description;

    /**
     * Create a new event instance.
     *
     * @return void
     */
     public function __construct($user, $name, $date, $description)
     {
          $this->user = $user;
          $this->name = $name;
          $this->date = $date;
          $this->description = $description;
     }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
         return $this->markdown('emails.task')->subject('Credenciales (Sistema Intermerk)');
    }
}
