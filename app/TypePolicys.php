<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePolicys extends Model
{
    //
    

    public function policys()
    {
         return $this->hasMany( Policy::class );
    }

    public function end_policy()
    {
        return $this->hasOne( Policy::class )->orderBy('id','DESC');

    }
}
