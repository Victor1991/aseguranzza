@extends('admin.layout')

@section('content')
<div class="col-md-8 ml-auto mr-auto">
     <form class="form" method="post" enctype="multipart/form-data" action="{{ route('admin.task.update', $task) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}

          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Registrar tarea</h3>
                    <button type="button" class="btn btn-success btn-sm btn-fill float-right" onclick="$('#modal_avance').modal('show');" >Agregar avance</button>

               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name">Folio</label>
                                             <input type="text" name="name" class="form-control" value="AZ-{{ $task->num_folio }}" placeholder="Nombre" readonly>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name">Asignado a :</label>
                                             <select class="form-control" name="user_id">
                                                  <?php foreach ($users as $key => $user): ?>
                                                       <option
                                                       <?php if (old('user_id', $task->user_id) == $user->id ): ?>
                                                            selected
                                                       <?php endif; ?>
                                                       value="{{$user->id}}">{{$user->name}} {{$user->last_name}} {{$user->mother_last_name}}</option>
                                                  <?php endforeach; ?>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name"> Fecha asignación:</label>
                                            <input type="text" name="" disabled class="form-control" value="{{ \Carbon\Carbon::parse($task->created_at)->format('d-m-Y') }}">
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name"> Contacto :</label>
                                             <select class="form-control" name="contact_id">
                                                  <?php foreach ($contacts as $key => $contact): ?>
                                                  <option
                                                  <?php if (old('contact_id', $task->contact_id) == $contact->id ): ?>
                                                       selected
                                                  <?php endif; ?>
                                                  value="{{$contact->id}}">{{$contact->name}} {{$contact->last_name}} {{$contact->mother_last_name}}</option>
                                             <?php endforeach; ?>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label for="name">Nombre de la tarea</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name', $task->name) }}" placeholder="Nombre">
                                        </div>
                                   </div>

                                   <div class="col-md-4">
                                        <div class="form-group">
                                             <label>Fecha de entrega</label>
                                             <input type="text" name="end_date" class="form-control cale"
                                             <?php if (!is_null($task->end_date) && $task->end_date != '0000-00-00'): ?>
                                                  value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $task->end_date)->format('d-m-Y') }}"
                                             <?php else: ?>
                                                  value=""
                                             <?php endif; ?>
                                              >
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Descripción</label>
                                             <textarea name="description" rows="4" cols="80" class="form-control">{{ old('description', $task->description) }}</textarea>
                                        </div>
                                   </div>
                              </div>
                              <div class="row justify-content-center">
                                   <div class="col-md-10 ">
                                        <div class="form-group">
                                             <label for="name">Imagen</label>
                                             <div class="row">
                                                  <div class="col-md-12 div_img">
                                                       <div id='img_container'>
                                                            <img id="preview" src="
                                                            <?php if ($task->imagen): ?>
                                                                      {{ $task->imagen }}
                                                                 <?php else: ?>
                                                                      https://i.pinimg.com/originals/3a/ab/e0/3aabe0e9a520b9ad90407a82f85adb42.jpg
                                                                 <?php endif; ?>
                                                                 " alt="your image"  class="img-fluid" title=''/>
                                                       </div>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" name="imagen" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01" accept="image/*">
                                                                 <label class="custom-file-label" for="inputGroupFile01">Elija el archivo</label>
                                                            </div>
                                                            <button type="button" class="btn btn-danger btn-sm eliminar_archivo" id="eliminar_imagen"
                                                            <?php if (!$task->imagen): ?>
                                                                 style="display:none;"
                                                            <?php endif; ?> ><i class="fa fa-trash"></i> </button>

                                                            <input type="hidden" name="eliminar_imagen" value="">

                                                       </div>
                                                  </div>

                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                         <div class="col-md-12">
                              <h4>Avances</h4>
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <?php foreach ($task->avances as $key => $avance): ?>
                                        <div class="col-md-6">
                                             <div class="card card-outline card-primary">
                                                  <div class="card-body">
                                                       <p style="margin-bottom: 4px;"><b>Avance</b> : {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $avance->created_at)->format('d-m-Y') }} </p>
                                                       <p style="margin-bottom: 4px;"><b>Creado por  </b> : {{ $avance->creado_por->name }} {{ $avance->creado_por->last_name }} {{ $avance->creado_por->mother_last_name }} </p>
                                                       <p style="margin-bottom: 4px;"><b>Descripción </b> : {{ $avance->description }} </p>

                                                  </div>
                                                  <!-- /.card-body -->
                                             </div>
                                             <!-- /.card -->
                                        </div>
                                   <?php endforeach; ?>
                              </div>

                         </div>
                    </div>
               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right" >Actualizar tarea</button>
                    <?php if ($task->status != 2): ?>
                         <button type="button" class="btn btn-warning btn-fill float-right" onclick="change_status(2)" style="margin-right: 10px;">Pendiente tarea</button>
                    <?php endif; ?>
                    <button type="button" class="btn btn-success btn-fill float-right" onclick="change_status(3)" style="margin-right: 10px;">Finalizar tarea</button>
                    <a href="{{ route('admin.task.index') }}" class="btn btn-default btn-fill float-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>
     </form>
</div>

<div class="modal fade" id="change_status" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-header" style="background-color: #0a6aff; color: #fff;">
                    <h4 class="modal-title" id="tit_modal"></h4>
               </div>
               <form action="{{ route('admin.task.finish_task') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                         <input type="hidden" name="tasks_id" value="{{ $task->id }}">
                         <input type="hidden" name="status_new" id="status_new" value="">
                         <div class="row">
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label for="name">Comentarios</label>
                                        <textarea name="description" rows="4" cols="80" class="form-control" required></textarea>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                         <button type="submit" class="btn btn-info">Guardar</button>
                    </div>
               </form>
          </div>
     </div>
</div>

<div class="modal fade" id="modal_avance" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-header" style="background-color: #0a6aff; color: #fff;">
                    <h4 class="modal-title" id="">Agregar avance</h4>
               </div>
               <form action="{{ route('admin.task.advance_task') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                         <input type="hidden" name="tasks_id" value="{{ $task->id }}">
                         <div class="row">
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label for="name">Comentarios</label>
                                        <textarea name="description" rows="4" cols="80" class="form-control" required></textarea>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                         <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
               </form>
          </div>
     </div>
</div>

@stop
@push('scripts')
<script src="{{ asset('/js/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script> -->
<script src="{{ asset('/js/bootstrap_datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('/js/bootstrap-datepicker.es.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

<script>
// $( document ).ready(function() {
//      $('.datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
// });

var date = new Date();
date.setDate(date.getDate());
$('.cale').datepicker({
     format: "dd-mm-yyyy",
     autoclose: true,
     todayHighlight: true,
     startDate: date,
     language: "es",
});

function change_status(status) {
     $('#status_new').val(status);
     if (status == 2) {
          $('#tit_modal').text('Poner tarea en pendiente');
     }else {
          $('#tit_modal').text('Finalizar tarea');
     }
     $('#change_status').modal('show');
}


$("#inputGroupFile01").change(function(event) {
     let uploadFile = this.files[0];
     if (!(/\.(jpg|png|gif)$/i).test(uploadFile.name)) {
          swal({
               title: "Archivo no aceptado",
               html: 'El solo se aceptan imagenes',
               type: 'error',
          });
        $('#inputGroupFile01').val('');
    }else{
          RecurFadeIn();
          readURL(this);
    }
});
function readURL(input) {
     if (input.files && input.files[0]) {
          var reader = new FileReader();
          var filename = $("#inputGroupFile01").val();
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          reader.onload = function(e) {
               debugger;
               $('#preview').attr('src', e.target.result);
               $('#preview').hide();
               $('#preview').fadeIn(500);
               $('.custom-file-label').text(filename);
               $('#eliminar_imagen').show();
          }
          reader.readAsDataURL(input.files[0]);
     }
     $(".alert").removeClass("loading").hide();
}
function FadeInAlert(text){
     $(".alert").show();
     $(".alert").text(text).addClass("loading");
}
function RecurFadeIn(){
     console.log('ran');
     FadeInAlert("Wait for it...");
}

$('.eliminar_archivo').on('click', function() {
     var id = $( this ).attr( "id" );
     $("input[name="+id+"]").val(1);
     $('#imagen').val('');
     $( this ).hide();
     $('.custom-file-label').text('Elija el archivo');
     $('#preview').attr("src","https://i.pinimg.com/originals/3a/ab/e0/3aabe0e9a520b9ad90407a82f85adb42.jpg");
});
</script>
@endpush
@push('style')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<!-- <link href="{{ asset('/js/bootstrap_datepicker/bootstrap-datepicker.css')}}"> -->

<style media="screen">
.alert{
     text-align:center;
}

#preview{
     max-height:256px;
     height:auto;
     width:auto;
     display:block;
     margin-left: auto;
     margin-right: auto;
     padding:5px;
}
#img_container{
     border-radius:5px;
     width:auto;
}

.imgInp{
     width:150px;
     margin-top:10px;
     padding:10px;
     background-color:#d3d3d3;
}
.loading{
     animation:blinkingText ease 2.5s infinite;
}
@keyframes blinkingText{
     0%{     color: #000;    }
     50%{   color: #transparent; }
     99%{    color:transparent;  }
     100%{ color:#000; }
}
.custom-file-label{
     cursor:pointer;
}

.div_img{
     background-color: #eee;
     border-radius: 5px;
     padding: 10px;
}
.custom-file-input:lang(en) ~ .custom-file-label::after {
    content: "Navegar";
}
</style>
@endpush
