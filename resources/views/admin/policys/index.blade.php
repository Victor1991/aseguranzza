@extends('admin.layout')

@section('content')
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h3 class="card-title">Pólizas <b>{{ $type_policys->name }}</b></h3>
                <div class="card-tools">
                    <a href="{{ url('admin/' . $type_policys->id . '/policys/create') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i> Nuevo
                    </a>
                </div>
            </div>

            <div class="card-body table-responsive">
                <table class="table table-hover table-striped table_js">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Numero de póliza</th>
                            <th>Asegurado</th>

                            <th>Inicio de vigencia</th>
                            <th>Fin de vigencia</th>
                            <th>Forma de pago</th>
                            <th>Prima neta</th>
                            <th>Prima total</th>

                            <th class="text-center">Opción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($type_policys->policys as $policys)
                            <tr>
                                <td>{{ $policys->id }}</td>
                                <td>{{ $policys->request_number }}</td>

                                @if ($policys->contratante != '')
                                    <td>{{ $policys->asegurado->name }} {{ $policys->asegurado->last_name }}
                                        {{ $policys->asegurado->mother_last_name }}</td>
                                @else
                                    <td></td>
                                @endif

                                @if ($policys->vigencia_inicial != '')
                                    <td>{{ \Carbon\Carbon::parse($policys->vigencia_inicial)->format('d-m-Y') }}</td>
                                @else
                                    <td></td>
                                @endif

                                @if ($policys->vigencia_final != '')
                                   <td>{{ \Carbon\Carbon::parse($policys->vigencia_final)->format('d-m-Y') }}</td>
                                @else
                                    <td></td>
                                @endif

                              <td>{{ $policys->fpago }}</td>
                              <td>{{ $policys->nanual }}</td>
                              <td>{{ $policys->nanual }}</td>
                              

                                <td class="text-center">
                                    <a href="{{ url('admin/' . $type_policys->id . '/policys/' . $policys->id . '') }}"
                                        class="btn btn-info btn-sm">
                                        <i class="fas fa-edit "></i>
                                    </a>
                                    <button type="button" class="btn btn-primary btn-sm"
                                        onclick="get_solicitudes(<?= $policys->id ?>)">
                                        <i class="fa fa-tag" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('admin.requisition.modal_lista')
    {{-- @include('admin.requisition.modal_new') --}}

    <script>
        let _glo_policy_id = "";

        function get_solicitudes(policy_id) {
            _glo_policy_id = policy_id;
            $('#table_modal').hide();
            $('#load_modal').show();
            $('#table_endosos').html('');
            $('#modelId').modal('show');

            $.ajax({
                type: "GET", // la variable type guarda el tipo de la peticion GET,POST,..
                url: "<?= url('/api/get_requisition_poilce') ?>", //url guarda la ruta hacia donde se hace la peticion
                data: {
                    policy_id: policy_id
                }, // data recive un objeto con la informacion que se enviara al servidor
                success: function(
                datos) { //success es una funcion que se utiliza si el servidor retorna informacion
                    $.each(datos, function(index, value) {
                        $("#table_endosos").append("<tr>" +
                            "<td>" + value.id + "</td>" +
                            "<td>" + value.num + "</td>" +
                            "<td>" + value.creado_por.name + " " + value.creado_por.last_name +
                            " " + value.creado_por.mother_last_name + "</td>" +
                            "<td><a target='_black' href='<?= url('admin/requisition/') ?>/" + value
                            .id +
                            "/edit' tar class='btn btn-info btn-sm'> <i class='fa fa-eye' aria-hidden='true'></i>  </a></td>" +
                            "</tr>");
                    });
                    $('#table_modal').show();
                    $('#load_modal').hide();
                },
                dataType: "json" // El tipo de datos esperados del servidor. Valor predeterminado: Intelligent Guess (xml, json, script, text, html).
            })
        }

        function recargar() {
            get_solicitudes(_glo_policy_id);
        }

        function nuevo() {

        }
    </script>
@stop
