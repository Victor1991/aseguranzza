@extends('admin.layout')

@section('content')
<!-- Info boxes -->
<div class="row">
     <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
               <span class="info-box-icon bg-info elevation-1"><i class="fa fa-eye" aria-hidden="true"></i></span>

               <div class="info-box-content">
                    <span class="info-box-text">Vistas</span>
                    <span class="info-box-number">
                         0
                    </span>
               </div>
               <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
     </div>
     <!-- /.col -->
     <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
               <span class="info-box-icon bg-danger elevation-1"><i class="far fa-calendar-alt"></i></span>

               <div class="info-box-content">
                    <span class="info-box-text">Tareas</span>
                    <span class="info-box-number">{{ $tasks->count() }}</span>
               </div>
               <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
     </div>
     <!-- /.col -->

     <!-- fix for small devices only -->
     <div class="clearfix hidden-md-up"></div>

     <!-- /.col -->
     <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
               <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

               <div class="info-box-content">
                    <span class="info-box-text">Clientes</span>
                    <span class="info-box-number">{{ $user->count() }}</span>
               </div>
               <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
     </div>
</div>
<div class="row">

     <!-- /.col -->
     <div class="col-md-12">
          <div class="card card-primary">
               <div class="card-body ">
                    <div class="row">

                         <div class="col-md-4">
                              <div class="bg-lightblue ui-draggable ui-draggable-handle text-center" style="background-color: #194497 !important; padding: 10px; border-radius: 4px;">
                                   <div class="icheck-success d-inline">
                                        <input type="checkbox" id="checkboxPrimary1" checked>
                                        <label for="checkboxPrimary1">
                                             Identificación por caducar
                                        </label>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-4">
                              <div class="bg-lightblue ui-draggable ui-draggable-handle text-center" style="background-color: #2ba896 !important; padding: 10px; border-radius: 4px;">
                                   <div class="icheck-success d-inline">
                                        <input type="checkbox" id="checkboxPrimary2" checked>
                                        <label for="checkboxPrimary2">
                                             Tareas
                                        </label>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-4">
                              <div class="bg-lightblue ui-draggable ui-draggable-handle text-center" style="background-color: #e19f00 !important; padding: 10px; border-radius: 4px;">
                                   <div class="icheck-success d-inline">
                                        <input type="checkbox" id="checkboxPrimary3" checked>
                                        <label for="checkboxPrimary3">
                                             Cumpleaños
                                        </label>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">

                         </div>
                    </div>
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
               </div>
               <!-- /.card-body -->
          </div>
          <!-- /.card -->
     </div>
     <!-- /.col -->
</div>
@stop
@push('scripts')
<script src="{{ asset('/theme/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>
<script src="{{ asset('/js/locales-all.min.js')}}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/es.min.js" integrity="sha512-rW9yo+wCSOiuvTcjeYtoihJoZHWXYWqPNdNs1hpMLn+Gz8THZaHpU10qbxdNlcydbUZghThvVR5KevjTHWM2ww==" crossorigin="anonymous"></script> -->
<!-- Page specific script -->
<script>
$(function () {

     $('#checkboxPrimary1').on('click', function () {
          if ($(this).is(":checked")) {
               $('.identificacion_event').show();
          }else {
               $('.identificacion_event').hide();
          }
          console.log($(this).is(":checked"));
     });

     $('#checkboxPrimary2').on('click', function () {
          if ($(this).is(":checked")) {
               $('.tareas_event').show();
          }else {
               $('.tareas_event').hide();
          }
          console.log($(this).is(":checked"));
     });

     $('#checkboxPrimary3').on('click', function () {
          if ($(this).is(":checked")) {
               $('.cumpleanios_event').show();
          }else {
               $('.cumpleanios_event').hide();
          }
          console.log($(this).is(":checked"));
     });

     /* initialize the calendar
     -----------------------------------------------------------------*/
     //Date for the calendar events (dummy data)
     var date = new Date()
     var d    = date.getDate(),
     m    = date.getMonth(),
     y    = date.getFullYear()

     console.log(d);
     console.log(m);
     console.log(y);


     var Calendar = FullCalendar.Calendar;
     var Draggable = FullCalendarInteraction.Draggable;

     var containerEl = document.getElementById('external-events');
     var checkbox = document.getElementById('drop-remove');
     var calendarEl = document.getElementById('calendar');

     // initialize the external events
     // -----------------------------------------------------------------

     var calendar = new Calendar(calendarEl, {
          plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
          header    : {
          //      today: 'Hoy'
          // },
               // left  : 'prev,next today',
               // center: 'title',
               // right : 'dayGridMonth,timeGridWeek,timeGridDay',
               // right : 'prev,next today',

          },
          buttonText:{
               prev:"Ant",
               next:"Sig",
               today:"Hoy",
               month:"Mes",
               week:"Semana",
               day:"DÃ­a",
               list:"Agenda"
          },
          locale: 'es',
          'themeSystem': 'bootstrap',
          //Random default events
          events    : [
               <?php foreach ($id_expires as $key => $id_expire): ?>
               <?php $fe = date("Y,m,d", strtotime($id_expire->identification_official_validity)); ?>
               {
                   title          : 'Identificación por caducar de \n  {{ $id_expire->name }} ' ,
                   start          : new Date('{{ $id_expire->identification_official_validity }}T13:07:20'),
                   className      : "identificacion_event",
                   backgroundColor: '#194497', //red
                   borderColor    : '#000', //red
                   url            : '{{ url("admin/user/$id_expire->id/edit") }}',
              },
               <?php endforeach; ?>
               <?php foreach ($passport_expires as $key => $passport_expire): ?>
               {
                  title          : 'Identificación por caducar de \n  {{ $passport_expire->name }} ' ,
                  start          : new Date('{{ $id_expire->passport_validity }}T13:07:20'),
                  backgroundColor: '#194497', //red
                  borderColor    : '#000', //red
                  className      : "identificacion_event",
                  url            : '{{ url("admin/user/$passport_expire->id/edit") }}',
               },
               <?php endforeach; ?>
               <?php foreach ($tasks as $key => $task): ?>
               {
                  title          : 'Tarea \n  {{ $task->name }} ' ,
                  start          : new Date('{{ $task->end_date }}T13:07:20'),
                  backgroundColor: '#2ba896', //red
                  borderColor    : '#000', //red
                  className      : "tareas_event",
                  url            : '{{ url("admin/task/$task->id/edit") }}',
               },
               <?php endforeach; ?>

               <?php foreach ($user as $key => $usr): ?>
               {
                  title          : 'Cumpleaños de \n  {{ $usr->name }} ' ,
                  start          : new Date(y+'{{ \Carbon\Carbon::createFromFormat("Y-m-d", $usr->birthdate)->format("-m-d") }}T13:07:20'),
                  backgroundColor: '#e19f00', //red
                  borderColor    : '#000', //red
                  className      : "cumpleanios_event",
                  url            : '{{ url("admin/user/$usr->id/edit") }}',
               },
               <?php endforeach; ?>
          ],
          editable  : true,
          droppable : true, // this allows things to be dropped onto the calendar !!!

     });

     calendar.render();
     // $('#calendar').fullCalendar()

})
</script>

@endpush

@push('style')
<!-- fullCalendar -->
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<style>
.info_card{
     text-align:center;
     background-color: rgba(10, 10, 10, 0.66);
     margin-top:80px;
     margin-bottom:80px;
     padding: 80px;
     border-radius: 15px;
}
</style>
@endpush
